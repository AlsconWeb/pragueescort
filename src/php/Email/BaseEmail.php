<?php
/**
 * Base email interface.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\Email;

/**
 * BaseEmail class file.
 */
abstract class BaseEmail {

	/**
	 * Send abstract email to customer.
	 *
	 * @param string      $subject Email subject.
	 * @param string      $body    Email body.
	 * @param string|null $email   Email send to.
	 */
	protected static function send( string $subject, string $body, string $email ): void {
		ob_start();
		?>
		<!doctype html>
		<html lang="en">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<?php self::get_style(); ?>
		</head>
		<body>
		<div class="body-email">
			<?php echo wp_kses_post( $body ); ?>
		</div>
		</body>
		</html>
		<?php

		$email_content = ob_get_clean();

		$to = $email;

		$headers = 'MIME-Version: 1.0' . "\r\n";

		$headers .= 'Content-type:text/html; charset=UTF-8' . "\r\n";
		wp_mail( $to, $subject, $email_content, $headers );

	}

	/**
	 * Content email.
	 *
	 * @param array $content Content.
	 *
	 * @return false|string
	 */
	protected static function get_content( array $data ) {
		ob_start();
		?>
		<body style="background: linear-gradient(92.06deg, #0E1A2D 0%, #30163E 100%);">
		<table
			border="0"
			cellpadding="0"
			cellspacing="0"
			width="520"
			style="margin-left: auto; margin-right: auto; font-family:sans-serif;">
			<?php self::get_header(); ?>
			<tbody>
			<tr>
				<td style="background:#ffffff; border-radius: 16px; padding: 40px 50px;">
					<table>
						<?php self::get_title( $data['title'] ); ?>
						<tbody>
						<?php echo wp_kses_post( $data['content'] ); ?>
						<?php self::get_footer(); ?>
						</tbody>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
		</body>
		<?php

		return ob_get_clean();
	}

	/**
	 * Get style.
	 *
	 * @return void
	 */
	private static function get_style(): void {
		?>
		<style>
			hr {
				width: 100%;
				border-width: 1px 0 0 0;
				border-color: #000000;
				border-style: solid;
				opacity: 0.12;
				margin-top: 7px;
				margin-bottom: 7px;
			}

			hr:last-of-type {
				margin-top: 13px;
				margin-bottom: 13px;
			}

			h1 {
				font-weight: 700;
				font-size: 22px;
				margin: 0;
				color: #331541;
			}

			h3 {
				font-weight: 700;
				font-size: 17px;
				line-height: 29px;
				color: #331541;
				padding-bottom: 10px;
				margin: 0;
			}

			p,
			.link {
				font-weight: 500;
				font-size: 16px;
				color: #331541;
				line-height: 19px;
				padding-bottom: 10px;
				margin: 0;
			}

			.btn {
				text-decoration: none;
				display: block;
				background: linear-gradient(100.51deg, #FF7625 0%, #C43BBB 100%);
				border-radius: 16px;
				font-weight: 700;
				font-size: 16px;
				color: #fff;
				width: 146px;
				height: 43px;
				text-align: center;
				line-height: 43px;
			}

			.btn + .link {
				margin-top: 15px;
			}

			.link {
				color: #17A3F1;
				display: block;
				padding-bottom: 0;
			}

			.last {
				font-weight: 500;
				font-size: 12px;
				line-height: 15px;
				color: #331541;
				opacity: 0.6;
				margin-top: 30px;
			}

			.last span {
				display: block;
				padding-top: 5px;
			}

			@media only screen and (max-width: 599px) {
				body > table {
					width: calc(100% - 10px);
				}

				body > table table {
					width: 100%;
				}

				body table thead > tr > td > table > tbody > tr > td {
					width: 100%;
					display: block;
				}

				body table thead > tr > td > table > tbody > tr > td:first-of-type {
					display: table;
					width: auto;
					margin: 0px auto 10px;
				}

				body table p {
					text-align: center;
				}

				body > table > tbody > tr > td {
					padding: 45px 15px !important;
				}
			}
		</style>
		<?php
	}

	/**
	 * Get header.
	 *
	 * @return void
	 */
	private static function get_header(): void {
		?>
		<thead>
		<tr>
			<td style="padding:24px 0">
				<table border="0" cellpadding="0" cellspacing="0" width="520">
					<tr>
						<td style="text-align: center; padding-bottom: 20px;">
							<a href="<?php bloginfo( 'url' ); ?>">
								<img
									src="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/img/logo.png' ); ?>"
									alt="Logo"
									width="76.27"
									height="42.6px">
							</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</thead>
		<?php
	}

	/**
	 * Get title.
	 *
	 * @param string $title Title.
	 *
	 * @return void
	 */
	protected static function get_title( string $title ): void {
		?>
		<thead>
		<tr>
			<td>
				<h1><?php echo esc_html( $title ); ?></h1>
			</td>
		</tr>
		</thead>
		<?php
	}

	/**
	 * Get footer email.
	 *
	 * @return void
	 */
	protected static function get_footer(): void {
		?>
		<tr>
			<td>
				<p class="last">
					<?php esc_html_e( 'Prague Escort Team', 'pragueescort' ); ?>
					<span><?php esc_html_e( 'Automated messege, please do not reply', 'pragueescort' ); ?></span>
				</p>
			</td>
		</tr>
		<?php
	}

}
