<?php
/**
 * Tab services.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\Helpers;

$escort_id = get_the_ID();
$helpers   = new Helpers();
?>
<div class="tab-content" id="services">
    <h3><?php esc_html_e( 'Standart service', 'pragueescort' ); ?></h3>
    <ul class="desc">
        <li>
            <?php esc_html_e( 'Duo with Girlfriend', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_duo_gir_friend' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Relax massage', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_relax_massage' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Erotic massage', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_erotic_massage' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Shower/bathing together', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_bathing_together' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Cum on body', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_cum_body' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Extra ball', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_extra_ball' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Handjob', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_handjob' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Kissing (if good chemistry)', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_kissing' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Sex toys', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_sex_toys' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Fingering', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_fingering' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( '2 men', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_two_men' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Blowjob without Condom to Completion', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_blowjob' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Oral sex without condom', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_oral' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Licking', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_licking' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Classic sex with condom', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_classic_sex' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Couple', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_couple' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Rimming', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_rimming' ) ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( '69 position', 'pragueescort' ); ?>
            <span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_sixty_nine' ) ) ); ?></span>
        </li>
    </ul>

    <h3 class="extra"><?php esc_html_e( 'Extra', 'pragueescort' ); ?></h3>
    <ul class="desc">
        <li>
            <?php esc_html_e( 'CIM (Come in mouth)', 'pragueescort' ); ?>
            <span><?php echo esc_html( $helpers->get_extra_price( 'pra_cim', $escort_id ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'COF (Come on face)', 'pragueescort' ); ?>
            <span><?php echo esc_html( $helpers->get_extra_price( 'pra_cof', $escort_id ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Swallow', 'pragueescort' ); ?>
            <span><?php echo esc_html( $helpers->get_extra_price( 'pra_swallow', $escort_id ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'DFK (Deep french kissing)', 'pragueescort' ); ?>
            <span><?php echo esc_html( $helpers->get_extra_price( 'pra_dfk', $escort_id ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'A-Level (Anal sex)', 'pragueescort' ); ?>
            <span><?php echo esc_html( $helpers->get_extra_price( 'pra_anal', $escort_id ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Anal Rimming (take)', 'pragueescort' ); ?>
            <span><?php echo esc_html( $helpers->get_extra_price( 'pra_anal_rimming', $escort_id ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Golden shower', 'pragueescort' ); ?>
            <span><?php echo esc_html( $helpers->get_extra_price( 'pra_golden', $escort_id ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Threesome', 'pragueescort' ); ?>
            <span><?php echo esc_html( $helpers->get_extra_price( 'pra_threesome', $escort_id ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Foot fetish', 'pragueescort' ); ?>
            <span><?php echo esc_html( $helpers->get_extra_price( 'pra_foot_fetish', $escort_id ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Submission', 'pragueescort' ); ?>
            <span><?php echo esc_html( $helpers->get_extra_price( 'pra_submission', $escort_id ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'BDSM', 'pragueescort' ); ?>
            <span><?php echo esc_html( $helpers->get_extra_price( 'pra_bdsm', $escort_id ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Domination', 'pragueescort' ); ?>
            <span><?php echo esc_html( $helpers->get_extra_price( 'pra_domination', $escort_id ) ); ?></span>
        </li>
        <li>
            <?php esc_html_e( 'Anal Rimming (Licking anus) give', 'pragueescort' ); ?>
            <span><?php echo esc_html( $helpers->get_extra_price( 'pra_anal_rimming_give', $escort_id ) ); ?></span>
        </li>
    </ul>
</div>
