<?php
/**
 * Site bar Hot template.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\Helpers;

$args_query = [
	'post_type'      => 'escort',
	'posts_per_page' => 4,
	'order'          => 'DESC',
	'orderby'        => 'rand',
	'tax_query'      => [
		[
			'taxonomy' => 'escort-type',
			'field'    => 'slug',
			'terms'    => 'diamond',
		],
	],
];

$hot_query = new WP_Query( $args_query );

$execute = $args['execute'];
?>
<div class="hot">
	<h3 class="status hot"><?php esc_html_e( 'Hot models', 'pragueescort' ); ?></h3>
	<?php
	if ( $hot_query->have_posts() ) {
		while ( $hot_query->have_posts() ) {
			$hot_query->the_post();
			$escort_id = get_the_ID();
			$execute->set_exclude_id( $escort_id );
			?>
			<div class="item">
				<a class="link" href="<?php the_permalink(); ?>"></a>
				<?php
				if ( has_post_thumbnail( $escort_id ) ) {
					the_post_thumbnail( 'escort_thumbnail' );
				} else {
					echo '<img src="//via.placeholder.com/78x117" alt="No image">';
				}
				?>
				<div class="desc">
					<h4><?php the_title(); ?></h4>
					<h5><?php echo esc_attr( Helpers::get_age( carbon_get_post_meta( $escort_id, 'pra_date_birth' ) ) ); ?></h5>
					<p><?php the_excerpt(); ?></p>
				</div>

			</div>
			<?php
		}
		wp_reset_postdata();
	}
	?>
	<a class="button blue" href="#">
		<?php esc_html_e( 'View all', 'pragueescort' ); ?>
	</a>
</div>
