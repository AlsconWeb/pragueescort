<?php
/**
 * Tab comments.
 *
 * @package pragueescort/theme
 */

$escort_id        = $args['escort_id'];
$profile_comments = get_comments(
	[
		'post_id'      => $escort_id,
		'status'       => 'approve',
		'hierarchical' => true,
	]
);

?>
<div class="tab-content" id="comments" data-profile_id="<?php echo esc_attr( $escort_id ); ?>">
	<?php if ( ! empty( $profile_comments ) ) { ?>
		<ul class="comments">
			<?php
			$count = count( $profile_comments );
			for ( $i = 0; $i < $count; $i ++ ) {
				?>
				<li>
					<div class="comment">
						<div class="user">
							<h3><?php echo esc_html( $profile_comments[ $i ]->comment_author ); ?></h3>
						</div>
						<p><?php echo esc_html( $profile_comments[ $i ]->comment_content ); ?></p>
					</div>
					<?php
					if ( $profile_comments[ $i + 1 ]->comment_parent ) {
						?>
						<ul class="reply icon-arrow">
							<li>
								<div class="comment">
									<div class="user">
										<h3><?php echo esc_html( $profile_comments[ $i + 1 ]->comment_author ); ?></h3>
									</div>
									<p><?php echo esc_html( $profile_comments[ $i + 1 ]->comment_content ); ?></p>
								</div>
							</li>
						</ul>
						<?php
						unset( $profile_comments[ $i + 1 ] );
						$count --;
						continue;
					}
					?>
				</li>
				<?php
			}
			?>
		</ul>
	<?php } else { ?>
		<ul class="comments">
			<li>
				<h4 class="no-comments">
					<?php esc_html_e( 'No comments', 'pragueescort' ); ?>
				</h4>
			</li>
		</ul>
	<?php } ?>
	<a class="button gradient" href="#" data-toggle="modal" data-target=".comment-modal">
		<?php esc_html_e( 'New review', 'pragueescort' ); ?>
	</a>
</div>
