<?php
/**
 * Single template file.
 *
 * @package pragueescort/theme
 */

get_header();
$post_categories = 0;
?>
	<section>
		<div class="vc_row wpb_row vc_row-fluid vc_row-o-content-middle vc_row-flex">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_row wpb_row vc_inner vc_row-fluid">
							<div class="wpb_column vc_column_container vc_col-xs-12 vc_col-sm-7 vc_col-md-8 vc_col-lg-9">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="wpb_column vc_column_container">
											<a
													class="back icon-arrow-left"
													href="<?php echo esc_url( get_bloginfo( 'url' ) ); ?>">
												<?php esc_html_e( 'Home', 'pragueescort' ); ?>
											</a>
											<div class="blog-content">
												<h1><?php the_title(); ?></h1>
												<?php
												if ( have_posts() ) {
													while ( have_posts() ) {
														the_post();

														$post_categories = get_the_category( get_the_ID() );
														$current_post_id = get_the_ID();
														the_content();
													}
												}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="wpb_column vc_column_container vc_col-xs-12 vc_col-sm-5 vc_col-md-4 vc_col-lg-3">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="wpb_column vc_column_container">
											<?php get_template_part( 'template-part/advertising' ); ?>
											<div class="blogs">
												<?php
												$args          = [
													'posts_per_page' => 3,
													'category'       => $post_categories[0]->term_id,
													'post__not_in'   => [ $current_post_id ],
												];
												$related_posts = new WP_Query( $args );

												if ( $related_posts->have_posts() ) {
													?>
													<h4><?php esc_html_e( 'Category', 'pragueescort' ); ?></h4>
													<?php
													while ( $related_posts->have_posts() ) {
														$related_posts->the_post();

														get_template_part( 'template-part/blog/blog', 'item' );
													}
												}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
