<?php
/**
 * Modal add comment.
 *
 * @package pragueescort/theme
 */

?>
<div class="modal fade comment-modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<i class="icon-close" data-dismiss="modal"></i>
			<form method="post" class="add-comment-form-client">
				<div class="photo">
					<label for="photo">
						<i class="icon-user" for="photo"></i>
					</label>
				</div>
				<div class="inputs">
					<div class="input icon-edit">
						<label for="client-name">
							<?php esc_html_e( 'Name', 'pragueescort' ); ?>
						</label>
						<input
							id="client-name"
							name="pra_comment[client_name]"
							type="text"
							placeholder="<?php esc_html_e( 'Name', 'pragueescort' ); ?>">
					</div>
					<div class="input icon-calendar">
						<label for="meeting-date">
							<?php esc_html_e( 'Meeting date', 'pragueescort' ); ?>
						</label>
						<input
							class="data"
							id="meeting-date"
							name="pra_comment[meeting_date]"
							type="text"
							placeholder="<?php esc_html_e( '12/04/2023', 'pragueescort' ); ?>">
					</div>
					<div class="input">
						<label for="client-phone">
							<?php esc_html_e( 'Phone', 'pragueescort' ); ?>
						</label>
						<input id="client-phone" name="pra_comment[phone]" type="tel">
					</div>
				</div>
				<div class="textarea">
					<label for="comment" class="hide"></label>
					<textarea id="comment" placeholder="<?php esc_html_e( 'Comment', 'pragueescort' ); ?>"></textarea>
				</div>
				<div class="submit">
					<input
						class="button gradient"
						id="comment-submit"
						type="submit"
						value="<?php esc_html_e( 'Send', 'pragueescort' ); ?>">
				</div>
			</form>
			<p><?php esc_html_e( 'The data you provide when adding a comment will not be published anywhere. It is only used for verification by the moderator.', 'pragueescort' ); ?></p>
		</div>
	</div>
</div>
