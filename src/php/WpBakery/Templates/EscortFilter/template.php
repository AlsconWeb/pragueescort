<?php
/**
 * Filter escort.
 *
 * @package pragueescort/theme
 */

if ( empty( $_GET['gender'] ) ) {
	$filter_gender = 'all';
} else {
	$filter_gender = filter_var( wp_unslash( $_GET['gender'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS );
}

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
?>
<div class="filter-block <?php echo esc_attr( $css_class ?? null ); ?>">
	<ul class="filter-menu">
		<?php foreach ( $this->filter_gender as $key => $filter ) { ?>
			<li class="<?php echo ! empty( $filter_gender ) && $filter_gender === $key ? 'active' : ''; ?>">
				<a href="#" data-gender="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $filter ); ?></a>
			</li>
		<?php } ?>
	</ul>
	<?php if ( ! empty( $this->filter_service ) ) { ?>
		<ul class="filter-menu service">
			<?php
			foreach ( $this->filter_service as $key => $service ) {
				$terms_id = get_term_by( 'slug', $key, 'service' );
				?>
				<li class="<?php echo ! empty( $filter_service ) && $filter_service === $key ? 'active' : ''; ?>">
					<a
							href="<?php echo esc_url( get_term_link( $terms_id, 'service' ) ); ?>"
							data-service="<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $service ); ?>
					</a>
				</li>
			<?php } ?>
		</ul>
	<?php } ?>
	<form action="/" class="filters" method="get">
		<input type="hidden" id="gender" name="gender" value="">
	</form>
</div>
