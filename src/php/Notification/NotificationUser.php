<?php
/**
 * Notification user.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\Notification;

/**
 * NotificationUser class file.
 */
class  NotificationUser {

	/**
	 * Account is freeze.
	 *
	 * @return void
	 */
	public static function account_is_freeze(): void {
		?>
		<div class="info freeze">
			<i class="icon-freeze"></i>
			<p>
				<?php esc_html_e( 'Your account has been frozen all profiles have become non-public', 'pragueescort' ); ?>
			</p>
		</div>
		<?php
	}

	/**
	 * Profile not been verified.
	 *
	 * @return void
	 */
	public static function profile_no_verified(): void {
		?>
		<div class="info ">
			<i class="icon-info"></i>
			<p>
				<?php esc_html_e( 'Your account has not been verified yet. Wait a while while the administrator checks all the data', 'pragueescort' ); ?>
			</p>
		</div>
		<?php
	}
}
