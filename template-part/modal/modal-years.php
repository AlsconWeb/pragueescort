<?php
/**
 * Template part: Year modal.
 *
 * @package pragueescort/theme
 */

$cookie_years = ! empty( $_COOKIE['yearsModal'] ) ? $_COOKIE['yearsModal'] : [];
?>
<div class="modal fade years-modal">
	<div class="modal-dialog">
		<?php if ( empty( $cookie_years ) ) { ?>
			<div class="modal-content">
				<h3><?php esc_html_e( 'Confirm your age', 'pragueescort' ); ?></h3>
				<h4><?php esc_html_e( 'Access to the site is only permitted for those over 18 years of age. The site contains pornographic material. By accessing the site, you agree to the stated terms of entry.', 'pragueescort' ); ?></h4>
				<div class="buttons">
					<a class="button gradient yes-btn" href="#" data-dismiss="modal">
						<?php esc_html_e( 'Yes', 'pragueescort' ); ?>
					</a>
					<a class="button blue no-btn" href="#">
						<?php esc_html_e( 'No', 'pragueescort' ); ?>
					</a>
				</div>
			</div>
		<?php } ?>
		<?php if ( 'false' === $cookie_years ) { ?>
			<div class="modal-content">
				<h3><?php esc_html_e( 'We apologize!', 'pragueescort' ); ?></h3>
				<h4><?php esc_html_e( 'You are not old enough to visit this site...', 'pragueescort' ); ?></h4>
			</div>
		<?php } ?>
	</div>
</div>
