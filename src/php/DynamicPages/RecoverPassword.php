<?php
/**
 * Dynamic page recovery password email.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\DynamicPages;

use PRAGUE\Theme\AuthorizationUser;

/**
 * RecoverPassword class file.
 */
class RecoverPassword extends DynamicPageBase {

	/**
	 * Page slug.
	 */
	public const PAGE_PATH = 'recover-password-email';

	/**
	 * Exclude footer and header.
	 *
	 * @var string
	 */
	public string $execute_slug = 'recover-password-email';

	/**
	 * Output content.
	 *
	 * @return void
	 */
	protected function content(): void {
		$this->show_content();
	}

	/**
	 * Show content.
	 *
	 * @return void
	 */
	private function show_content(): void {

		$nonce = ! empty( $_GET['nonce_recover'] ) ? filter_var( wp_unslash( $_GET['nonce_recover'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, AuthorizationUser::PRA_MAIL_AUTH_NONCE ) ) {
			echo '<h1>' . esc_html( __( 'Bad nonce code', 'pragueescort' ) ) . '</h1>';
		}

		$email     = ! empty( $_GET['user_email'] ) ? filter_var( wp_unslash( $_GET['user_email'] ), FILTER_SANITIZE_EMAIL ) : null;
		$user_pass = ! empty( $_GET['new_user_pass'] ) ? filter_var( wp_unslash( $_GET['new_user_pass'] ), FILTER_SANITIZE_STRING ) : null;

		if ( empty( $email ) ) {
			echo '<h1>' . esc_html( __( 'No correct email', 'pragueescort' ) ) . '</h1>';
		}

		if ( empty( $user_pass ) ) {
			echo '<h1>' . esc_html( __( 'Empty name', 'pragueescort' ) ) . '</h1>';
		}

		if ( ! email_exists( $email ) ) {
			echo '<h1>' . esc_html( __( 'This email is linked to another account', 'pragueescort' ) ) . '</h1>';
		}

		$user = get_user_by( 'email', $email );

		//phpcs:disable
		reset_password( $user, base64_decode( $user_pass ) );
		//phpcs:enable

		wp_redirect( get_bloginfo( 'url' ), 301 );
		die();
	}
}
