<?php
/**
 * Main init theme class.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme;

use PRAGUE\Theme\Account\AccountActions;
use PRAGUE\Theme\Account\PaymentOrders;
use PRAGUE\Theme\Archives\Actions;
use PRAGUE\Theme\DynamicPages\AuthVerificationEmail;
use PRAGUE\Theme\DynamicPages\ProfileComment;
use PRAGUE\Theme\DynamicPages\ProfilePage;
use PRAGUE\Theme\DynamicPages\ProfileSecurity;
use PRAGUE\Theme\DynamicPages\RecoverPassword;
use PRAGUE\Theme\DynamicPages\TariffPage;
use PRAGUE\Theme\Profile\Edit;
use PRAGUE\Theme\Profile\ProfileActions;
use PRAGUE\Theme\WpBakery\Components\Advertising;
use PRAGUE\Theme\WpBakery\Components\EscortFilter;
use PRAGUE\Theme\WpBakery\Components\EscortListing;
use PRAGUE\Theme\WpBakery\Components\EscortListingAjax;
use PRAGUE\Theme\WpBakery\Components\FAQ;
use PRAGUE\Theme\WpBakery\Components\Partners;
use PRAGUE\Theme\WpBakery\Components\ProductsBlock;
use WP_Query;

/**
 * Main class file.
 */
class Main {

	/**
	 * Theme version.
	 */
	public const PRA_VERSION = '1.2.2';

	/**
	 * Dir path.
	 */
	public const PRA_DIR_PATH = __DIR__;

	/**
	 * Archive posts per page.
	 */
	public const PRA_ARCHIVE_POST_PER_PAGE = 30;

	/**
	 * Main construct.
	 */
	public function __construct() {
		$this->init();

		new CPT();
		new CarbonFields();
		new AuthorizationUser();
		new Edit();
		new ProfileActions();
		new AccountActions();
		new Actions();
		new PaymentOrders();

		// dynamic page.
		new RecoverPassword();
		new AuthVerificationEmail();
		new ProfilePage();
		new ProfileComment();
		new ProfileSecurity();
		new TariffPage();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'after_setup_theme', [ $this, 'add_theme_support' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'add_script' ] );
		add_action( 'vc_before_init', [ $this, 'add_bakery_components' ] );
		add_action( 'pre_get_posts', [ $this, 'change_archive_posts_per_page' ] );
		add_action( 'after_setup_theme', [ $this, 'disable_admin_bar' ] );
		add_action( 'widgets_init', [ $this, 'register_widgets' ] );

		add_filter( 'mime_types', [ $this, 'add_support_mimes' ] );
		add_filter( 'get_custom_logo', [ $this, 'output_logo' ] );
		add_filter( 'excerpt_length', [ $this, 'change_excerpt_length' ] );
		add_filter( 'excerpt_more', [ $this, 'change_excerpt_more' ] );
	}

	/**
	 * Theme support.
	 *
	 * @return void
	 */
	public function add_theme_support(): void {

		// add custom logo.
		add_theme_support( 'custom-logo' );

		// add menu support.
		add_theme_support( 'menus' );

		register_nav_menus(
			[
				'top_bar_menu'        => __( 'Top bar menu', 'pragueescort' ),
				'main_menu'           => __( 'Main menu', 'pragueescort' ),
				'footer_menu_col_one' => __( 'Footer menu col one', 'pragueescort' ),
				'footer_menu_col_two' => __( 'Footer menu col two', 'pragueescort' ),
			]
		);

		// post thumbnail.
		add_theme_support( 'post-thumbnails' );

		// images size.
		add_image_size( 'escort_thumbnail', 180, 238, [ 'center', 'top' ] );
		add_image_size( 'prague-blog-thumbnail', 270, 196, [ 'center', 'top' ] );
	}

	/**
	 * Add scripts and style.
	 *
	 * @return void
	 */
	public function add_script(): void {
		$url = get_stylesheet_directory_uri();
		$min = '.min';

		if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
			$min = '';
		}

		wp_enqueue_script(
			'prague_select2',
			$url . '/assets/js/select2.min.js',
			[
				'jquery',
			],
			'4.1.0-rc.0',
			true
		);

		wp_enqueue_script(
			'prague_slick',
			$url . '/assets/js/slick.min.js',
			[
				'jquery',
			],
			'1.8.0',
			true
		);

		wp_enqueue_script(
			'prague_build',
			$url . '/assets/js/build' . $min . '.js',
			[
				'jquery',
				'fancyapps',
				'prague_modal',
				'inputmask',
				'intl-tel',
				'prague_select2',

			],
			self::PRA_VERSION,
			true
		);

		wp_enqueue_script(
			'prague_modal',
			$url . '/assets/js/modal.js',
			[
				'jquery',
			],
			self::PRA_VERSION,
			true
		);

		wp_enqueue_script(
			'prague_main',
			$url . '/assets/js/main' . $min . '.js',
			[
				'jquery',
				'fancyapps',
				'prague_modal',
				'prague_build',
			],
			self::PRA_VERSION,
			true
		);

		wp_enqueue_script( 'inputmask', '//cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.8/jquery.inputmask.bundle.min.js', '', '4.0.8', true );
		wp_enqueue_script( 'fancyapps', '//cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js', [], '5.0', false );
		wp_enqueue_script( 'intl-tel', '//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput-jquery.min.js', [ 'jquery' ], '17.0.8', false );
		wp_enqueue_script( 'html5shiv', '//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', [], '3.7.0', false );
		wp_enqueue_script( 'respond', '//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', [], '1.4.2', false );
		wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
		wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

		wp_enqueue_style( 'google-font', '//fonts.googleapis.com/css2?family=Inter:wght@500;700&amp;display=swap', '', '1.0' );
		wp_enqueue_style( 'fancyapps', '//cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css', '', '5.0' );

		if ( is_singular( 'escort' ) || fq_is_page( 'profile' ) || is_404() || is_archive() || is_home() || is_singular( 'post' ) ) {
			wp_enqueue_style( 'prague_bakery', $url . '/assets/css/wp_bakery.min.css', '', self::PRA_VERSION );
			wp_enqueue_style( 'prague_date-picker', $url . '/assets/css/bootstrap-datepicker3.min.css', '', self::PRA_VERSION );
			wp_enqueue_script( 'prague_date-picker', $url . '/assets/js/bootstrap-datepicker.min.js', [ 'jquery' ], self::PRA_VERSION, true );
		}

		wp_enqueue_style( 'intl-tel', $url . '/assets/css/intlTelInput.min.css', '', '17.0.8' );
		wp_enqueue_style( 'prague_modal', $url . '/assets/css/modal' . $min . '.css', '', self::PRA_VERSION );
		wp_enqueue_style( 'prague_main', $url . '/assets/css/main' . $min . '.css', '', self::PRA_VERSION );
		wp_enqueue_style( 'prague_style', $url . '/style.css', '', self::PRA_VERSION );
		wp_enqueue_style( 'prague_select2', $url . '/assets/css/select2.min.css', '', self::PRA_VERSION );

		wp_localize_script(
			'prague_main',
			'praObjectSettings',
			[
				'ajaxUrl'                     => admin_url( 'admin-ajax.php' ),
				'loginAction'                 => AuthorizationUser::PRA_AUTH_ACTION_NAME,
				'loginNonceName'              => AuthorizationUser::PRA_AUTH_ACTION_NAME,
				'registerAction'              => AuthorizationUser::PRA_REG_ACTION_NAME,
				'registerNonceName'           => AuthorizationUser::PRA_REG_ACTION_NAME,
				'recoverPswdActionName'       => AuthorizationUser::PRA_RECOVER_PSWD_ACTION_NAME,
				'recoverPswdNonceName'        => AuthorizationUser::PRA_RECOVER_PSWD_ACTION_NAME,
				'addProfileAction'            => Edit::PRA_ADD_PROFILE_ACTION_NAME,
				'addProfileNonce'             => wp_create_nonce( Edit::PRA_ADD_PROFILE_ACTION_NAME ),
				'deleteProfileAction'         => Edit::PRA_DELETE_PROFILE_ACTION_NAME,
				'deleteProfileNonce'          => wp_create_nonce( Edit::PRA_DELETE_PROFILE_ACTION_NAME ),
				'changeProfileAction'         => Edit::PRA_CHANGE_PROFILE_ACTION_NAME,
				'changeProfileNonce'          => wp_create_nonce( Edit::PRA_CHANGE_PROFILE_ACTION_NAME ),
				'deleteImageInGalleryAction'  => Edit::PRA_DELETE_IMAGE_IN_GALLERY,
				'deleteImageInGalleryNonce'   => wp_create_nonce( Edit::PRA_DELETE_IMAGE_IN_GALLERY ),
				'loadMoreEscortListingAction' => EscortListingAjax::PRA_LISTING_ACTION_NAME,
				'loadMoreEscortListingNonce'  => wp_create_nonce( EscortListingAjax::PRA_LISTING_ACTION_NAME ),
				'addCommentNonce'             => wp_create_nonce( ProfileActions::PRA_ADD_COMMENT_ACTION_NAME ),
				'addCommentActionName'        => ProfileActions::PRA_ADD_COMMENT_ACTION_NAME,
				'addRefuteCommentActionName'  => ProfileActions::PRA_COMMENT_REFUTE_ACTION_NAME,
				'addRefuteCommentNonce'       => wp_create_nonce( ProfileActions::PRA_COMMENT_REFUTE_ACTION_NAME ),
				'updateUserInfoActionName'    => AccountActions::PRA_UPDATE_USER_INFO,
				'updateUserInfoNonce'         => wp_create_nonce( AccountActions::PRA_UPDATE_USER_INFO ),
				'changePasswordActionName'    => AccountActions::PRA_CHANGE_PASSWORD,
				'changePasswordNonce'         => wp_create_nonce( AccountActions::PRA_CHANGE_PASSWORD ),
				'deleteAllProfileActionName'  => AccountActions::PRA_DELETE_USER_ALL_PROFILE_ACTION_NAME,
				'deleteAllProfileNonce'       => wp_create_nonce( AccountActions::PRA_DELETE_USER_ALL_PROFILE_ACTION_NAME ),
				'freezeAccountActionName'     => AccountActions::PRA_ACCOUNT_FREEZE_ACTION_NAME,
				'freezeAccountNonce'          => wp_create_nonce( AccountActions::PRA_ACCOUNT_FREEZE_ACTION_NAME ),
				'unfreezeAccountActionName'   => AccountActions::PRA_ACCOUNT_UNFREEZE_ACTION_NAME,
				'unfreezeAccountNonce'        => wp_create_nonce( AccountActions::PRA_ACCOUNT_UNFREEZE_ACTION_NAME ),
				'loadMoreArchiveActionName'   => Actions::PRA_ARCHIVE_LOAD_MORE_ACTION_NAME,
				'loadMoreArchiveNonce'        => wp_create_nonce( Actions::PRA_ARCHIVE_LOAD_MORE_ACTION_NAME ),
				'generatePaymentOrderAction'  => PaymentOrders::PRA_GENERATE_PAYMENT_ORDER_ACTION_NAME,
				'generatePaymentOrderNonce'   => wp_create_nonce( PaymentOrders::PRA_GENERATE_PAYMENT_ORDER_ACTION_NAME ),
			]
		);

		wp_localize_script(
			'prague_build',
			'buildObject',
			[
				'imageUrl'    => get_stylesheet_directory_uri(),
				'assetsJsUrl' => get_stylesheet_directory_uri() . '/assets/js',
			]
		);
	}

	/**
	 * Add SVG and Webp formats to upload.
	 *
	 * @param array $mimes Mimes type.
	 *
	 * @return array
	 */
	public function add_support_mimes( array $mimes ): array {

		$mimes['webp'] = 'image/webp';
		$mimes['svg']  = 'image/svg+xml';

		return $mimes;
	}

	/**
	 * Change output custom logo.
	 *
	 * @param string $html HTML custom logo.
	 *
	 * @return string
	 */
	public function output_logo( string $html ): string {

		$home  = esc_url( get_bloginfo( 'url' ) );
		$class = 'logo-img';
		if ( has_custom_logo() ) {
			$logo    = wp_get_attachment_image(
				get_theme_mod( 'custom_logo' ),
				'full',
				false,
				[
					'class'    => 'logo',
					'itemprop' => 'logo',
				]
			);
			$content = $logo;

			$html = sprintf(
				'<a href="%s" class="%s" rel="home" itemprop="url">%s</a>',
				$home,
				$class,
				$content
			);

		}

		return $html;
	}

	/**
	 * Change excerpt length
	 *
	 * @param int $length Length.
	 *
	 * @return int
	 */
	public function change_excerpt_length( int $length ): int {

		return 10;
	}

	/**
	 * Change excerpt more.
	 *
	 * @param string $more More.
	 *
	 * @return string
	 */
	public function change_excerpt_more( string $more ): string {

		return '...';
	}

	/**
	 * Add WPBakery components.
	 *
	 * @return void
	 */
	public function add_bakery_components(): void {
		new EscortFilter();
		new EscortListing();
		new EscortListingAjax();
		new FAQ();
		new Partners();
		new ProductsBlock();
		new Advertising();
	}

	/**
	 * Change archive posts per page.
	 *
	 * @param WP_Query $query WP Query.
	 *
	 * @return void
	 */
	public function change_archive_posts_per_page( WP_Query $query ): void {
		if ( $query->is_archive() && $query->is_main_query() ) {
			$query->set( 'posts_per_page', self::PRA_ARCHIVE_POST_PER_PAGE );
		}
	}

	/**
	 * Deactivate admin bar.
	 *
	 * @return void
	 */
	public function disable_admin_bar(): void {
		if ( ! current_user_can( 'administrator' ) && ! is_admin() ) {
			show_admin_bar( false );
		}
	}

	/**
	 * Register widgets.
	 *
	 * @return void
	 */
	public function register_widgets(): void {
		register_sidebar(
			[
				'name'          => __( 'Languages', 'pragueescort' ),
				'id'            => 'languages_sidebar',
				'before_widget' => '<div class="widget">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			]
		);
	}
}
