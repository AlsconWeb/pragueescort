/* global slick, select2, intlTelInput, inputmask, buildObject, Fancybox */
/**
 * @param buildObject.imageUrl
 * @param buildObject.assetsJsUrl
 */

jQuery( document ).ready( function( $ ) {
	let gallerySlider = $( '.gallery-photo' );
	let slider = $( '.slider' );
	let menuTabsEl = $( '.tabs-nav a' );
	let editeEl = $( 'i.icon-edit' );
	let selectEl = $( 'select:not(.lang):not(.tag):not(.contact)' );
	let selectEllang = $( 'select.lang' );
	let selectElTags = $( 'select.tag' );
	let burgerMenu = $( '.burger-menu' );
	let arrowMenu = '<i class="icon-arrow-down"></i>';
	let userButton = $( 'header .icon-user' );

	// burger menu.
	burgerMenu.click( function() {
		$( this ).parents( 'header' ).toggleClass( 'open' );
	} );

	// click body hide menu
	$( document ).mouseup( function( e ) {
		var folder = $( 'header' );
		if ( ! folder.is( e.target ) && folder.has( e.target ).length === 0 ) {
			folder.removeClass( 'open' );
		}
	} );

	// add arrow menu.
	$( '.menu-item-has-children' ).each( function() {
		if ( $( this ).children( 'arrow-bottom' ).length === 0 ) {
			$( this ).children( '.sub-menu' ).before( arrowMenu );
			$( this ).find( '.icon-arrow-down' ).click( function() {
				console.log( 'click' );
				$( this ).toggleClass( 'open' );
				$( this ).next().slideToggle();
			} );
		}
	} );

	// datepicer
	if ( $( '.age' ).length ) {
		$( '.age' ).datepicker( {
			container: '.age-block',
			autoclose: true,
			startView: 1,
			minViewMode: 2,
			maxViewMode: 3,
		} );
	}

	if ( $( '.data' ).length ) {
		$( '.data' ).datepicker( {
			container: '.icon-calendar',
			autoclose: true,
		} );
	}

	// select 2 init.
	if ( selectEl.length ) {
		selectEl.select2( {
			minimumResultsForSearch: Infinity,
		} );
	}

	// add flag state.
	function formatState( state ) {
		if ( ! state.id ) return state.text;
		return '<img class=\'flag\' src=\'' + buildObject.imageUrl + '/assets/img/flags/' + state.id.toLowerCase() + '.svg\'>' + state.text;
	}

	// slelect 2 from language.

	if ( selectEllang.length ) {
		selectEllang.select2( {
			placeholder: 'English',
			templateResult: formatState,
			templateSelection: formatState,
			escapeMarkup: function( m ) {
				return m;
			},
			dropdownParent: $( '.select.language' )
		} );
	}

	// select 2 tags.
	if ( selectElTags.length ) {
		selectElTags.select2( {
			placeholder: 'Premium Escort',
			dropdownParent: $( '.select.tags' ),
			minimumResultsForSearch: Infinity
		} );
	}

	// add icon.
	function formatIcon( icon ) {
		if ( ! icon.id ) return icon.text;
		return '<i class="icon-' + icon.id.toLowerCase() + '"></i>' + icon.text;
	}

	// profile edit name.
	editeEl.click( function() {

		if ( $( this ).parent( '.name' ).hasClass( 'edit' ) ) {
			$( this ).parent( '.name' ).removeClass( 'edit' );
		} else {
			let name = $( this ).parent( '.name' ).find( 'h2' ).text().trim();
			$( this ).parent( '.name' ).find( 'input' ).val( name );
			$( this ).parent( '.name' ).addClass( 'edit' );
		}

	} );

	$( '.name-input' ).keyup( function() {

		let nameVal = $( this ).val();

		$( this ).parents( '.name' ).find( 'h2' ).text( nameVal );
		$( this ).parents( '.name' ).find( 'h2' ).attr( 'title', nameVal );
	} );

	$( '.icon-check' ).click( function() {
		$( this ).parents( '.name' ).removeClass( 'edit' );
	} );

	// tabs.
	menuTabsEl.click( function( e ) {
		e.preventDefault();
		const idEl = $( this ).attr( 'href' );
		$( '.tabs' ).children( '.tab-content' ).removeClass( 'active' );
		$( this ).parents( '.tabs-nav' ).children( 'li' ).removeClass( 'active' );
		$( this ).parent().addClass( 'active' );
		$( idEl ).addClass( 'active' );
	} );


	// gallery fancybox.
	Fancybox.bind( '[data-fancybox="gallery"]', {} );

	// gallery slider init.
	if ( gallerySlider.length ) {

		let arrowLeft = '<i class="icon-arrow-left"></i>';
		let arrowRight = '<i class="icon-arrow-right"></i>';

		gallerySlider.slick( {
			prevArrow: arrowLeft,
			nextArrow: arrowRight,
			dots: true,
			infinite: false,
			speed: 500,
			fade: true,
			cssEase: 'linear',
		} );

	}

	// add image in gallery.
	$( '#gallery-img' ).on( 'change', function( e ) {

		var files = e.target.files,
			filesLength = files.length;

		for ( var i = 0; i < filesLength; i++ ) {

			var f = files[ i ],
				fileType = files[ i ].type.split( '/' ).shift();
			var fileReader = new FileReader();

			if ( fileType === 'image' ) {
				let tmppath = URL.createObjectURL( files[ i ] );
				fileReader.onload = ( function( e ) {
					var file = e.target;
					$( '<div class="img">' +
						'<img class="imageThumb" src="' + tmppath + '" title="' + file.name + '"/>' +
						'<br/><i class="icon-close"></i>' +
						'</div>' ).insertAfter( '#gallery-img' );
					$( '.icon-close' ).click( function() {
						$( this ).parent( '.img' ).remove();
					} );
				} );
			}

			if ( fileType === 'video' ) {
				let tmppath = URL.createObjectURL( files[ i ] );

				$( '<div class="video">' +
					'<video width="400" height="300" controls="controls"><source src="' + tmppath + '"></video>' +
					'<i class="icon-close"></i>' +
					'</div>' ).insertAfter( '#gallery-img' );

				$( '.icon-close' ).click( function() {
					$( this ).parent( '.video' ).remove();
				} );
			}

			fileReader.readAsDataURL( f );
		}
	} );

	// slider items.
	if ( slider.length ) {
		slider.each( function() {
			let slideElLength = $( this ).data( 'length' );
			let arrowLeft = $( this ).parents( '.slider-block' ).find( '.icon-arrow-left' );
			let arrowRight = $( this ).parents( '.slider-block' ).find( '.icon-arrow-right' );
			$( this ).slick( {
				infinite: true,
				slidesToShow: slideElLength,
				prevArrow: arrowLeft,
				nextArrow: arrowRight,
				responsive: [
					{
						breakpoint: 1200,
						settings: {
							variableWidth: true
						}
					} ]
			} );
		} );

	}

	// add photo.
	$( '#photo' ).on( 'change', function( e ) {

		let tmppath = URL.createObjectURL( e.target.files[ 0 ] );

		if ( $( this ).next().find( 'img' ).length ) {
			$( this ).parent().find( 'img' ).attr( 'src', tmppath );
		} else {
			$( this ).parent().find( 'label' ).append( '<img src="' + tmppath + '" alt="">' ).addClass( 'edite' );
		}

	} );

	// contacts.
	$( '.add-contact' ).click( function( e ) {
		e.preventDefault();
		let itemLength = $( '.contact-item' ).length + 1;
		let input = '<div class="contact-item"><p class="icon-phone">Add phone</p><input type="tel" name="phones[' + itemLength + ']" id="number-' + itemLength + '" value=""><i class="icon-checked"></i><i class="icon-close"></i></div>';
		$( this ).before( input );

		addPhoneMask( '#number-' + itemLength );
	} );

	// input tel.
	function addPhoneMask( selector ) {

		let input = $( selector );

		input.intlTelInput( {
			autoHideDialCode: false,
			autoPlaceholder: 'aggressive',
			initialCountry: 'auto',
			separateDialCode: false,
			nationalMode: false,
			customPlaceholder: function( selectedCountryPlaceholder, selectedCountryData ) {
				let placeholder = selectedCountryPlaceholder.replace( /[0-9]/g, '*' );
				input.attr( 'data-mask', placeholder );
				return '+' + selectedCountryData.dialCode;
			},
			geoIpLookup: function( callback ) {
				$.get( 'https://ipinfo.io?token=67e4255eaef3e1', function() {
				}, 'jsonp' ).always( function( resp ) {
					let countryCode = ( resp && resp.country ) ? resp.country : '';
					callback( countryCode );
				} );
			},
			utilsScript: buildObject.assetsJsUrl + '/utils.js',
		} );

		setTimeout( () => {
			let countryData = input.intlTelInput( 'getSelectedCountryData' ),
				CountryCode = '+' + countryData.dialCode,
				countCountryCode = CountryCode.length;

			let maskAttr = input.attr( 'data-mask' ),
				number = maskAttr.substring( countCountryCode ),
				mask = CountryCode + number;

			input.inputmask( {
				'mask': mask,
				'placeholder': '-',
				oncomplete: function( elem ) {
					elem.target.setAttribute( 'area-valid', 'true' );
				},
				onincomplete: function( elem ) {
					elem.target.setAttribute( 'area-valid', 'false' );
				},
				oncleared: function( elem ) {
					elem.target.removeAttribute( 'area-valid' );
				},
			} );

			deletePhone( input.parents( '.contact-item' ) );
		}, 500 );
	}

	//remove phone
	function deletePhone( selector ) {
		$( selector ).find( '.icon-close' ).click( function( e ) {
			$( this ).parent().remove();
		} );
	}

	// table
	if ( $( window ).width() < 1200 ) {
		if ( $( '.vc_col-sm-7 .tariff-block .items' ) ) {
			$( '.vc_col-sm-7 .tariff-block .items' ).slick( {
				arrows: false,
				dots: true,
			} );
		}
	}

	if ( $( window ).width() < 992 ) {
		if ( $( '.vc_col-sm-12 .tariff-block .items' ) ) {
			$( '.vc_col-sm-12 .tariff-block .items' ).slick( {
				arrows: false,
				dots: true,
			} );
		}
	}

	// mobile
	if ( $( window ).width() < 768 ) {
		//profile accordion
		$( '.bottom-head .wpml-ls li' ).each( function() {
			var languagesItem = $( this );
			$( '#menu-main-menu' ).append( languagesItem );
		} );
		if ( $( '.vc_col-lg-3 .menu' ).length ) {
			$( '.profiles h3' ).click( function() {
				$( this ).parent().toggleClass( 'open' );
			} );
			userButton.click( function( e ) {
				e.preventDefault();
				$( '.vc_col-lg-3 .menu' ).toggleClass( 'open' );
				$( 'body' ).toggleClass( 'open-menu' );

			} );
		}
	}

	// seo more
	$( '.seo-more .vc_btn3' ).click( function( e ) {
		e.preventDefault();
		$( this ).parents( '.seo-block' ).find( '.vc_column-inner > .wpb_wrapper' ).toggleClass( 'open' );
	} );
} );
