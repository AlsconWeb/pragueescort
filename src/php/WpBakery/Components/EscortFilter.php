<?php
/**
 * Escort filter  class.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\WpBakery\Components;

use PRAGUE\Theme\Main;

/**
 * EscortFilter class file.
 */
class EscortFilter {

	/**
	 * Filter item gender.
	 *
	 * @var array
	 */
	public array $filter_gender;

	/**
	 * Filter item escort service.
	 *
	 * @var array
	 */
	public array $filter_service;

	/**
	 * EscortFilter construct
	 */
	public function __construct() {

		$this->filter_gender = [
			'all'    => __( 'All', 'pragueescort' ),
			'female' => __( 'Female', 'pragueescort' ),
			'male'   => __( 'Male', 'pragueescort' ),
		];

		$this->set_filter_service();

		add_shortcode( 'pra_filter_diamond', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'pra_filter_diamond', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Diamond filter', 'pragueescort' ),
			'description'             => esc_html__( 'Diamond filter', 'pragueescort' ),
			'base'                    => 'pra_filter_diamond',
			'category'                => __( 'PRA', 'pragueescort' ),
			'show_settings_on_create' => false,
			'icon'                    => PRA_URL_PATH . '/assets/icons/filter-solid.svg',
			'params'                  => [
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Custom css', 'pragueescort' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design options', 'pragueescort' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::PRA_DIR_PATH . '/WpBakery/Templates/EscortFilter/template.php';

		return ob_get_clean();
	}

	/**
	 * Set filter escort type from terms.
	 *
	 * @return void
	 */
	private function set_filter_service(): void {
		$term_escort_type = get_terms(
			[
				'taxonomy'   => 'service',
				'hide_empty' => false,
			]
		);

		if ( ! empty( $term_escort_type ) && ! is_wp_error( $term_escort_type ) ) {
			foreach ( $term_escort_type as $term ) {
				$this->filter_service[ $term->slug ] = $term->name;
			}
		}
	}
}
