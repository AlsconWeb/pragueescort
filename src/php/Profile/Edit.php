<?php
/**
 * Edit escort profile.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\Profile;

use DateTime;
use PRAGUE\Theme\Helpers;

/**
 * Edit class file.
 */
class Edit {

	/**
	 * Action and nonce add profile.
	 */
	public const PRA_ADD_PROFILE_ACTION_NAME = 'pra_add_profile';

	/**
	 * Action and nonce delete profile.
	 */
	public const PRA_DELETE_PROFILE_ACTION_NAME = 'pra_delete_profile';

	/**
	 * Action and nonce edit profile.
	 */
	public const PRA_EDIT_PROFILE_ACTION_NAME = 'pra_edit_profile';

	/**
	 * Action and nonce change profile.
	 */
	public const PRA_CHANGE_PROFILE_ACTION_NAME = 'pra_change_profile';

	/**
	 * Action and nonce delete image in gallery.
	 */
	public const PRA_DELETE_IMAGE_IN_GALLERY = 'pra_delete_image_in_gallery';

	/**
	 * Max photo size in MB.
	 */
	private const PRA_MAX_PHOTO_SIZE = 5 * 1024 * 1024;

	/**
	 * Edit construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {

		// add profile ajax.
		add_action( 'wp_ajax_' . self::PRA_ADD_PROFILE_ACTION_NAME, [ $this, 'add_profile' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_ADD_PROFILE_ACTION_NAME, [ $this, 'add_profile' ] );

		// delete profile ajax.
		add_action( 'wp_ajax_' . self::PRA_DELETE_PROFILE_ACTION_NAME, [ $this, 'delete_profile' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_DELETE_PROFILE_ACTION_NAME, [ $this, 'delete_profile' ] );

		// change profile ajax.
		add_action( 'wp_ajax_' . self::PRA_CHANGE_PROFILE_ACTION_NAME, [ $this, 'change_profile' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_CHANGE_PROFILE_ACTION_NAME, [ $this, 'change_profile' ] );

		// edite profile.
		add_action( 'admin_post_nopriv_' . self::PRA_EDIT_PROFILE_ACTION_NAME, [ $this, 'edit_profile' ] );
		add_action( 'admin_post_' . self::PRA_EDIT_PROFILE_ACTION_NAME, [ $this, 'edit_profile' ] );

		// delete image in gallery.
		add_action( 'wp_ajax_' . self::PRA_DELETE_IMAGE_IN_GALLERY, [ $this, 'delete_image' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_DELETE_IMAGE_IN_GALLERY, [ $this, 'delete_image' ] );
	}

	/**
	 * Ajax add profile.
	 *
	 * @return void
	 */
	public function add_profile(): void {
		$nonce = ! empty( $_POST['addUserNonce'] ) ? filter_var( wp_unslash( $_POST['addUserNonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_ADD_PROFILE_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Bad nonce code', 'pragueescort' ) ] );
		}

		$escort_id    = ! empty( $_POST['userID'] ) ? filter_var( wp_unslash( $_POST['userID'] ), FILTER_SANITIZE_NUMBER_INT ) : null;
		$profile_name = ! empty( $_POST['profileName'] ) ? filter_var( wp_unslash( $_POST['profileName'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( empty( $escort_id ) ) {
			wp_send_json_error( [ 'message' => __( 'User id undefined', 'pragueescort' ) ] );
		}

		if ( empty( $profile_name ) ) {
			wp_send_json_error( [ 'message' => __( 'Empty profile name.', 'pragueescort' ) ] );
		}

		$id_new_profile = $this->create_new_profile( $escort_id, $profile_name );

		if ( ! $id_new_profile ) {
			wp_send_json_error( [ 'message' => __( 'Could not create a new profile', 'pragueescort' ) ] );
		}

		setcookie( 'current_profile_id', $id_new_profile, 30 * DAY_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN );

		wp_send_json_success(
			[
				'message'      => __( 'Create new profile:' ) . 'Karina',
				'newProfileID' => $id_new_profile,
			]
		);
	}

	/**
	 * Create new profile.
	 *
	 * @param int    $user_id  User id.
	 * @param string $new_name New profile name.
	 *
	 * @return int
	 */
	private function create_new_profile( int $user_id, string $new_name ): int {
		$new_profile_args = [
			'post_type'   => 'escort',
			'post_title'  => $new_name,
			'post_status' => 'draft',
			'post_author' => $user_id,
		];

		$profile_id = wp_insert_post( $new_profile_args );

		if ( empty( $profile_id ) ) {
			return 0;
		}

		$this->set_tariff_plan( $profile_id );

		return $profile_id;

	}

	/**
	 * Set tariff plan in profile.
	 *
	 * @param int    $profile_id Profile ID.
	 * @param string $tariff     Tariff plan. Default value standard.
	 *
	 * @return bool
	 */
	public function set_tariff_plan( int $profile_id, string $tariff = 'standard' ): bool {
		$result = wp_set_post_terms( $profile_id, $tariff, 'escort-type' );

		if ( is_wp_error( $result ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Delete profile.
	 *
	 * @return void
	 */
	public function delete_profile(): void {
		$nonce = ! empty( $_POST['deleteProfileNonce'] ) ? filter_var( wp_unslash( $_POST['deleteProfileNonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_DELETE_PROFILE_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Bad nonce code', 'pragueescort' ) ] );
		}

		$profile_id = ! empty( $_POST['profileID'] ) ? filter_var( wp_unslash( $_POST['profileID'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $profile_id ) ) {
			wp_send_json_error( [ 'message' => __( 'Profile id is empty', 'pragueescort' ) ] );
		}

		$result = wp_delete_post( $profile_id, true );

		if ( ! $result ) {
			wp_send_json_error( [ 'message' => __( 'Error occurred the profile was not deleted', 'pragueescort' ) ] );
		}

		wp_send_json_success(
			[
				'message'   => __( 'Profile has been deleted', 'pragueescort' ),
				'profileID' => $result->ID,
			]
		);
	}

	/**
	 * Change profile.
	 *
	 * @return void
	 */
	public function change_profile(): void {
		$nonce = ! empty( $_POST['nonceChange'] ) ? filter_var( wp_unslash( $_POST['nonceChange'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_CHANGE_PROFILE_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Bad nonce code', 'pragueescort' ) ] );
		}

		$profile_id = ! empty( $_POST['profileID'] ) ? filter_var( wp_unslash( $_POST['profileID'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $profile_id ) ) {
			wp_send_json_error( [ 'message' => __( 'Profile id is empty', 'pragueescort' ) ] );
		}

		setcookie( 'current_profile_id', $profile_id, time() + 3600, COOKIEPATH, COOKIE_DOMAIN );

		wp_send_json_success(
			[
				'message'   => __( 'Change profile', 'pragueescort' ),
				'profileID' => $profile_id,
			]
		);
	}

	/**
	 * Edit profile.
	 *
	 * @return void
	 */
	public function edit_profile(): void {
		$errors = [];

		if ( ! wp_verify_nonce( filter_input( INPUT_POST, self::PRA_EDIT_PROFILE_ACTION_NAME, FILTER_SANITIZE_STRING ), self::PRA_EDIT_PROFILE_ACTION_NAME ) ) {
			wp_safe_redirect( filter_input( INPUT_POST, '_wp_http_referer', FILTER_SANITIZE_STRING ) );
			$errors[] = [ 'error' => __( 'Bad nonce', 'pragueescort' ) ];
			Helpers::set_cookie_errors( $errors );
			die();
		}

		$profile_id = ! empty( $_POST['active_profile'] ) ? filter_var( wp_unslash( $_POST['active_profile'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $profile_id ) ) {
			wp_safe_redirect( filter_input( INPUT_POST, '_wp_http_referer', FILTER_SANITIZE_STRING ) );
			$errors[] = [ 'error' => __( 'Profile id empty', 'pragueescort' ) ];
			Helpers::set_cookie_errors( $errors );
			die();
		}

		// phpcs:disable
		$main_photo = wp_unslash( $_FILES['main_photo'] );
		// phpcs:enable

		if ( ! empty( $main_photo['tmp_name'] ) && $main_photo['size'] < self::PRA_MAX_PHOTO_SIZE ) {
			$photo_set = $this->set_main_photo( $main_photo, $profile_id );
		}

		if ( ! empty( $main_photo ) && $main_photo['size'] > self::PRA_MAX_PHOTO_SIZE ) {
			$errors[] = [ 'error' => __( 'The maximum size of the uploaded photo is 5 megabytes', 'pragueescort' ) ];
		}

		if ( isset( $photo_set ) && ! $photo_set ) {
			$errors[] = [ 'error' => __( 'The photo was not uploaded to the server', 'pragueescort' ) ];
		}

		$content = [];

		$title = ! empty( $_POST['edit_profile']['profile_title'] ) ? filter_var( wp_unslash( $_POST['edit_profile']['profile_title'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( ! empty( $title ) ) {
			$content['post_title'] = $title;
			$content['ID']         = $profile_id;
		}

		$description = ! empty( $_POST['edit_profile']['description'] ) ? filter_var( wp_unslash( $_POST['edit_profile']['description'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( ! empty( $description ) ) {
			$content['post_content'] = $description;
			$content['ID']           = $profile_id;
		}

		if ( ! empty( $content ) ) {
			wp_update_post( $content );
		}

		// phpcs:disable
		$gallery_images = wp_unslash( $_FILES['media_gallery'] );
		// phpcs:enable

		$file_array = $this->create_gallery_array( $gallery_images );

		if ( ! empty( $file_array ) ) {
			$this->set_image_gallery( $file_array, $profile_id );
		}

		$data = filter_var_array(
			wp_unslash( $_POST['edit_profile'] ),
			[
				'pra_gender'            => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_hair_color'        => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_height'            => [
					'filter'  => FILTER_VALIDATE_INT,
					'options' => [
						'min_range' => 20,
						'max_range' => 120,
					],
				],
				'pra_date_birth'        => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_eye_color'         => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_weight'            => [
					'filter'  => FILTER_VALIDATE_INT,
					'options' => [
						'min_range' => 100,
						'max_range' => 200,
					],
				],
				'pra_ethnic_origin'     => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_breastcup'         => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_orientation'       => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_smoke'             => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_drink'             => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_shaved'            => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_cim'               => FILTER_VALIDATE_INT,
				'pra_golden'            => FILTER_VALIDATE_INT,
				'pra_anal_rimming_give' => FILTER_VALIDATE_INT,
				'pra_cof'               => FILTER_VALIDATE_INT,
				'pra_threesome'         => FILTER_VALIDATE_INT,
				'pra_anal_rimming'      => FILTER_VALIDATE_INT,
				'pra_swallow'           => FILTER_VALIDATE_INT,
				'pra_foot_fetish'       => FILTER_VALIDATE_INT,
				'pra_bdsm'              => FILTER_VALIDATE_INT,
				'pra_dfk'               => FILTER_VALIDATE_INT,
				'pra_domination'        => FILTER_VALIDATE_INT,
				'pra_submission'        => FILTER_VALIDATE_INT,
				'pra_anal'              => FILTER_VALIDATE_INT,
				'pra_half_hour'         => FILTER_VALIDATE_INT,
				'pra_one_hour'          => FILTER_VALIDATE_INT,
				'pra_two_hour'          => FILTER_VALIDATE_INT,
				'pra_three_hour'        => FILTER_VALIDATE_INT,
				'pra_night'             => FILTER_VALIDATE_INT,
				'pra_day'               => FILTER_VALIDATE_INT,
				'pra_duo_gir_friend'    => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_classic_sex'       => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_two_men'           => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_relax_massage'     => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_kissing'           => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_rimming'           => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_erotic_massage'    => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_sex_toys'          => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_blowjob'           => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_bathing_together'  => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_fingering'         => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_sixty_nine'        => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_oral'              => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_couple'            => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_extra_ball'        => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_licking'           => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_cum_body'          => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_handjob'           => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
				'pra_currency'          => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
			]
		);

		$birthday = new DateTime( $data['pra_date_birth'] );
		$today    = new DateTime();
		$age      = $today->diff( $birthday )->y;

		if ( $age > 18 ) {
			$errors[] = [ 'error' => __( 'You must be over 18 to access this page', 'pragueescort' ) ];
		}

		$this->set_profile_fields( $data, $profile_id );

		$service  = ! empty( $_POST['edit_profile']['service'] ) ? filter_var_array( wp_unslash( $_POST['edit_profile']['service'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : [];
		$language = ! empty( $_POST['edit_profile']['pra_language'] ) ? filter_var_array( wp_unslash( $_POST['edit_profile']['pra_language'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : [];
		$contacts = ! empty( $_POST['edit_profile']['pra_contact_value'] ) ? filter_var_array( wp_unslash( $_POST['edit_profile']['pra_contact_value'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : [];

		$contacts_array = [];

		foreach ( $contacts as $contact ) {
			$contacts_array[] = [
				'pra_contact_value' => $contact,
			];
		}

		carbon_set_post_meta( $profile_id, 'pra_language', $language );

		$this->set_services( $profile_id, $service );

		carbon_set_post_meta( $profile_id, 'pra_contacts', $contacts_array );

		wp_safe_redirect( filter_input( INPUT_POST, '_wp_http_referer', FILTER_SANITIZE_STRING ) );
		die();
	}

	/**
	 * Set main photo in profile.
	 *
	 * @param array $file       global var $_FILE.
	 * @param int   $profile_id Escort profile id.
	 *
	 * @return bool
	 */
	private function set_main_photo( array $file, int $profile_id ): bool {
		$upload_overrides = [ 'test_form' => false ];
		$uploaded_file    = wp_handle_upload( $file, $upload_overrides );

		if ( ! isset( $uploaded_file['error'] ) ) {
			$attachment_id = wp_insert_attachment(
				[
					'guid'           => $uploaded_file['url'],
					'post_mime_type' => $uploaded_file['type'],
					'post_title'     => preg_replace( '/\.[^.]+$/', '', $file['name'] ),
					'post_content'   => '',
					'post_status'    => 'inherit',
				],
				$uploaded_file['file']
			);

			if ( $attachment_id && ! is_wp_error( $attachment_id ) ) {
				set_post_thumbnail( $profile_id, $attachment_id );
			}

			return true;
		}

		return false;
	}

	/**
	 * Create file array.
	 *
	 * @param array $file $_FILE array.
	 *
	 * @return array
	 */
	private function create_gallery_array( array $file ): array {
		$file_array = [];

		foreach ( $file['name'] as $key => $value ) {
			if ( UPLOAD_ERR_OK === $file['error'][ $key ] ) {
				$file_array[] = [
					'name'     => $value,
					'type'     => $file['type'][ $key ],
					'tmp_name' => $file['tmp_name'][ $key ],
					'error'    => $file['error'][ $key ],
					'size'     => $file['size'][ $key ],
				];
			}
		}

		return $file_array;
	}

	/**
	 * Set image gallery in profile.
	 *
	 * @param array $images     Images array.
	 * @param int   $profile_id Post id escort post type.
	 *
	 * @return void
	 */
	private function set_image_gallery( array $images, int $profile_id ): void {
		$media_gallery = [];
		$gallery       = carbon_get_post_meta( $profile_id, 'pra_media_gallery' );

		foreach ( $images as $value ) {

			$upload_overrides = [ 'test_form' => false ];
			$uploaded_file    = wp_handle_upload( $value, $upload_overrides );

			if ( ! isset( $uploaded_file['error'] ) ) {
				$attachment_id = wp_insert_attachment(
					[
						'guid'           => $uploaded_file['url'],
						'post_mime_type' => $uploaded_file['type'],
						'post_title'     => preg_replace( '/\.[^.]+$/', '', $value['name'] ),
						'post_content'   => '',
						'post_status'    => 'inherit',
					],
					$uploaded_file['file']
				);

				$media_gallery[] = $attachment_id;
			}
		}

		$media_gallery = array_merge( $media_gallery, $gallery );

		carbon_set_post_meta( $profile_id, 'pra_media_gallery', $media_gallery );
	}

	/**
	 * Set main data field in profile.
	 *
	 * @param array $data       Input data.
	 * @param int   $profile_id Profile id.
	 *
	 * @return void
	 */
	private function set_profile_fields( array $data, int $profile_id ): void {
		if ( empty( $data ) ) {
			return;
		}

		foreach ( $data as $key => $value ) {
			if ( ! empty( $value ) ) {
				carbon_set_post_meta( $profile_id, $key, $value );
			}
		}
	}

	/**
	 * Set service.
	 *
	 * @param int   $profile_id Profile ID.
	 * @param array $service    Service profile.
	 *
	 * @return bool
	 */
	public function set_services( int $profile_id, array $service ): bool {
		$services_id = [];
		foreach ( $service as $value ) {
			$services_id[] = get_term_by( 'slug', $value, 'service' )->term_id;
		}

		$result = wp_set_post_terms( $profile_id, $services_id, 'service' );

		if ( is_wp_error( $result ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Delete image in gallery.
	 *
	 * @return void
	 */
	public function delete_image(): void {
		$nonce = ! empty( $_POST['deleteImageNonce'] ) ? filter_var( wp_unslash( $_POST['deleteImageNonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_DELETE_IMAGE_IN_GALLERY ) ) {
			wp_send_json_error( [ 'message' => __( 'Bad nonce code', 'pragueescort' ) ] );
		}

		$profile_id = ! empty( $_POST['profileID'] ) ? filter_var( wp_unslash( $_POST['profileID'] ), FILTER_SANITIZE_NUMBER_INT ) : null;
		$image_id   = ! empty( $_POST['imageID'] ) ? filter_var( wp_unslash( $_POST['imageID'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $profile_id ) ) {
			wp_send_json_error( [ 'message' => __( 'Unknown id profile', 'pragueescort' ) ] );
		}

		if ( empty( $image_id ) ) {
			wp_send_json_error( [ 'message' => __( 'Unknown image id', 'pragueescort' ) ] );
		}

		$gallery = carbon_get_post_meta( $profile_id, 'pra_media_gallery' );

		$index = array_search( $image_id, $gallery, true );

		unset( $gallery[ $index ] );

		carbon_set_post_meta( $profile_id, 'pra_media_gallery', $gallery );

		$result = wp_delete_attachment( $image_id, true );

		if ( $result ) {
			wp_send_json_success( [ 'imageID' => $image_id ] );
		}

		wp_send_json_error( [ 'massage' => __( 'The picture has not been deleted from the server' ) ] );
	}
}
