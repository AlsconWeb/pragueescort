<?php
/**
 * Partners template.
 *
 * @package pragueescort/theme
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$partners  = vc_param_group_parse_atts( $atts['partners'] );
?>
<div class="partners">
	<h1><?php echo esc_html( $atts['title'] ); ?></h1>
	<?php if ( ! empty( $partners ) ) { ?>
		<div class="items">
			<?php
			foreach ( $partners as $partner ) {
				$image = wp_get_attachment_image( $partner['logo'], 'full' );
				$url   = vc_build_link( $partner['link'] );
				?>
				<div class="item">
					<a
						href="<?php echo esc_url( $url['url'] ?? '#' ); ?>"
						target="<?php echo esc_attr( $url['target'] ?? '' ); ?>"
						rel="<?php echo esc_attr( $url['rel'] ?? '' ); ?>"
						title="<?php echo esc_attr( $url['title'] ?? '' ); ?>">
						<?php
						if ( ! empty( $image ) ) {
							echo wp_kses_post( $image );
						}
						?>
					</a>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
</div>
