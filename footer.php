<?php
/**
 * Footer theme file.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\DynamicPages\ProfileComment;
use PRAGUE\Theme\DynamicPages\ProfilePage;

?>
<footer>
	<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<div class="vc_column-inner">
				<div class="wpb_wrapper">
					<div class="wpb_column vc_column_container">
						<div class="dfr">
							<?php
							the_custom_logo();

							if ( has_nav_menu( 'footer_menu_col_one' ) ) {
								wp_nav_menu(
									[
										'theme_location' => 'footer_menu_col_one',
										'menu_class'     => 'menu',
										'menu_id'        => '',
										'echo'           => true,
										'fallback_cb'    => 'wp_page_menu',
										'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									]
								);
							}

							if ( has_nav_menu( 'footer_menu_col_two' ) ) {
								wp_nav_menu(
									[
										'theme_location' => 'footer_menu_col_two',
										'menu_class'     => 'menu',
										'menu_id'        => '',
										'echo'           => true,
										'fallback_cb'    => 'wp_page_menu',
										'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									]
								);
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php

if ( ! is_user_logged_in() ) {
	get_template_part( 'template-part/modal/modal', 'auth' );
	get_template_part( 'template-part/modal/modal', 'verification' );
	get_template_part( 'template-part/modal/modal', 'years' );
}

if ( fq_is_page( ProfilePage::PAGE_PATH ) ) {
	get_template_part( 'template-part/modal/modal', 'profile_delete' );
	get_template_part( 'template-part/modal/modal', 'add_profile' );
}

if ( fq_is_page( ProfileComment::PAGE_PATH ) || is_singular( 'escort' ) ) {
	get_template_part( 'template-part/modal/modal', 'add_comment' );
	get_template_part( 'template-part/modal/modal', 'contacts' );
}

wp_footer();
?>
</body>
</html>

