<?php
/**
 * Search form template.
 *
 * @package pragueescort/theme
 */

?>
<div class="search">
	<form role="search" method="get" id="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input
				type="search"
				value="<?php echo get_search_query(); ?>"
				name="s"
				id="s"
				placeholder="<?php echo esc_html_e( 'search', 'pragueescort' ); ?>">
		<button class="icon-search"></button>
	</form>
</div>
