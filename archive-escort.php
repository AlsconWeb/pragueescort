<?php
/**
 * Archive escort template.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\Main;

global $wp_query;

$term_obj = get_queried_object();

$seo_title = carbon_get_term_meta( $term_obj->term_id, 'pra_seo_text_title' );
$seo_text  = carbon_get_term_meta( $term_obj->term_id, 'pra_seo_text' );

if ( empty( $seo_title ) ) {
	$seo_title = carbon_get_theme_option( 'pra_archive_title' );
}

if ( empty( $seo_text ) ) {
	$seo_text = carbon_get_theme_option( 'pra_archive_seo_text' );
}

get_header();
?>
	<section>
		<div class="vc_row wpb_row vc_row-fluid vc_row-o-content-middle vc_row-flex">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_row wpb_row vc_inner vc_row-fluid">
							<div class="wpb_column vc_column_container vc_col-sm-12">
								<div class="diamond-plan-block">

									<h2 class="status">
										<?php esc_html_e( 'All Escort', 'pragueescort' ); ?>
									</h2>
									<?php
									if ( have_posts() ) {
										?>
										<div class="standard-plan">
											<?php
											$i = 0;
											while ( have_posts() ) {
												the_post();

												$escort_id         = get_the_ID();
												$escort_type_class = wp_get_post_terms( $escort_id, 'escort-type' )[0]->slug;

												get_template_part(
													'template-part/single-escort/slider',
													'item',
													[
														'escort_id'  => $escort_id,
														'item_class' => $escort_type_class ?? 'standard',
													]
												);

												$i ++;

												if ( 10 === $i ) {
													$i = 0;
													get_template_part( 'template-part/advertising' );
												}
											}
											?>
										</div>
										<?php
										if ( $wp_query->max_num_pages > 1 ) {
											?>
											<a
													href="#"
													data-count="<?php echo esc_attr( Main::PRA_ARCHIVE_POST_PER_PAGE ); ?>"
													data-paged="<?php echo esc_attr( get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1 ); ?>"
													data-max_pages="<?php echo esc_attr( $wp_query->max_num_pages ); ?>"
													data-tax="<?php echo esc_attr( $wp_query->query_vars['taxonomy'] ?? '' ); ?>"
													data-term="<?php echo esc_attr( $wp_query->query_vars['term'] ?? '' ); ?>"
													class="button gradient load-more-archive">
												<?php esc_html_e( 'Look more' ); ?>
											</a>
											<span class="icon-more" style="display:none;">
													<?php esc_html_e( 'Loading more', 'pragueescort' ); ?>
												</span>
										<?php } ?>
									<?php } ?>
								</div>
							</div>
							<?php if ( ! empty( $seo_text ) ) { ?>
								<div class="wpb_column vc_column_container vc_col-sm-12 seo-block">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="wpb_column vc_column_container">
												<h1><?php echo esc_html( $seo_title ); ?></h1>
												<?php echo wp_kses_post( wpautop( $seo_text ) ); ?>
											</div>
											<div class="vc_btn3-container  button seo-more vc_btn3-inline">
												<a
														class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom"
														href="#" title="">
													<?php esc_attr_e( 'More', 'pragueescort' ); ?>
												</a>
											</div>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
