<?php
/**
 * Advertising template.
 *
 * @package pragueescort/theme
 */

$ads = carbon_get_theme_option( 'pr_ads_archive_page' );
if ( ! empty( $ads ) ) {
	$key = wp_rand( 0, count( $ads ) - 1 );
	shuffle( $ads );
	$image_desktop = wp_get_attachment_image( $ads[ $key ]['ads_desktop'], 'full' );
	$image_mobile  = wp_get_attachment_image( $ads[ $key ]['ads_mobile'], 'full' );
	?>
	<div class="wpb_column vc_column_container vc_col-sm-12 advertising">
		<?php
		echo sprintf(
			'<a href="%s"><span class="desktop">%s</span> <span class="mobile">%s</span></a>',
			esc_url( $ads[ $key ]['link'] ),
			wp_kses_post( $image_desktop ),
			wp_kses_post( $image_mobile ),
		);
		?>
	</div>
	<?php
}
