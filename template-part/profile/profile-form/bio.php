<?php
/**
 * Profile form bio.
 *
 * @package pragueescort/theme
 */


use PRAGUE\Theme\CarbonFields;

$profile_fields      = new CarbonFields();
$profile             = $args['profile'];
$profile_tariff_plan = wp_get_post_terms( $profile->active_profile_id, 'escort-type' );

$escort_service   = get_terms(
	[
		'taxonomy'   => 'service',
		'hide_empty' => false,
		'exclude'    => ( ! empty( $profile_tariff_plan[0] ) && 'standard' === $profile_tariff_plan[0]->slug ) ? [ 3 ] : [],
	]
);
$profile_service  = wp_get_post_terms( $profile->active_profile_id, 'service' );
$selected_service = [];

foreach ( $profile_service as $value ) {
	$selected_service[] = $value->slug;
}

?>
<div class="bio">
	<h2><?php esc_html_e( 'BIO', 'pragueescort' ); ?></h2>
	<div class="wrapper">
		<div class="select">
			<p><?php esc_html_e( 'Gender', 'pragueescort' ); ?></p>
			<label for="gender" class="hide"></label>
			<select name="edit_profile[pra_gender]" id="gender">
				<?php
				$gender = carbon_get_post_meta( $profile->active_profile_id, 'pra_gender' );
				foreach ( $profile_fields->gender as $key => $item ) {
					?>
					<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $gender, $key ); ?>>
						<?php echo esc_html( $item ); ?>
					</option>
				<?php } ?>
			</select>
		</div>
		<div class="select">
			<p><?php esc_html_e( 'Hair color', 'pragueescort' ); ?></p>
			<label for="hair-color" class="hide"></label>
			<input
					type="text"
					id="hair-color"
					name="edit_profile[pra_hair_color]"
					value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_hair_color' ) ?? '' ); ?>"
					placeholder="<?php esc_html_e( 'Color', 'pragueescort' ); ?>">
		</div>
		<div class="input">
			<p><?php esc_html_e( 'Height (cm)', 'pragueescort' ); ?></p>
			<label for="height" class="hide"></label>
			<input
					type="number"
					id="height"
					name="edit_profile[pra_height]"
					min="20"
					max="120"
					value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_height' ) ?? '' ); ?>"
					placeholder="60">
		</div>
		<div class="input age-block">
			<p><?php esc_html_e( 'Age', 'pragueescort' ); ?></p>
			<label for="date-birth" class="hide"></label>
			<input
					class="age"
					name="edit_profile[pra_date_birth]"
					id="date-birth"
					value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_date_birth' ) ?? '' ); ?>"
					type="text">
		</div>
		<div class="input">
			<p><?php esc_html_e( 'Eye color', 'pragueescort' ); ?></p>
			<label for="eye-color" class="hide"></label>
			<input
					type="text"
					id="eye-color"
					name="edit_profile[pra_eye_color]"
					value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_eye_color' ) ?? '' ); ?>"
					placeholder="<?php esc_html_e( 'Color', 'pragueescort' ); ?>">
		</div>
		<div class="input">
			<p><?php esc_html_e( 'Weight (cm)', 'pragueescort' ); ?></p>
			<label for="weight" class="hide"></label>
			<input
					type="number"
					id="weight"
					name="edit_profile[pra_weight]"
					min="100"
					max="200"
					value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_weight' ) ?? '' ); ?>"
					placeholder="167">
		</div>
		<div class="select">
			<p><?php esc_html_e( 'Ethnic Origin', 'pragueescort' ); ?></p>
			<label for="ethnic-origin" class="hide"></label>
			<select name="edit_profile[pra_ethnic_origin]" id="ethnic-origin">
				<?php
				$ethnic_origin = carbon_get_post_meta( $profile->active_profile_id, 'pra_ethnic_origin' );
				foreach ( $profile_fields->ethnic_origin as $key => $item ) {
					?>
					<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $ethnic_origin, $key ); ?>>
						<?php echo esc_html( $item ); ?>
					</option>
				<?php } ?>
			</select>
		</div>
		<div class="select">
			<p><?php esc_html_e( 'Breast cup', 'pragueescort' ); ?></p>
			<label for="breast-cup" class="hide"></label>
			<input
					type="text"
					id="breast-cup"
					name="edit_profile[pra_breastcup]"
					value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_breastcup' ) ?? '' ); ?>"
					placeholder="3">
		</div>
		<div class="select">
			<p><?php esc_html_e( 'Orientation', 'pragueescort' ); ?></p>
			<label for="orientation" class="hide"></label>
			<select name="edit_profile[pra_orientation]" id="orientation">
				<?php
				$orientation = carbon_get_post_meta( $profile->active_profile_id, 'pra_orientation' );
				foreach ( $profile_fields->orientation as $key => $item ) {
					?>
					<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $orientation, $key ); ?>>
						<?php echo esc_html( $item ); ?>
					</option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="wrapper">
		<div class="radio-buttons w_two">
			<p><?php esc_html_e( 'Smoke', 'pragueescort' ); ?></p>
			<div class="wrap">
				<?php
				$i     = 0;
				$smoke = carbon_get_post_meta( $profile->active_profile_id, 'pra_smoke' );
				foreach ( $profile_fields->smoke as $key => $item ) {
					?>
					<div class="radio-button">
						<input
								id="smoke-<?php echo esc_attr( $key ); ?>"
								type="radio"
								name="edit_profile[pra_smoke]"
								value="<?php echo esc_attr( $key ); ?>"
							<?php checked( $smoke, $key ); ?>>
						<label for="smoke-<?php echo esc_attr( $key ); ?>">
							<?php echo esc_html( $item ); ?>
						</label>
					</div>
					<?php
					$i ++;
				}
				?>
			</div>
		</div>
		<div class="radio-buttons w_two">
			<p><?php esc_html_e( 'Drink', 'pragueescort' ); ?></p>
			<div class="wrap">
				<?php
				$j     = 0;
				$drink = carbon_get_post_meta( $profile->active_profile_id, 'pra_drink' );
				foreach ( $profile_fields->drink as $key => $item ) {
					?>
					<div class="radio-button">
						<input
								id="drink-<?php echo esc_attr( $key ); ?>"
								type="radio"
								value="<?php echo esc_attr( $key ); ?>"
								name="edit_profile[pra_drink]"
							<?php checked( $drink, $key ); ?>>
						<label for="drink-<?php echo esc_attr( $key ); ?>">
							<?php echo esc_html( $item ); ?>
						</label>
					</div>
					<?php
					$j ++;
				}
				?>
			</div>
		</div>
		<div class="radio-buttons w_two">
			<p><?php esc_html_e( 'Shaved', 'pragueescort' ); ?></p>
			<div class="wrap">
				<?php
				$o      = 0;
				$shaved = carbon_get_post_meta( $profile->active_profile_id, 'pra_shaved' );
				foreach ( $profile_fields->shaved as $key => $item ) {
					?>
					<div class="radio-button">
						<input
								id="shaved-<?php echo esc_attr( $key ); ?>"
								type="radio"
								value="<?php echo esc_attr( $key ); ?>"
								name="edit_profile[pra_shaved]"
							<?php checked( $shaved, $key ); ?>>
						<label for="shaved-<?php echo esc_attr( $key ); ?>">
							<?php echo esc_html( $item ); ?>
						</label>
					</div>
					<?php
					$o ++;
				}
				?>
			</div>
		</div>
	</div>
	<div class="wrapper">
		<div class="select tags">
			<p><?php esc_html_e( 'Service', 'pragueescort' ); ?></p>
			<label for="service" class="hide"></label>
			<?php if ( ! empty( $escort_service ) ) { ?>
				<select
						class="tag"
						name="edit_profile[service][]"
						id="service"
						multiple="multiple">
					<?php
					foreach ( $escort_service as $item ) {
						?>
						<option
								value="<?php echo esc_attr( $item->slug ); ?>"
							<?php echo in_array( $item->slug, $selected_service, true ) ? 'selected' : ''; ?>>
							<?php echo esc_html( $item->name ); ?>
						</option>
					<?php } ?>
				</select>
			<?php } ?>
		</div>
		<div class="select language">
			<p><?php esc_html_e( 'Languages', 'pragueescort' ); ?></p>
			<label for="language" class="hide"></label>
			<select
					name="edit_profile[pra_language][]"
					class="lang"
					id="language"
					multiple="multiple">
				<?php
				$language = carbon_get_post_meta( $profile->active_profile_id, 'pra_language' );
				foreach ( CarbonFields::LIST_LANGUAGE as $key => $item ) {
					?>
					<option
							value="<?php echo esc_attr( $key ); ?>"
						<?php echo in_array( $key, $language, true ) ? 'selected' : ''; ?>>
						<?php echo esc_html( $item ); ?>
					</option>
				<?php } ?>
			</select>
		</div>
	</div>
	<hr class="sline">
</div>
