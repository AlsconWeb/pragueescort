<?php
/**
 * Slider vip template part.
 *
 * @package pragueescort/theme
 */

$execute = $args['execute'];

$arg_vip = [
	'post_type'      => 'escort',
	'posts_per_page' => 10,
	'order'          => 'DESC',
	'orderby'        => 'rand',
	'post__not_in'   => $execute->exclude_id,
	'tax_query'      => [
		[
			'taxonomy' => 'escort-type',
			'field'    => 'slug',
			'terms'    => 'vip',
		],
	],
];

$query_vip = new WP_Query( $arg_vip );

if ( is_404() ) {
	$data_slider = 5;
} else {
	$data_slider = 4;
}
?>
<div class="slider-block">
	<h2 class="status vip"><?php esc_html_e( 'VIP', 'pragueescort' ); ?></h2>
	<?php if ( $query_vip->have_posts() ) { ?>
		<a class="button blue" href="#"><?php esc_html_e( 'See all', 'pragueescort' ); ?></a>
		<i class="icon-arrow-left"></i>
		<i class="icon-arrow-right"></i>
		<div class="slider" data-length="<?php echo esc_html( $data_slider ); ?>">
			<?php
			while ( $query_vip->have_posts() ) {
				$query_vip->the_post();
				$escort_id = get_the_ID();

				get_template_part(
					'template-part/single-escort/slider',
					'item',
					[
						'escort_id'  => $escort_id,
						'item_class' => 'vip',
					]
				);
			}
			wp_reset_postdata();
			?>
		</div>
	<?php } ?>
</div>
