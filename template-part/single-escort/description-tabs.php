<?php
/**
 * Description tabs template part.
 *
 * @package pragueescort/theme
 */

get_template_part( 'template-part/single-escort/tab/tab', 'nav' );

$escort_id = $args['escort_id'];
?>
<div class="tabs">
	<?php

	get_template_part( 'template-part/single-escort/tab/tab', 'bio' );
	get_template_part( 'template-part/single-escort/tab/tab', 'services' );
	get_template_part( 'template-part/single-escort/tab/tab', 'price' );
	get_template_part( 'template-part/single-escort/tab/tab', 'comments', [ 'escort_id' => $escort_id ] );

	?>
</div>
