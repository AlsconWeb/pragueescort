<?php
/**
 * Archives and terms actions.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\Archives;

use PRAGUE\Theme\Main;

/**
 * Actions class file.
 */
class Actions {
	/**
	 * Action and nonce load more.
	 */
	public const PRA_ARCHIVE_LOAD_MORE_ACTION_NAME = 'pra_archive_load_more';

	/**
	 * Actions construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and action.
	 *
	 * @return void
	 */
	private function init(): void {
		// archive load more ajax.
		add_action( 'wp_ajax_' . self::PRA_ARCHIVE_LOAD_MORE_ACTION_NAME, [ $this, 'load_more_archive' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_ARCHIVE_LOAD_MORE_ACTION_NAME, [ $this, 'load_more_archive' ] );
	}

	/**
	 * Load more archive page.
	 *
	 * @return void
	 */
	public function load_more_archive(): void {
		$nonce = ! empty( $_POST['archiveNonce'] ) ? filter_var( wp_unslash( $_POST['archiveNonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_ARCHIVE_LOAD_MORE_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Bad nonce code', 'pragueescort' ) ] );
		}

		$count     = ! empty( $_POST['count'] ) ? filter_var( wp_unslash( $_POST['count'] ), FILTER_SANITIZE_NUMBER_INT ) : Main::PRA_ARCHIVE_POST_PER_PAGE;
		$paged     = ! empty( $_POST['paged'] ) ? filter_var( wp_unslash( $_POST['paged'] ), FILTER_SANITIZE_NUMBER_INT ) : 1;
		$tax_name  = ! empty( $_POST['taxName'] ) ? filter_var( wp_unslash( $_POST['taxName'] ), FILTER_SANITIZE_STRING ) : null;
		$term_slug = ! empty( $_POST['termSlug'] ) ? filter_var( wp_unslash( $_POST['termSlug'] ), FILTER_SANITIZE_STRING ) : null;

		$paged ++;

		$args = [
			'post_type'      => 'escort',
			'posts_per_page' => $count,
			'paged'          => $paged,
			'tax_query'      => [
				[
					'taxonomy' => $tax_name,
					'field'    => 'slug',
					'terms'    => $term_slug,
				],
			],
		];

		$archive_obj = new \WP_Query( $args );

		if ( $archive_obj->have_posts() ) {
			ob_start();
			$i = 0;
			while ( $archive_obj->have_posts() ) {
				$archive_obj->the_post();

				$escort_id         = get_the_ID();
				$escort_type_class = wp_get_post_terms( $escort_id, 'escort-type' )[0]->slug;

				get_template_part(
					'template-part/single-escort/slider',
					'item',
					[
						'escort_id'  => $escort_id,
						'item_class' => $escort_type_class ?? 'standard',
					]
				);

				$i ++;

				if ( 10 === $i ) {
					$i = 0;
					get_template_part( 'template-part/advertising' );
				}
			}
			wp_reset_postdata();

			wp_send_json_success(
				[
					'html'  => ob_get_clean(),
					'paged' => $paged,
				]
			);
		}
	}
}
