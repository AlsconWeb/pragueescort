<?php
/**
 * Profile content from.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\Notification\NotificationUser;
use PRAGUE\Theme\Profile\Edit;

$profile = $args['profile'];

if ( ! isset( $profile->active_profile_id ) || isset( $_COOKIE['current_profile_id'] ) ) {
	$profile->active_profile_id = filter_var( wp_unslash( $_COOKIE['current_profile_id'] ), FILTER_SANITIZE_NUMBER_INT );
	$active_profile             = $profile->active_profile_id;
} else {
	$active_profile = $profile->active_profile_id;
}


?>
<div class="wpb_column vc_column_container vc_col-xs-12 vc_col-sm-7 vc_col-md-8 vc_col-lg-9">
	<div class="vc_column-inner">
		<div class="wpb_wrapper">
			<div class="wpb_column vc_column_container">
				<form
						id="profile-form"
						enctype="multipart/form-data"
						method="post"
						action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>">

					<?php
					if ( 'draft' === get_post_status( $active_profile ) ) {
						NotificationUser::profile_no_verified();
					}

					if ( get_user_meta( get_current_user_id(), 'pra_account_freeze', false ) ) {
						NotificationUser::account_is_freeze();
					}
					?>
					<?php
					get_template_part( 'template-part/profile/profile-form/description', null, [ 'profile' => $profile ] );
					get_template_part( 'template-part/profile/profile-form/gallery', null, [ 'profile' => $profile ] );
					get_template_part( 'template-part/profile/profile-form/bio', null, [ 'profile' => $profile ] );
					get_template_part( 'template-part/profile/profile-form/services', null, [ 'profile' => $profile ] );
					get_template_part( 'template-part/profile/profile-form/extra', null, [ 'profile' => $profile ] );
					get_template_part( 'template-part/profile/profile-form/price', null, [ 'profile' => $profile ] );

					wp_nonce_field( Edit::PRA_EDIT_PROFILE_ACTION_NAME, Edit::PRA_EDIT_PROFILE_ACTION_NAME )
					?>
					<input
							type="hidden"
							id="active-profile"
							name="active_profile"
							value="<?php echo esc_attr( $active_profile ); ?>">
					<input
							type="hidden"
							name="action"
							value="<?php echo esc_attr( Edit::PRA_EDIT_PROFILE_ACTION_NAME ); ?>">
					<input
							class="button dark"
							type="submit"
							value="<?php esc_html_e( 'Save changes', 'pragueescort' ); ?>">
				</form>
			</div>
		</div>
	</div>
</div>
