<?php
/**
 * DynamicPageBase class file.
 *
 * @package sws/theme
 */

namespace PRAGUE\Theme\DynamicPages;

use SitePress;

/**
 * Class DynamicPageBase
 */
abstract class DynamicPageBase {

	/**
	 * Page url, must be redefined in the extending class.
	 */
	protected const PAGE_PATH = '';

	/**
	 * Page title.
	 *
	 * @var string
	 */
	protected string $page_title = '';

	/**
	 * Execute header and footer page slug.
	 *
	 * @var string
	 */
	protected string $execute_slug = '';

	/**
	 * This is dynamic page.
	 *
	 * @var bool|null
	 */
	private ?bool $is_dynamic_page = null;

	/**
	 * Class constructor.
	 */
	public function __construct() {
		$this->init_hooks();
	}

	/**
	 * Init hooks.
	 */
	public function init_hooks(): void {
		if ( ! $this->is_dynamic_page() ) {
			return;
		}

		add_action( 'init', [ $this, 'dynamic_page' ], PHP_INT_MAX );
		add_action( 'wp_head', [ $this, 'inline_styles' ] );
		add_filter( 'icl_ls_languages', [ $this, 'icl_ls_languages' ] );
		add_filter( 'sws_dynamic_page', [ $this, 'sws_dynamic_page_filter' ] );
	}

	/**
	 * Determine if we are on the class page.
	 *
	 * @return bool
	 */
	protected function is_dynamic_page(): bool {
		global $sitepress;

		if ( null !== $this->is_dynamic_page ) {
			return $this->is_dynamic_page;
		}

		$uri = '';
		if ( isset( $_SERVER['REQUEST_URI'] ) ) {
			$uri = filter_var( wp_unslash( $_SERVER['REQUEST_URI'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS );
		}

		$path = wp_parse_url( $uri, PHP_URL_PATH );

		$this->is_dynamic_page = preg_match(
			'#(/.+)*?/' . trailingslashit( static::PAGE_PATH ) . '#',
			trailingslashit( $path ),
			$matches
		);

		if ( $this->is_dynamic_page && class_exists( SitePress::class ) ) {
			$lang = isset( $matches[1] ) ? trim( $matches[1], '/' ) : '';
			$sitepress->switch_lang( $lang );
		}

		return $this->is_dynamic_page;
	}

	/**
	 * Show class page on proper uri.
	 *
	 * @todo нужно сделать проверку на страницы.
	 */
	public function dynamic_page(): void {

		if ( ! empty( $this->execute_slug ) && self::is_dynamic_page( $this->execute_slug ) ) {
			$this->page();
		} else {

			if ( ! is_user_logged_in() || current_user_can( 'administrator' ) ) {
				wp_safe_redirect( get_bloginfo( 'url' ), 301 );
				exit;
			}

			$this->header();
			$this->page();
			$this->footer();
		}
		exit;
	}

	/**
	 * Show page.
	 */
	protected function page(): void {
		?>
		<section id="<?php echo esc_attr( static::PAGE_PATH ); ?>" class="dynamic-page">
			<?php
			$this->content();
			?>
		</section>
		<?php
	}

	/**
	 * Show page content.
	 */
	abstract protected function content(): void;

	/**
	 * Show header.
	 */
	private function header(): void {
		get_header();
	}

	/**
	 * Show footer.
	 */
	private function footer(): void {
		get_footer();
	}

	/**
	 * Print style inline.
	 *
	 * @return void
	 */
	public function inline_styles(): void {
	}

	/**
	 * Filter language switcher languages to insert class page url.
	 *
	 * @param array $languages Languages.
	 *
	 * @return array
	 */
	public function icl_ls_languages( array $languages ): array {
		foreach ( $languages as & $language ) {
			$language['url'] = trailingslashit( $language['url'] ) . static::PAGE_PATH . '/';
		}

		return $languages;
	}

	/**
	 * Is dynamic page filter.
	 *
	 * @param bool $is_dynamic_page The page is dynamic.
	 *
	 * @return bool
	 */
	public function sws_dynamic_page_filter( bool $is_dynamic_page ): bool {
		if ( $is_dynamic_page ) {
			return true;
		}

		return $this->is_dynamic_page();
	}
}
