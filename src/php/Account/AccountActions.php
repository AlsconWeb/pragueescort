<?php
/**
 * Account actions class.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\Account;

/**
 * AccountActions class file.
 */
class AccountActions {

	/**
	 * Action and nonce update user info.
	 */
	public const PRA_UPDATE_USER_INFO = 'pra_update_user_info';

	/**
	 * Action and nonce change password.
	 */
	public const PRA_CHANGE_PASSWORD = 'pra_change_password';

	/**
	 * Action and nonce delete user all profile.
	 */
	public const PRA_DELETE_USER_ALL_PROFILE_ACTION_NAME = 'pra_delete_all_profile';

	/**
	 * Action and nonce freeze account.
	 */
	public const PRA_ACCOUNT_FREEZE_ACTION_NAME = 'pra_account_freeze';

	/**
	 * Action and nonce freeze account.
	 */
	public const PRA_ACCOUNT_UNFREEZE_ACTION_NAME = 'pra_account_unfreeze';

	/**
	 * AccountActions construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init actions and hooks.
	 *
	 * @return void
	 */
	private function init(): void {

		// update user info.
		add_action( 'wp_ajax_' . self::PRA_UPDATE_USER_INFO, [ $this, 'update_user_info' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_UPDATE_USER_INFO, [ $this, 'update_user_info' ] );

		// change password.
		add_action( 'wp_ajax_' . self::PRA_CHANGE_PASSWORD, [ $this, 'change_password' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_CHANGE_PASSWORD, [ $this, 'change_password' ] );

		// delete user profile.
		add_action( 'wp_ajax_' . self::PRA_DELETE_USER_ALL_PROFILE_ACTION_NAME, [ $this, 'delete_user_profile' ] );
		add_action(
			'wp_ajax_nopriv_' . self::PRA_DELETE_USER_ALL_PROFILE_ACTION_NAME,
			[
				$this,
				'delete_user_profile',
			]
		);

		// account freeze.
		add_action( 'wp_ajax_' . self::PRA_ACCOUNT_FREEZE_ACTION_NAME, [ $this, 'account_freeze' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_ACCOUNT_FREEZE_ACTION_NAME, [ $this, 'account_freeze' ] );

		// account unfreeze.
		add_action( 'wp_ajax_' . self::PRA_ACCOUNT_UNFREEZE_ACTION_NAME, [ $this, 'account_unfreeze' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_ACCOUNT_UNFREEZE_ACTION_NAME, [ $this, 'account_unfreeze' ] );
	}

	/**
	 * Update user info.
	 *
	 * @return void
	 */
	public function update_user_info(): void {
		$nonce = ! empty( $_POST['nonceUserInfo'] ) ? filter_var( wp_unslash( $_POST['nonceUserInfo'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_UPDATE_USER_INFO ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'submit-field',
					'message'   => __( 'Bad nonce code', 'pragueescort' ),
				]
			);
		}

		$user_id = ! empty( $_POST['userID'] ) ? filter_var( wp_unslash( $_POST['userID'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;

		if ( empty( $user_id ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'submit-field',
					'message'   => __( 'User id is empty', 'pragueescort' ),
				]
			);
		}

		$new_email = ! empty( $_POST['newEmail'] ) ? filter_var( wp_unslash( $_POST['newEmail'] ), FILTER_SANITIZE_EMAIL ) : null;
		$new_phone = ! empty( $_POST['newPhone'] ) ? filter_var( wp_unslash( $_POST['newPhone'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( ! empty( $new_email ) ) {
			wp_update_user(
				[
					'ID'         => $user_id,
					'user_email' => $new_email,
				]
			);
		}

		if ( ! empty( $new_phone ) ) {
			update_user_meta( $user_id, 'phone', $new_phone );
		}

		if ( empty( $new_email ) && ! empty( $new_phone ) ) {
			wp_send_json_success(
				[ 'message' => __( 'Your phone has been updated', 'pragueescort' ) ]
			);
		} elseif ( ! empty( $new_email ) && empty( $new_phone ) ) {
			wp_send_json_success(
				[ 'message' => __( 'Your email has been updated', 'pragueescort' ) ]
			);
		} else {
			wp_send_json_error(
				[
					'fieldName' => 'submit-field',
					'message'   => __( 'The form was submitted blank', 'pragueescort' ),
				]
			);
		}
	}

	/**
	 * Change user password.
	 *
	 * @return void
	 */
	public function change_password(): void {
		$nonce = ! empty( $_POST['changePswdNonce'] ) ? filter_var( wp_unslash( $_POST['changePswdNonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_CHANGE_PASSWORD ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'pswd-submit',
					'message'   => __( 'Bad nonce code', 'pragueescort' ),
				]
			);
		}

		$user_id = ! empty( $_POST['userID'] ) ? filter_var( wp_unslash( $_POST['userID'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $user_id ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'pswd-submit',
					'message'   => __( 'User id is empty', 'pragueescort' ),
				]
			);
		}

		$old_password = ! empty( $_POST['oldPassword'] ) ? filter_var( wp_unslash( $_POST['oldPassword'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		$user            = get_user_by( 'id', $user_id );
		$hashed_password = $user->data->user_pass;

		$is_password_correct = wp_check_password( $old_password, $hashed_password );

		if ( ! $is_password_correct ) {
			wp_send_json_error(
				[
					'fieldName' => 'old-password',
					'message'   => __( 'Wrong password', 'pragueescort' ),
				]
			);
		}

		$new_password     = ! empty( $_POST['newPassword'] ) ? filter_var( wp_unslash( $_POST['newPassword'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;
		$confirm_password = ! empty( $_POST['confirmPassword'] ) ? filter_var( wp_unslash( $_POST['confirmPassword'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( trim( $new_password ) !== trim( $confirm_password ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'new-password',
					'message'   => __( 'The entered passwords do not match', 'pragueescort' ),
				]
			);
		}

		wp_set_password( $new_password, $user_id );
		wp_logout();

		wp_send_json_success(
			[
				'message' => __( 'New password has been set', 'pragueescort' ),
			]
		);
	}

	/**
	 * Delete user profile and all profiles.
	 *
	 * @return void
	 */
	public function delete_user_profile(): void {
		$nonce = ! empty( $_POST['deleteAllNonce'] ) ? filter_var( wp_unslash( $_POST['deleteAllNonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_DELETE_USER_ALL_PROFILE_ACTION_NAME ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'pswd-submit',
					'message'   => __( 'Bad nonce code', 'pragueescort' ),
				]
			);
		}

		$user_id = ! empty( $_POST['userID'] ) ? filter_var( wp_unslash( $_POST['userID'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $user_id ) ) {
			wp_send_json_error(
				[
					'message' => __( 'User id is empty', 'pragueescort' ),
				]
			);
		}

		$args = [
			'post_type'      => 'escort',
			'author'         => $user_id,
			'posts_per_page' => - 1,
			'post_status'    => 'any',
		];

		$post_query = new \WP_Query( $args );

		if ( $post_query->have_posts() ) {
			while ( $post_query->have_posts() ) {
				$post_query->the_post();

				wp_delete_post( get_the_ID(), true );
			}
			wp_reset_postdata();
		}

		if ( wp_delete_user( $user_id ) ) {
			wp_logout();
			wp_send_json_success( [ 'message' => __( 'all delete', 'pragueescort' ) ] );
		}

		wp_send_json_error(
			[
				'message' => __( 'The user has not been deleted', 'pragueescort' ),
			]
		);
	}

	/**
	 * Account freeze.
	 *
	 * @return void
	 */
	public function account_freeze(): void {
		$nonce = ! empty( $_POST['freezeNonce'] ) ? filter_var( wp_unslash( $_POST['freezeNonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_ACCOUNT_FREEZE_ACTION_NAME ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'pswd-submit',
					'message'   => __( 'Bad nonce code', 'pragueescort' ),
				]
			);
		}

		$user_id = ! empty( $_POST['userID'] ) ? filter_var( wp_unslash( $_POST['userID'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $user_id ) ) {
			wp_send_json_error(
				[
					'message' => __( 'User id is empty', 'pragueescort' ),
				]
			);
		}

		update_user_meta( $user_id, 'pra_account_freeze', true );

		$args = [
			'post_type'      => 'escort',
			'author'         => $user_id,
			'posts_per_page' => - 1,
			'post_status'    => 'publish',
		];

		$post_query = new \WP_Query( $args );

		if ( $post_query->have_posts() ) {
			while ( $post_query->have_posts() ) {
				$post_query->the_post();

				wp_update_post(
					[
						'ID'          => get_the_ID(),
						'post_status' => 'draft',
					]
				);
			}
			wp_reset_postdata();
		}

		wp_send_json_success( [ 'message' => __( 'Your account has been frozen all profiles have become non-public', 'pragueescort' ) ] );
	}

	public function account_unfreeze(): void {
		$nonce = ! empty( $_POST['unfreezeNonce'] ) ? filter_var( wp_unslash( $_POST['unfreezeNonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_ACCOUNT_UNFREEZE_ACTION_NAME ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'pswd-submit',
					'message'   => __( 'Bad nonce code', 'pragueescort' ),
				]
			);
		}

		$user_id = ! empty( $_POST['userID'] ) ? filter_var( wp_unslash( $_POST['userID'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $user_id ) ) {
			wp_send_json_error(
				[
					'message' => __( 'User id is empty', 'pragueescort' ),
				]
			);
		}

		update_user_meta( $user_id, 'pra_account_freeze', false );

		$args = [
			'post_type'      => 'escort',
			'author'         => $user_id,
			'posts_per_page' => - 1,
			'post_status'    => 'draft',
		];

		$post_query = new \WP_Query( $args );

		if ( $post_query->have_posts() ) {
			while ( $post_query->have_posts() ) {
				$post_query->the_post();

				wp_update_post(
					[
						'ID'          => get_the_ID(),
						'post_status' => 'publish',
					]
				);
			}
			wp_reset_postdata();
		}

		wp_send_json_success( [ 'message' => __( 'Your account has been unfrozen all profiles', 'pragueescort' ) ] );
	}
}
