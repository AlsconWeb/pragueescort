<?php
/**
 * Slider item.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\Helpers;

$escort_id = $args['escort_id'];
$class     = $args['item_class'];
$helpers   = new Helpers();
?>

<div class="item <?php echo esc_attr( $class ); ?>">
	<div class="img">
		<a class="link" href="<?php the_permalink(); ?>"></a>
		<?php
		if ( has_post_thumbnail( $escort_id ) ) {
			the_post_thumbnail( 'full' );
		} else {
			echo '<img src="https://via.placeholder.com/180x238" alt="">';
		}
		?>
		<div class="status <?php echo esc_attr( $class ); ?>"><?php echo esc_attr( $class ); ?></div>
		<div class="desc">
			<p class="weight">
				<?php esc_html_e( 'Weight', 'pragueescort' ); ?>
				<span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_weight' ) ) ); ?></span>
			</p>
			<p class="height">
				<?php esc_html_e( 'Height', 'pragueescort' ); ?>
				<span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_height' ) ) ); ?></span>
			</p>
			<p class="breasts">
				<?php esc_html_e( 'Breasts', 'pragueescort' ); ?>
				<span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_breastcup' ) ) ); ?></span>
			</p>
			<ul>
				<li>
					<?php esc_html_e( '30 min', 'pragueescort' ); ?>
					<span><?php echo esc_html( $helpers->get_extra_price( 'pra_half_hour', $escort_id ) ); ?></span>
				</li>
				<li>
					<?php esc_html_e( '1 hour', 'pragueescort' ); ?>
					<span><?php echo esc_html( $helpers->get_extra_price( 'pra_one_hour', $escort_id ) ); ?></span>
				</li>
				<li>
					<?php esc_html_e( '2 hour', 'pragueescort' ); ?>
					<span><?php echo esc_html( $helpers->get_extra_price( 'pra_two_hour', $escort_id ) ); ?></span>
				</li>
				<li>
					<?php esc_html_e( '3 hour', 'pragueescort' ); ?>
					<span><?php echo esc_html( $helpers->get_extra_price( 'pra_three_hour', $escort_id ) ); ?></span>
				</li>
				<li>
					<?php esc_html_e( 'Night', 'pragueescort' ); ?>
					<span><?php echo esc_html( $helpers->get_extra_price( 'pra_night', $escort_id ) ); ?></span>
				</li>
			</ul>
		</div>
	</div>
	<h3><?php the_title(); ?></h3>
	<p><?php echo esc_attr( Helpers::get_age( carbon_get_post_meta( $escort_id, 'pra_date_birth' ) ) ); ?></p>
</div>
