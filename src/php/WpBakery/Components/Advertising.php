<?php
/**
 * Advertising class.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\WpBakery\Components;

use PRAGUE\Theme\Main;

/**
 * Advertising class file.
 */
class Advertising {
	/**
	 * Advertising construct
	 */
	public function __construct() {

		add_shortcode( 'pra_ads', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'pra_ads', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Advertising', 'pragueescort' ),
			'description'             => esc_html__( 'Advertising', 'pragueescort' ),
			'base'                    => 'pra_ads',
			'category'                => __( 'PRA', 'pragueescort' ),
			'show_settings_on_create' => false,
			'icon'                    => PRA_URL_PATH . '/assets/icons/brush-solid.svg',
			'params'                  => [
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Custom css', 'pragueescort' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design options', 'pragueescort' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::PRA_DIR_PATH . '/WpBakery/Templates/Advertising/template.php';

		return ob_get_clean();
	}
}
