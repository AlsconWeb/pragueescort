<?php
/**
 * FAQ wp bakery component.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\WpBakery\Components;

use PRAGUE\Theme\Main;

/**
 * FAQ class file.
 */
class FAQ {
	/**
	 * FAQ construct.
	 */
	public function __construct() {
		add_shortcode( 'ai_faq', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ai_faq', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'FAQ', 'pragueescort' ),
			'description'             => esc_html__( 'FAQ', 'pragueescort' ),
			'base'                    => 'ai_faq',
			'category'                => __( 'PRA', 'pragueescort' ),
			'show_settings_on_create' => false,
			'icon'                    => PRA_URL_PATH . '/assets/icons/info-solid.svg',
			'params'                  => [
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Title', 'pragueescort' ),
					'param_name' => 'title',
				],
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Question', 'pragueescort' ),
					'param_name' => 'questions',
					'params'     => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Question', 'pragueescort' ),
							'param_name' => 'question',
						],
						[
							'type'       => 'textarea',
							'value'      => '',
							'heading'    => __( 'Answer', 'pragueescort' ),
							'param_name' => 'answer',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Custom css', 'pragueescort' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design options', 'pragueescort' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::PRA_DIR_PATH . '/WpBakery/Templates/FAQ/template.php';

		return ob_get_clean();
	}
}
