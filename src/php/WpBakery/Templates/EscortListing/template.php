<?php
/**
 * EscortListing template.
 *
 * @package pragueescort/theme
 */

$css_class    = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$escort_type  = $atts['escort_type'] ?? null;
$count_in_row = $atts['listing_in_row'] ?? 4;

// phpcs:disable
$gender = ! empty( $_GET['gender'] ) && 'all' !== $_GET['gender'] ? filter_var( wp_unslash( $_GET['gender'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

$count = $atts['listing_in_row'] ?? 5;


if ( $gender ) {
	$slider_arg = [
		'post_type'      => 'escort',
		'posts_per_page' => $count * $count,
		'post_status'    => 'publish',
		'order'          => 'DESC',
		'orderby'        => 'rand',
		'tax_query'      => [
			[
				'taxonomy' => 'escort-type',
				'field'    => 'slug',
				'terms'    => $escort_type,
			],
		],
		'meta_query'     => [
			[
				'key'   => '_pra_gender',
				'value' => $gender,
			],
		],
	];
} else {
	$slider_arg = [
		'post_type'      => 'escort',
		'posts_per_page' => $count * $count,
		'order'          => 'DESC',
		'orderby'        => 'rand',
		'post_status'    => 'publish',
		'tax_query'      => [
			[
				'taxonomy' => 'escort-type',
				'field'    => 'slug',
				'terms'    => $escort_type,
			],
		],
	];
}
// phpcs:enable

$slider_query = new WP_Query( $slider_arg );

$escort_count = $slider_query->found_posts;
if ( $escort_count % 2 !== 0 ) {
	$slider_query->posts = array_slice( $slider_query->posts, 0, - 1 );
	$slider_query->post_count --;
}

if ( ! empty( $escort_type ) && $slider_query->have_posts() ) {
	?>
	<div class="slider-block <?php echo esc_attr( $css_class ?? null ); ?>">
		<h2 class="status <?php echo esc_html( $escort_type ); ?>"><?php echo esc_html( strtoupper( $escort_type ) ); ?></h2>
		<a class="button blue" href="<?php echo esc_url( get_term_link( $escort_type, 'escort-type' ) ); ?>">
			<?php esc_html_e( 'See all', 'pragueescort' ); ?>
		</a>
		<i class="icon-arrow-left"></i>
		<i class="icon-arrow-right"></i>
		<div class="slider" data-length="<?php echo esc_attr( $count_in_row ); ?>">
			<?php
			$counter = 0;
			while ( $slider_query->have_posts() ) {
				$slider_query->the_post();
				$escort_id = get_the_ID();

				if ( $counter % 2 === 0 ) {
					echo '<div class="items">';
				}

				get_template_part(
					'template-part/single-escort/slider',
					'item',
					[
						'escort_id'  => $escort_id,
						'item_class' => $escort_type,
					]
				);
				$counter ++;

				if ( $counter % 2 === 0 ) {
					echo '</div>';
				}
			}
			wp_reset_postdata();
			?>
		</div>
	</div>
	<?php
}
