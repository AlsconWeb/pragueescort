<?php
/**
 * Send Admin email.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\Email;

use PRAGUE\Theme\Account\PaymentOrders;

/**
 * AdminEmail class file.
 */
class AdminEmail extends BaseEmail {
	/**
	 * Send register validation.
	 *
	 * @param array $arg Arguments.
	 *
	 * @return void
	 */
	public static function send_validation_email_register( array $arg ): void {
		ob_start();
		?>
		<tr>
			<td>
				<hr>
				<h3><?php esc_html_e( 'Welcome to Prague Escort!', 'pragueescort' ); ?></h3>
				<p><?php esc_html_e( 'Click the button below to complete verification:', 'pragueescort' ); ?></p>
				<a href="<?php echo esc_url( $arg['url'] ); ?>" class="btn">
					<?php esc_html_e( 'Verify Email', 'pragueescort' ); ?>
				</a>
				<hr>
				<p>
					<?php
					esc_html_e(
						'If you can\'t confirm by clicking the button above, please copy the address below to the browser address bar to confirm.',
						'pragueescort'
					);
					?>
				</p>
				<a href="<?php echo esc_url( $arg['url'] ); ?>" class="link">
					<?php echo esc_url( $arg['url'] ); ?>
				</a>
			</td>
		</tr>
		<?php
		$data = [
			'title'   => __( 'Confirm Your Registration', 'pragueescort' ),
			'content' => ob_get_clean(),
		];

		$content = self::get_content( $data );
		$subject = __( 'Confirm Your Registration', 'pragueescort' );
		$email   = $arg['email'];

		self::send( $subject, $content, $email );
	}

	/**
	 * Send new password in user email.
	 *
	 * @param array $arg Arguments.
	 *
	 * @return void
	 */
	public static function send_recover_password( array $arg ): void {
		ob_start();
		?>
		<tr>
			<td>
				<p><?php esc_html_e( 'You are receiving this email because you have requested a password recovery for your account.', 'pragueescort' ); ?></p>
				<p><?php esc_html_e( 'To create a new password, please follow the link below:', 'pragueescort' ); ?></p>
				<a href="<?php echo esc_url( $arg['url'] ); ?>" class="btn">
					<?php esc_html_e( 'Recover password', 'pragueescort' ); ?>
				</a>
				<a href="<?php echo esc_url( $arg['url'] ); ?>" class="link"><?php echo esc_url( $arg['url'] ); ?></a>
				<hr>
				<p><?php esc_html_e( 'If you did not request a password recovery, please ignore this message.', 'pragueescort' ); ?></p>
			</td>
		</tr>
		<?php
		$user_id   = get_user_by( 'email', $arg['email'] )->ID;
		$user_name = get_userdata( $user_id )->display_name;

		$data = [
			'title'   => __( 'Hello,', 'pragueescort' ) . ' ' . $user_name,
			'content' => ob_get_clean(),
		];

		$content = self::get_content( $data );
		$subject = __( 'Recover password on Prague Escort', 'pragueescort' );
		$email   = $arg['email'];

		self::send( $subject, $content, $email );
	}

	/**
	 * Send notification profile moderation done.
	 *
	 * @param array $arg Arguments.
	 *
	 * @return void
	 */
	public static function notify_profile_moderation_publish( array $arg ): void {
		ob_start();
		?>
		<tr>
			<td>
				<p><?php esc_html_e( 'Your profile has been moderated and will be published on the website.', 'pragueescort' ); ?></p>
				<p><?php esc_html_e( 'Profile name:', 'pragueescort' ); ?></p>
				<a href="<?php echo esc_url( $arg['url'] ); ?>" class="btn">
					<?php echo esc_html( $arg['profile_name'] ); ?>
				</a>
				<hr>
			</td>
		</tr>
		<?php
		$user_id   = $arg['user_id'];
		$user      = get_userdata( $user_id );
		$user_name = $user->display_name;
		$email     = $user->user_email;
		$data      = [
			'title'   => __( 'Hello,', 'pragueescort' ) . ' ' . $user_name,
			'content' => ob_get_clean(),
		];

		$content = self::get_content( $data );
		$subject = __( 'Profile has been moderated on Prague Escort', 'pragueescort' );

		self::send( $subject, $content, $email );
	}

	/**
	 * Send notification profile moderation done.
	 *
	 * @param array $arg Arguments.
	 *
	 * @return void
	 */
	public static function notify_new_order( array $arg ): void {
		ob_start();
		?>
		<tr>
			<td>
				<p><?php esc_html_e( 'A new invoice has been issued', 'pragueescort' ); ?></p>
				<p><?php esc_html_e( 'Profile name', 'pragueescort' ) . ' ' . $arg['profile_name']; ?></p>
				<p><?php esc_html_e( 'Order number', 'pragueescort' ) . ' ' . $arg['order_number']; ?></p>
				<a href="<?php echo esc_url( $arg['order_url'] ); ?>">
					<?php echo esc_html( __( 'Order edit url', 'pragueescort' ) ); ?>
				</a>
				<a href="<?php echo esc_url( $arg['profile_url'] ); ?>">
					<?php echo esc_html( __( 'Profile url', 'pragueescort' ) ); ?>
				</a>
				<hr>
			</td>
		</tr>
		<?php
		$email = get_option( 'admin_email' );
		$data  = [
			'title'   => __( 'New invoice generate', 'pragueescort' ),
			'content' => ob_get_clean(),
		];

		$content = self::get_content( $data );
		$subject = __( 'Prague Escort user generate new invoice', 'pragueescort' );

		self::send( $subject, $content, $email );
	}

	/**
	 * Send notification user about start tariff plan.
	 *
	 * @param array $arg Arguments.
	 *
	 * @return void
	 */
	public static function notify_update_user_tariff( array $arg ): void {
		ob_start();
		?>
		<tr>
			<td>
				<p><?php esc_html_e( 'Your tariff plan has been approved and started from the moment you receive this email', 'pragueescort' ); ?></p>
				<p><?php esc_html_e( 'Profile name:', 'pragueescort' ); ?></p>
				<a href="<?php echo esc_url( $arg['url'] ); ?>" class="btn">
					<?php echo esc_html( $arg['profile_name'] ); ?>
				</a>
				<hr>
			</td>
		</tr>
		<?php
		$user_id   = $arg['user_id'];
		$user      = get_userdata( $user_id );
		$user_name = $user->display_name;
		$email     = $user->user_email;
		$data      = [
			'title'   => __( 'Hello,', 'pragueescort' ) . ' ' . $user_name,
			'content' => ob_get_clean(),
		];

		$content = self::get_content( $data );
		$subject = __( 'Tariff has been activated for your profile', 'pragueescort' );

		self::send( $subject, $content, $email );
	}

	/**
	 * Send notification user about end tariff plan.
	 *
	 * @param array $arg Arguments.
	 *
	 * @return void
	 */
	public static function notify_set_standard_tariff( array $arg ): void {
		ob_start();
		?>
		<tr>
			<td>
				<p><?php esc_html_e( 'Your tariff has been switched to standard', 'pragueescort' ); ?></p>
				<p><?php esc_html_e( 'Profile name:', 'pragueescort' ); ?></p>
				<a href="<?php echo esc_url( $arg['url'] ); ?>" class="btn">
					<?php echo esc_html( $arg['profile_name'] ); ?>
				</a>
				<hr>
			</td>
		</tr>
		<?php
		$user_id   = $arg['user_id'];
		$user      = get_userdata( $user_id );
		$user_name = $user->display_name;
		$email     = $user->user_email;
		$data      = [
			'title'   => __( 'Hello,', 'pragueescort' ) . ' ' . $user_name,
			'content' => ob_get_clean(),
		];

		$content = self::get_content( $data );
		$subject = __( 'Tariff has been activated for your profile', 'pragueescort' );

		self::send( $subject, $content, $email );
	}

	/**
	 * Send notification user about end tariff plan.
	 *
	 * @param array $arg Arguments.
	 *
	 * @return void
	 */
	public static function notify_tariff_expiry( array $arg ): void {
		ob_start();
		?>
		<tr>
			<td>
				<?php
				echo sprintf(
					'<p>%s : %d %s </p>',
					esc_html( __( 'Your plan will end in', 'pragueescort' ) ),
					esc_html( PaymentOrders::PRA_DAYS_BEFORE_TARIFF_EXPIRY ),
					esc_html( __( 'days.', 'pragueescort' ) )
				);
				?>
				<p><?php esc_html_e( 'Profile name:', 'pragueescort' ); ?></p>
				<a href="<?php echo esc_url( $arg['url'] ); ?>" class="btn">
					<?php echo esc_html( $arg['profile_name'] ); ?>
				</a>
				<hr>
			</td>
		</tr>
		<?php
		$user_id   = $arg['user_id'];
		$user      = get_userdata( $user_id );
		$user_name = $user->display_name;
		$email     = $user->user_email;
		$data      = [
			'title'   => __( 'Hello,', 'pragueescort' ) . ' ' . $user_name,
			'content' => ob_get_clean(),
		];

		$content = self::get_content( $data );
		$subject = __( 'Your plan is expiring', 'pragueescort' );

		self::send( $subject, $content, $email );
	}
}
