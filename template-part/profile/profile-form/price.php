<?php
/**
 * Profile form price.
 *
 * @package pragueescort/theme
 */

$profile = $args['profile'];
?>
<div class="price-block">
	<h2><?php esc_html_e( 'PRICE', 'pragueescort' ); ?></h2>
	<div class="input">
		<p><?php esc_html_e( '30 min', 'pragueescort' ); ?></p>
		<label for="half-hour" class="hide"></label>
		<input
			type="number"
			min="0"
			value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_half_hour' ) ?? '' ); ?>"
			name="edit_profile[pra_half_hour]"
			id="half-hour"
			placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( '1 hour', 'pragueescort' ); ?></p>
		<label for="one-hour" class="hide"></label>
		<input
			type="number"
			min="0"
			value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_one_hour' ) ?? '' ); ?>"
			name="edit_profile[pra_one_hour]"
			id="one-hour"
			placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( '2 hour', 'pragueescort' ); ?></p>
		<label for="two-hour" class="hide"></label>
		<input
			type="number"
			min="0"
			value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_two_hour' ) ?? '' ); ?>"
			name="edit_profile[pra_two_hour]"
			id="two-hour"
			placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( '3 hour', 'pragueescort' ); ?></p>
		<label for="three-hour" class="hide"></label>
		<input
			type="number"
			value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_three_hour' ) ?? '' ); ?>"
			min="0"
			id="three-hour"
			name="edit_profile[pra_three_hour]"
			placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'Night', 'pragueescort' ); ?></p>
		<label for="night" class="hide"></label>
		<input
			type="number"
			id="night"
			min="0"
			value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_night' ) ?? '' ); ?>"
			name="edit_profile[pra_night]"
			placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'Twenty-four hours', 'pragueescort' ); ?></p>
		<label for="day" class="hide"></label>
		<input
			type="number"
			min="0"
			id="day"
			value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_day' ) ?? '' ); ?>"
			name="edit_profile[pra_day]"
			placeholder="0">
	</div>
</div>
