<?php
/**
 * Profile sidebar template.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\DynamicPages\ProfileComment;
use PRAGUE\Theme\DynamicPages\ProfilePage;
use PRAGUE\Theme\DynamicPages\ProfileSecurity;
use PRAGUE\Theme\DynamicPages\TariffPage;

$profile = $args['profile'];

$active_profile = ! empty( $_COOKIE['current_profile_id'] ) ? filter_var( wp_unslash( $_COOKIE['current_profile_id'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

$escort_ars = [
	'post_type'   => 'escort',
	'author'      => $profile->user_id,
	'post_status' => [ 'draft', 'publish' ],
	'order'       => 'ASC',
];

$escort_obj = new WP_Query( $escort_ars );
?>
<div class="wpb_column vc_column_container vc_col-xs-12 vc_col-sm-5 vc_col-md-4 vc_col-lg-3">
	<div class="vc_column-inner">
		<div class="wpb_wrapper">
			<div class="wpb_column vc_column_container">
				<div class="profiles">
					<h3><?php esc_html_e( 'Profile', 'pragueescort' ); ?></h3>
					<?php if ( $escort_obj->have_posts() ) { ?>
						<ul class="profiles-menu">
							<?php
							$i = 0;
							while ( $escort_obj->have_posts() ) {
								$escort_obj->the_post();
								$profile_id   = get_the_ID();
								$active_class = '';

								if ( (int) $active_profile === $profile_id ) {
									$profile->active_profile_id = $profile_id;

									$active_class = 'active';
								}

								if ( ( empty( $active_profile ) && 0 === $i ) ) {
									$profile->active_profile_id = $profile_id;

									$active_class = 'active';
								}

								?>
								<li
										class="<?php echo esc_attr( $active_class ); ?>"
										data-profile_id="<?php echo esc_attr( $profile_id ); ?>">
									<a href="#" class="change-profile"><?php the_title(); ?></a>
									<a
											class="icon-close delete-profile"
											data-profile_id="<?php echo esc_attr( $profile_id ); ?>"
											href="#">
									</a>
								</li>
								<?php
								$i ++;
							}
							wp_reset_postdata();
							?>
							<li>
								<a
										class="icon-plus"
										id="add-profile"
										data-user_id="<?php echo esc_attr( $profile->user_id ); ?>"
										href="#">
									<?php esc_html_e( 'Add profile', 'pragueescort' ); ?>
								</a>
							</li>
						</ul>
					<?php } ?>
				</div>

				<ul class="menu">
					<li class="<?php echo esc_attr( ProfilePage::PAGE_PATH === $profile->active_page ? 'active' : '' ); ?>">
						<a
								class="icon-edit"
								href="<?php echo esc_url( get_bloginfo( 'url' ) . '/' . ProfilePage::PAGE_PATH ); ?>">
							<?php esc_html_e( 'BIO', 'pragueescort' ); ?>
						</a>
					</li>

					<li class="<?php echo esc_attr( TariffPage::PAGE_PATH === $profile->active_page ? 'active' : '' ); ?>">
						<a
								class="icon-card"
								href="<?php echo esc_url( get_bloginfo( 'url' ) . '/' . TariffPage::PAGE_PATH ); ?>">
							<?php esc_html_e( 'Tarification', 'pragueescort' ); ?>
						</a>
					</li>
					<li class="<?php echo esc_attr( ProfileComment::PAGE_PATH === $profile->active_page ? 'active' : '' ); ?>">
						<a
								class="icon-message"
								href="<?php echo esc_url( get_bloginfo( 'url' ) . '/' . ProfileComment::PAGE_PATH ); ?>">
							<?php esc_html_e( 'Comments', 'pragueescort' ); ?>
						</a>
					</li>
					<li class="<?php echo esc_attr( ProfileSecurity::PAGE_PATH === $profile->active_page ? 'active' : '' ); ?>">
						<a
								class="icon-unlock"
								href="<?php echo esc_url( get_bloginfo( 'url' ) . '/' . ProfileSecurity::PAGE_PATH ); ?>">
							<?php esc_html_e( 'Security', 'pragueescort' ); ?>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
