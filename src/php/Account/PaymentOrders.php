<?php
/**
 * Payment orders.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\Account;

use Konekt\PdfInvoice\InvoicePrinter;
use PRAGUE\Theme\Email\AdminEmail;

/**
 * PaymentOrders class file.
 */
class PaymentOrders {

	/**
	 * Generate payment order action and nonce code.
	 */
	public const PRA_GENERATE_PAYMENT_ORDER_ACTION_NAME = 'pra_generate_payment_order';

	/**
	 * How many days in advance to notify that the tariff ends.
	 */
	public const PRA_DAYS_BEFORE_TARIFF_EXPIRY = 5;

	/**
	 * PaymentOrders construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action(
			'wp_ajax_' . self::PRA_GENERATE_PAYMENT_ORDER_ACTION_NAME,
			[
				$this,
				'generate_ajax_payment_order',
			]
		);
		add_action(
			'wp_ajax_nopriv_' . self::PRA_GENERATE_PAYMENT_ORDER_ACTION_NAME,
			[
				$this,
				'generate_ajax_payment_order',
			]
		);

		add_action( 'set_object_terms', [ $this, 'change_escort_type' ], 10, 4 );
		add_action( 'init', [ $this, 'add_schedule_cron' ] );
	}

	/**
	 * Generate payment order.
	 *
	 * @return void
	 */
	public function generate_ajax_payment_order(): void {
		$nonce = ! empty( $_POST['nonceOrder'] ) ? filter_var( wp_unslash( $_POST['nonceOrder'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_GENERATE_PAYMENT_ORDER_ACTION_NAME ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'submit-field',
					'message'   => __( 'Bad nonce code', 'pragueescort' ),
				]
			);
		}

		$user_id    = ! empty( $_POST['userID'] ) ? filter_var( wp_unslash( $_POST['userID'] ), FILTER_SANITIZE_NUMBER_INT ) : null;
		$price      = ! empty( $_POST['price'] ) ? filter_var( wp_unslash( $_POST['price'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;
		$tariff_id  = ! empty( $_POST['tariffID'] ) ? filter_var( wp_unslash( $_POST['tariffID'] ), FILTER_SANITIZE_NUMBER_INT ) : null;
		$profile_id = ! empty( $_POST['profileID'] ) ? filter_var( wp_unslash( $_POST['profileID'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $user_id ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'submit-field',
					'message'   => __( 'You are not registered or logged in with your account ', 'pragueescort' ),
				]
			);
		}

		$tariff_name = get_term( $tariff_id, 'escort-type' )->name;

		if ( empty( $tariff_name ) && is_wp_error( $tariff_name ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'submit-field',
					'message'   => __( 'There is no such tariff or an error has occurred.', 'pragueescort' ),
				]
			);
		}

		$last_order_count = get_option( 'pra_last_order_count', 1 );
		$last_order_count ++;

		$order_content = sprintf(
			'<p><b>%s :</b> %s</p><p><b>%s :</b> %s</p><p><b>%s :</b> %s</p><p><b>%s :</b> %s</p><p><b>%s :</b> %s</p>',
			esc_html( __( 'User Email', 'pragueescort' ) ),
			esc_html( get_user_by( 'id', $user_id )->user_email ),
			esc_html( __( 'Tariff name', 'pragueescort' ) ),
			esc_html( $tariff_name ),
			esc_html( __( 'Tariff price', 'pragueescort' ) ),
			esc_html( $price ),
			esc_html( __( 'Date order generate', 'pragueescort' ) ),
			esc_html( current_time( 'Y-m-d H:i:s' ) ),
			esc_html( __( 'Profile name and ID', 'pragueescort' ) ),
			esc_html( get_the_title( $profile_id ) . ' ' . $profile_id )
		);

		$order_data = [
			'post_title'   => sanitize_text_field( 'New Order # INV-' . $last_order_count ),
			'post_content' => $order_content,
			'post_status'  => 'draft',
			'post_author'  => $user_id,
			'post_type'    => 'orders',
		];

		$post_id = wp_insert_post( wp_slash( $order_data ) );

		if ( is_wp_error( $post_id ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'submit-field',
					'message'   => __( 'The invoice was not generated due to an error contact the administration', 'pragueescort' ),
				]
			);
		}

		$invoice         = new InvoicePrinter();
		$custom_logo_url = PRA_ROOT_PATH . '/assets/img/Logo.png';

		$invoice->setLogo( $custom_logo_url );

		$invoice->setType( 'Invoice' );
		$invoice->setReference( 'INV-' . $last_order_count );
		$invoice->setDate( gmdate( 'M dS ,Y', time() ) );
		$invoice->setTime( gmdate( 'h:i:s A', time() ) );
		$invoice->setDue( gmdate( 'M dS ,Y', strtotime( '+1 months' ) ) );
		$invoice->setFrom(
			[
				'Account holder: ' . carbon_get_theme_option( 'pra_account_holder' ),
				'IBAN: ' . carbon_get_theme_option( 'pra_iban' ),
				'Bank: ' . carbon_get_theme_option( 'pra_bank' ),
				'SWIFT: ' . carbon_get_theme_option( 'pra_swift' ),
				'Bank code: ' . carbon_get_theme_option( 'pra_bank_code' ),
			]
		);
		$invoice->setTo(
			[
				'',
			]
		);
		$invoice->addItem(
			$tariff_name,
			get_the_title( $profile_id ) . ' ' . $profile_id,
			1,
			'0Ks',
			$price,
			'0Ks',
			$price
		);
		$invoice->addTotal( 'Total', $price );
		$invoice->addTotal( 'VAT 0%', '0Ks' );
		$invoice->addTotal( 'Total due', $price, true );

		$invoice->addTitle( 'Payment description:' );
		$invoice->addParagraph( carbon_get_theme_option( 'pra_payment_description' ) );
		$invoice->addTitle( 'Important Notice' );
		$invoice->addParagraph( "Your order was successfully submited! As soonas the payment has been accepted by the bank Your order willbe completed. Keep in mind! To make yor order complete, please make the payment for the services as explained bellow. Transfer $price or an equivalent in EUR or USD to the fallowing bank account." );

		$invoice->changeLanguageTerm( 'from', 'Billing data' );
		$invoice->changeLanguageTerm( 'to', '' );
		$file_path = wp_upload_dir()['basedir'] . '/invoices/INV-' . $last_order_count . '.pdf';

		if ( ! is_dir( wp_upload_dir()['basedir'] . '/invoices/' ) ) {
			mkdir( wp_upload_dir()['basedir'] . '/invoices/', 0777, true );
		}

		$invoice->render( $file_path, 'F' );

		update_option( 'pra_last_order_count', $last_order_count );

		AdminEmail::notify_new_order(
			[
				'order_number' => 'INV-' . $last_order_count,
				'order_url'    => get_edit_post_link( $post_id ),
				'profile_url'  => get_the_permalink( $profile_id ),
				'profile_name' => get_the_title( $profile_id ),
			]
		);

		wp_send_json_success(
			[
				'invoice' => wp_upload_dir()['baseurl'] . '/invoices/INV-' . $last_order_count . '.pdf',
			]
		);
	}

	/**
	 * Set data changes escort type.
	 *
	 * @param int    $profile_id Term id.
	 * @param array  $terms      Terms ids.
	 * @param array  $tt_ids     Terms ids.
	 * @param string $taxonomy   Taxonomy name.
	 *
	 * @return void
	 */
	public function change_escort_type( int $profile_id, array $terms, array $tt_ids, string $taxonomy ): void {

		if ( 'escort-type' === $taxonomy ) {
			$escort_types = [
				'diamond'  => 5,
				'standard' => 7,
				'vip'      => 6,
			];

			if ( in_array( $terms[1], $escort_types, true ) ) {
				$current_date = gmdate( 'Y-m-d' );
				$future_date  = gmdate( 'Y-m-d', strtotime( $current_date . ' +30 days' ) );

				$user_id = get_post( $profile_id )->post_author;

				AdminEmail::notify_update_user_tariff(
					[
						'user_id'      => $user_id,
						'url'          => get_the_permalink( $profile_id ),
						'profile_name' => get_the_title( $profile_id ),
					]
				);

				update_post_meta( $profile_id, 'pra_end_date_tariff', $future_date );
			}
		}
	}

	/**
	 * Add cron job.
	 *
	 * @return void
	 */
	public function add_schedule_cron(): void {
		if ( ! wp_next_scheduled( 'pra_deactivate_tariff_plan' ) ) {
			wp_schedule_event( time(), 'daily', 'pra_deactivate_tariff_plan' );
		}

		if ( ! wp_next_scheduled( 'pra_before_tariff_expiry' ) ) {
			wp_schedule_event( time(), 'daily', 'pra_before_tariff_expiry' );
		}

		add_action( 'pra_deactivate_tariff_plan', [ $this, 'deactivate_tariff_plan' ] );
		add_action( 'pra_before_tariff_expiry', [ $this, 'tariff_expiry_reminder' ] );
	}

	/**
	 * Deactivation profile type to standard.
	 *
	 * @return void
	 */
	public function deactivate_tariff_plan(): void {
		$profile_ids = self::get_all_profiles_date( gmdate( 'Y-m-d' ) );

		if ( ! empty( $profile_ids ) ) {
			foreach ( $profile_ids as $id ) {
				wp_set_object_terms( $id, 7, 'escort-type' );

				$user_id = get_post( $id )->post_author;

				AdminEmail::notify_set_standard_tariff(
					[
						'user_id'      => $user_id,
						'url'          => get_the_permalink( $id ),
						'profile_name' => get_the_title( $id ),
					]
				);
			}
		}
	}

	/**
	 * Get all profiles id.
	 * Returns all profiles that expire due date.
	 *
	 * @param string $date Date.
	 *
	 * @return array
	 */
	private static function get_all_profiles_date( string $date ): array {
		global $wpdb;

		$table_name = $wpdb->prefix . 'postmeta';

		$result = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT post_id FROM  $table_name WHERE meta_key = %s AND meta_value = %s",
				'pra_end_date_tariff',
				$date
			)
		);

		if ( empty( $result ) ) {
			return [];
		}

		$ids = [];

		foreach ( $result as $item ) {
			$ids[] = $item->post_id;
		}

		return $ids;
	}

	/**
	 * Notifying the user that the tariff plan will end in X days
	 *
	 * @return void
	 */
	public function tariff_expiry_reminder(): void {
		$current_date = gmdate( 'Y-m-d' );

		$future_date = gmdate( 'Y-m-d', strtotime( $current_date . ' +' . self::PRA_DAYS_BEFORE_TARIFF_EXPIRY . ' days' ) );

		$ids = self::get_all_profiles_date( $future_date );

		if ( empty( $ids ) ) {
			return;
		}

		foreach ( $ids as $id ) {
			wp_set_object_terms( $id, 7, 'escort-type' );

			$user_id = get_post( $id )->post_author;

			AdminEmail::notify_tariff_expiry(
				[
					'user_id'      => $user_id,
					'url'          => get_the_permalink( $id ),
					'profile_name' => get_the_title( $id ),
				]
			);
		}
	}

}
