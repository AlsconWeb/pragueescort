<?php
/**
 * Carbon fields init
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme;

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * CarbonFields class file.
 */
class CarbonFields {

	/**
	 * Language list.
	 */
	public const LIST_LANGUAGE = [
		'af' => 'Afrikaans',
		'sq' => 'Albanian',
		'ar' => 'Arabic',
		'hy' => 'Armenian',
		'az' => 'Azerbaijani',
		'ba' => 'Bashkir',
		'be' => 'Belarusian',
		'bs' => 'Bosnian',
		'bg' => 'Bulgarian',
		'ca' => 'Catalan; Valencian',
		'ce' => 'Chechen',
		'zh' => 'Chinese',
		'co' => 'Corsican',
		'hr' => 'Croatian',
		'cs' => 'Czech',
		'da' => 'Danish',
		'nl' => 'Dutch',
		'en' => 'English',
		'et' => 'Estonian',
		'fj' => 'Fijian',
		'fi' => 'Finnish',
		'fr' => 'French',
		'ka' => 'Georgian',
		'de' => 'German',
		'el' => 'Greek, Modern',
		'he' => 'Hebrew (modern)',
		'hi' => 'Hindi',
		'hu' => 'Hungarian',
		'id' => 'Indonesian',
		'ga' => 'Irish',
		'is' => 'Icelandic',
		'it' => 'Italian',
		'ja' => 'Japanese',
		'kn' => 'Kannada',
		'kk' => 'Kazakh',
		'ky' => 'Kirghiz, Kyrgyz',
		'kg' => 'Kongo',
		'ko' => 'Korean',
		'ku' => 'Kurdish',
		'lb' => 'Luxembourgish, Letzeburgesch',
		'lt' => 'Lithuanian',
		'lv' => 'Latvian',
		'mk' => 'Macedonian',
		'mt' => 'Maltese',
		'mn' => 'Mongolian',
		'ne' => 'Nepali',
		'no' => 'Norwegian',
		'os' => 'Ossetian',
		'pl' => 'Polish',
		'pt' => 'Portuguese',
		'ro' => 'Romanian, Moldavian',
		'ru' => 'Russian',
		'sc' => 'Sardinian',
		'sr' => 'Serbian',
		'gd' => 'Scottish Gaelic',
		'sk' => 'Slovak',
		'sl' => 'Slovene',
		'es' => 'Spanish',
		'sv' => 'Swedish',
		'tg' => 'Tajik',
		'th' => 'Thai',
		'tk' => 'Turkmen',
		'tr' => 'Turkish',
		'tt' => 'Tatar',
		'uk' => 'Ukrainian',
		'uz' => 'Uzbek',
		'vi' => 'Vietnamese',
		'yi' => 'Yiddish',
	];

	/**
	 * Ethnic origin.
	 *
	 * @var array
	 */
	public array $ethnic_origin = [];

	/**
	 * Orientation.
	 *
	 * @var array
	 */
	public array $orientation = [];

	/**
	 * Gender.
	 *
	 * @var array
	 */
	public array $gender = [];

	/**
	 * Drink.
	 *
	 * @var array
	 */
	public array $drink = [];

	/**
	 * Shaved.
	 *
	 * @var array
	 */
	public array $shaved = [];

	/**
	 * Smoke.
	 *
	 * @var array
	 */
	public array $smoke = [];

	/**
	 * Currency.
	 *
	 * @var array
	 */
	public array $currency = [];

	/**
	 * Contact types
	 *
	 * @var array
	 */
	public array $contact_type = [];

	/**
	 * CarbonFields construct.
	 */
	public function __construct() {
		$this->init();

		$this->ethnic_origin = [
			'arabian'   => __( 'Arabian', 'pragueescort' ),
			'asian'     => __( 'Asian', 'pragueescort' ),
			'ebony'     => __( 'Ebony (Black)', 'pragueescort' ),
			'caucasian' => __( 'Caucasian (White)', 'pragueescort' ),
			'japanese'  => __( 'Japanese', 'pragueescort' ),
			'hispanic'  => __( 'Hispanic', 'pragueescort' ),
			'indian'    => __( 'Indian', 'pragueescort' ),
			'latin'     => __( 'Latin', 'pragueescort' ),
			'mongolia'  => __( 'Mongolia', 'pragueescort' ),
			'mixed'     => __( 'Mixed', 'pragueescort' ),
		];

		$this->orientation = [
			'straight'   => __( 'Straight', 'pragueescort' ),
			'bisexual'   => __( 'Bisexual', 'pragueescort' ),
			'lesbian'    => __( 'Lesbian', 'pragueescort' ),
			'homosexual' => __( 'Homosexual', 'pragueescort' ),
		];

		$this->gender = [
			'female' => __( 'Female', 'pragueescort' ),
			'male'   => __( 'Male', 'pragueescort' ),
		];

		$this->drink = [
			'no'        => __( 'No', 'pragueescort' ),
			'yes'       => __( 'Yes', 'pragueescort' ),
			'sometimes' => __( 'Sometimes', 'pragueescort' ),
		];

		$this->shaved = [
			'no'             => __( 'No', 'pragueescort' ),
			'yes'            => __( 'Yes', 'pragueescort' ),
			'erotic_haircut' => __( 'Erotic haircut', 'pragueescort' ),
		];

		$this->smoke = [
			'no'        => __( 'No', 'pragueescort' ),
			'yes'       => __( 'Yes', 'pragueescort' ),
			'sometimes' => __( 'Sometimes', 'pragueescort' ),
		];

		$this->currency = [
			'dollar' => __( 'Dollar', 'pragueescort' ),
			'euro'   => __( 'Euro', 'pragueescort' ),
			'crone'  => __( 'Czech Koruna', 'pragueescort' ),
		];

		$this->contact_type = [
			'phone'     => __( 'Phone', 'pragueescort' ),
			'telegram'  => __( 'Telegram', 'pragueescort' ),
			'instagram' => __( 'Instagram', 'pragueescort' ),
			'whatsapp'  => __( 'Whatsapp', 'pragueescort' ),
		];
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'after_setup_theme', [ $this, 'boot_carbon_field' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_escort_field' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_theme_options' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_tariff_plan_in_term_service' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_fields_in_escort_type_and_service' ] );
	}

	/**
	 * Boot carbon field class.
	 *
	 * @return void
	 */
	public function boot_carbon_field(): void {
		Carbon_Fields::boot();
	}


	/**
	 * Register field from single page escort.
	 *
	 * @return void
	 */
	public function add_escort_field(): void {
		Container::make( 'post_meta', __( 'Escort settings', 'pragueescort' ) )
			->where( 'post_type', '=', 'escort' )
			->add_tab(
				__( 'Gallery', 'pragueescort' ),
				[
					Field::make( 'media_gallery', 'pra_media_gallery', __( 'Media Gallery', 'pragueescort' ) )
						->set_duplicates_allowed( false )
						->set_type(
							[
								'image',
								'video',
							]
						),
				]
			)->add_tab(
				__( 'Bio', 'pragueescort' ),
				[
					Field::make( 'radio', 'pra_gender', __( 'Gender', 'pragueescort' ) )
						->set_width( 50 )
						->add_options( $this->gender ),
					Field::make( 'date', 'pra_date_birth', __( 'Date of birth', 'pragueescort' ) )
						->set_width( 50 )
						->set_storage_format( 'Y-m-d' ),
					Field::make( 'select', 'pra_orientation', __( 'Orientation', 'pragueescort' ) )
						->set_width( 50 )
						->add_options( $this->orientation ),
					Field::make( 'select', 'pra_ethnic_origin', __( 'Ethnic Origin', 'pragueescort' ) )
						->set_width( 50 )
						->add_options( $this->ethnic_origin ),
					Field::make( 'text', 'pra_height', __( 'Height', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_weight', __( 'Weight', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_breastcup', __( 'Breast cup', 'pragueescort' ) )
						->set_width( 50 ),
					Field::make( 'text', 'pra_hair_color', __( 'Hair color', 'pragueescort' ) )
						->set_width( 50 ),
					Field::make( 'text', 'pra_eye_color', __( 'Eye color', 'pragueescort' ) )
						->set_width( 50 ),
					Field::make( 'radio', 'pra_drink', __( 'Drink', 'pragueescort' ) )
						->set_width( 50 )
						->add_options( $this->drink ),
					Field::make( 'radio', 'pra_shaved', __( 'Shaved', 'pragueescort' ) )
						->set_width( 50 )
						->add_options( $this->shaved ),
					Field::make( 'radio', 'pra_smoke', __( 'Smoke', 'pragueescort' ) )
						->set_width( 50 )
						->add_options( $this->smoke ),
					Field::make( 'multiselect', 'pra_language', __( 'Language', 'pragueescort' ) )
						->set_width( 50 )
						->add_options( self::LIST_LANGUAGE ),
				]
			)->add_tab(
				__( 'Services', 'pragueescort' ),
				[
					Field::make( 'radio', 'pra_duo_gir_friend', __( 'Duo with Girlfriend', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_relax_massage', __( 'Relax massage', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_erotic_massage', __( 'Erotic and relaxing massage', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_bathing_together', __( 'Shower/bathing together', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_cum_body', __( 'Cum on body', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_extra_ball', __( 'Extra ball', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_handjob', __( 'Handjob', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_kissing', __( 'French kissing', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_sex_toys', __( 'Sex toys', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_fingering', __( 'Fingering', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_two_men', __( '2 men', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_oral', __( 'Oral sex without condom', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_licking', __( 'Licking', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_classic_sex', __( 'Classic sex with condom', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_couple', __( 'Couple', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_sixty_nine', __( '69 position', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
					Field::make( 'radio', 'pra_masturbation', __( 'Masturbation', 'pragueescort' ) )
						->set_width( 50 )
						->add_options(
							[
								'no'  => __( 'No', 'pragueescort' ),
								'yes' => __( 'Yes', 'pragueescort' ),
							]
						),
				]
			)->add_tab(
				__( 'Extra', 'pragueescort' ),
				[
					Field::make( 'text', 'pra_cim', __( 'CIM (Come in mouth)', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_cof', __( 'COF (Come on face)', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_swallow', __( 'Swallow', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_dfk', __( 'DFK (Deep french kissing)', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_anal', __( 'A-Level (Anal sex)', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_anal_rimming', __( 'Rimming take', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_golden', __( 'Golden shower', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_threesome', __( 'Threesome', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_foot_fetish', __( 'Foot fetish', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_submission', __( 'Submission', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_bdsm', __( 'BDSM', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_domination', __( 'Domination', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_anal_rimming_give', __( 'Rimming give', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_strap_on', __( 'Strap on', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_squirt', __( 'Squirt', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_couples', __( 'Couples', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
				]
			)->add_tab(
				__( 'Price', 'pragueescort' ),
				[
					Field::make( 'radio', 'pra_currency', __( 'Currency', 'pragueescort' ) )
						->set_width( 50 )
						->add_options( $this->currency ),
					Field::make( 'text', 'pra_half_hour', __( '30 min', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_one_hour', __( '1 hour', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_two_hour', __( '2 hour', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_three_hour', __( '3 hour', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_night', __( 'Night', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_day', __( 'Days', 'pragueescort' ) )
						->set_width( 50 )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
				]
			)->add_tab(
				__( 'Contacts', 'pragueescort' ),
				[
					Field::make( 'complex', 'pra_contacts', __( 'Contacts', 'pragueescort' ) )
						->add_fields(
							[
								Field::make( 'text', 'pra_contact_value', __( 'Contact', 'pragueescort' ) ),
							]
						),
					Field::make( 'text', 'pra_contact_email', __( 'Contact email', 'pragueescort' ) ),
				]
			);
	}

	/**
	 * Add theme settings.
	 *
	 * @return void
	 */
	public function add_theme_options(): void {
		$basic_options_container = Container::make( 'theme_options', __( 'Theme Options', 'pragueescort' ) )
			->add_fields(
				[
					Field::make( 'header_scripts', 'pra_header_script', __( 'Header Script', 'pragueescort' ) ),
					Field::make( 'footer_scripts', 'pra_footer_script', __( 'Footer Script', 'pragueescort' ) ),
				]
			);

		Container::make( 'theme_options', __( 'Dynamic Page text settings', 'pragueescort' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'textarea', 'pra_profile_comment_page', __( 'Profile comment text', 'pragueescort' ) )
						->set_rows( 4 ),
				]
			);
		Container::make( 'theme_options', __( '404 error page', 'pragueescort' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'text', 'pra_404_title', __( '404 page title', 'pragueescort' ) ),
					Field::make( 'text', 'pra_404_text', __( '404 page text', 'pragueescort' ) ),
					Field::make( 'text', 'pra_404_seo_title', __( '404 seo title', 'pragueescort' ) ),
					Field::make( 'textarea', 'pra_404_seo_text', __( '404 seo text', 'pragueescort' ) )
						->set_rows( 4 ),
				]
			);

		Container::make( 'theme_options', __( 'Archive page', 'pragueescort' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'text', 'pra_archive_title', __( 'Archive seo title', 'pragueescort' ) ),
					Field::make( 'textarea', 'pra_archive_seo_text', __( 'Archive seo text', 'pragueescort' ) )
						->set_rows( 4 ),
				]
			);

		Container::make( 'theme_options', __( 'Advertising', 'pragueescort' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'complex', 'pr_ads_home_page', __( 'Home page ads', 'pragueescort' ) )
						->add_fields(
							[
								Field::make( 'image', 'ads_desktop', __( 'Ads image desktop', 'pragueescort' ) ),
								Field::make( 'image', 'ads_mobile', __( 'Ads image mobile', 'pragueescort' ) ),
								Field::make( 'text', 'link', __( 'Ads link', 'pragueescort' ) ),
							]
						),
					Field::make( 'complex', 'pr_ads_archive_page', __( 'Archive page ads', 'pragueescort' ) )
						->add_fields(
							[
								Field::make( 'image', 'ads_desktop', __( 'Ads image desktop', 'pragueescort' ) ),
								Field::make( 'image', 'ads_mobile', __( 'Ads image mobile', 'pragueescort' ) ),
								Field::make( 'text', 'link', __( 'Ads link', 'pragueescort' ) ),
							]
						),
					Field::make( 'complex', 'pr_ads_site_bar_page', __( 'Archive site bar ads', 'pragueescort' ) )
						->add_fields(
							[
								Field::make( 'image', 'ads_desktop', __( 'Ads image desktop', 'pragueescort' ) ),
								Field::make( 'image', 'ads_mobile', __( 'Ads image mobile', 'pragueescort' ) ),
								Field::make( 'text', 'link', __( 'Ads link', 'pragueescort' ) ),
							]
						),
				]
			);

		Container::make( 'theme_options', __( 'Invoice settings', 'pragueescort' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'text', 'pra_account_holder', __( 'Account holder:', 'pragueescort' ) ),
					Field::make( 'text', 'pra_iban', __( 'IBAN:', 'pragueescort' ) ),
					Field::make( 'text', 'pra_bank', __( 'Bank:', 'pragueescort' ) ),
					Field::make( 'text', 'pra_swift', __( 'SWIFT:', 'pragueescort' ) ),
					Field::make( 'text', 'pra_bank_code', __( 'Bank code:', 'pragueescort' ) ),
					Field::make( 'textarea', 'pra_payment_description', __( 'Payment description:', 'pragueescort' ) )
						->set_rows( 4 ),
				]
			);
	}

	/**
	 * Add tariff plan in term service.
	 *
	 * @return void
	 */
	public function add_tariff_plan_in_term_service(): void {
		Container::make( 'term_meta', __( 'Tariff settings', 'pragueescort' ) )
			->where( 'term_taxonomy', '=', 'escort-type' )
			->add_fields(
				[
					Field::make( 'complex', 'pra_type_tariff', __( 'Tariff point', 'pragueescort' ) )
						->add_fields(
							'points',
							[
								Field::make( 'text', 'pra_tarif_point', __( 'Tariff point', 'pragueescort' ) ),
							]
						),
					Field::make( 'radio', 'pra_tariff_currency', __( 'Currency', 'pragueescort' ) )
						->set_width( 50 )
						->add_options( $this->currency ),
					Field::make( 'text', 'pra_tariff_price', __( 'Tariff price', 'pragueescort' ) )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
					Field::make( 'text', 'pra_tariff_order', __( 'Tariff order', 'pragueescort' ) )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', '0' ),
				]
			);
	}

	/**
	 * Add seo text in categories.
	 *
	 * @return void
	 */
	public function add_fields_in_escort_type_and_service(): void {
		Container::make( 'term_meta', __( 'Seo text', 'pragueescort' ) )
			->where( 'term_taxonomy', '=', 'escort-type' )
			->or_where( 'term_taxonomy', '=', 'service' )
			->add_fields(
				[
					Field::make( 'text', 'pra_seo_text_title', __( 'Seo title', 'pragueescort' ) ),
					Field::make( 'rich_text', 'pra_seo_text', __( 'Seo text', 'pragueescort' ) ),
				]
			);
	}
}
