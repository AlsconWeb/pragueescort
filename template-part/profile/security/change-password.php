<?php
/**
 * Change password profile from.
 *
 * @package pragueescort/theme
 */

?>
<form class="change-user-password" method="post">
	<h4><?php esc_html_e( 'Change password', 'pragueescort' ); ?></h4>
	<div class="input">
		<label for="old-password">
			<?php esc_html_e( 'Old password', 'pragueescort' ); ?>
		</label>
		<input type="password" name="user_info_password[old_password]" id="old-password">
	</div>
	<div class="input">
		<label for="new-password">
			<?php esc_html_e( 'New password', 'pragueescort' ); ?>
		</label>
		<input type="password" name="user_info_password[new_password]" id="new-password">
	</div>
	<div class="input">
		<label for="confirm-password">
			<?php esc_html_e( 'Confirm password', 'pragueescort' ); ?>
		</label>
		<input type="password" name="user_info_password[confirm_password]" id="confirm-password">
	</div>
	<div class="submit">
		<input type="hidden" name="user_id" id="user-id" value="<?php echo esc_attr( get_current_user_id() ); ?>">
		<input class="button dark" id="pswd-submit" type="submit"
		       value="<?php esc_html_e( 'Save', 'pragueescort' ); ?>">
	</div>
</form>
