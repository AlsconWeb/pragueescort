<?php
/**
 * Tab BIO.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\CarbonFields;

$date_birthday      = carbon_get_the_post_meta( 'pra_date_birth' );
$year_birthday      = gmdate( 'Y', strtotime( $date_birthday ) );
$current_year       = gmdate( 'Y' );
$current_olds_years = $current_year - $year_birthday;

$language     = carbon_get_the_post_meta( 'pra_language' );
$service_type = wp_get_post_terms( get_the_ID(), 'service' );

?>
<div class="tab-content active" id="bio">
	<div class="left-block">
		<ul class="desc">
			<li>
				<?php esc_html_e( 'Gender', 'pragueescort' ); ?>
				<span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_gender' ) ) ); ?></span>
			</li>
			<li>
				<?php esc_html_e( 'Age', 'pragueescort' ); ?>
				<span><?php echo esc_html( $current_olds_years ); ?></span>
			</li>
			<li>
				<?php esc_html_e( 'Drink', 'pragueescort' ); ?>
				<span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_drink' ) ) ); ?></span>
			</li>
			<li>
				<?php esc_html_e( 'Shaved', 'pragueescort' ); ?>
				<span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_shaved' ) ) ); ?></span>
			</li>
			<li>
				<?php esc_html_e( 'Ethnic Origin', 'pragueescort' ); ?>
				<span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_ethnic_origin' ) ) ); ?></span>
			</li>
			<li>
				<?php esc_html_e( 'Hair color', 'pragueescort' ); ?>
				<span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_hair_color' ) ) ); ?></span>
			</li>
			<li>
				<?php esc_html_e( 'Eye color', 'pragueescort' ); ?>
				<span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_eye_color' ) ) ); ?></span>
			</li>
			<li>
				<?php esc_html_e( 'Breastcup', 'pragueescort' ); ?>
				<span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_breastcup' ) ) ); ?></span>
			</li>
			<li>
				<?php esc_html_e( 'Orientation', 'pragueescort' ); ?>
				<span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_orientation' ) ) ); ?></span>
			</li>
			<li>
				<?php esc_html_e( 'Height', 'pragueescort' ); ?>
				<span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_height' ) ) ); ?></span>
			</li>
			<li>
				<?php esc_html_e( 'Weight', 'pragueescort' ); ?>
				<span><?php echo esc_html( ucfirst( carbon_get_the_post_meta( 'pra_weight' ) ) ); ?></span>
			</li>
		</ul>
	</div>
	<div class="right-block">
		<?php if ( ! empty( $service_type ) ) { ?>
			<div class="service">
				<h4><?php esc_html_e( 'Service', 'pragueescort' ); ?></h4>
				<ul>
					<?php foreach ( $service_type as $item ) { ?>
						<li><?php echo esc_html( $item->name ); ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		<?php if ( ! empty( $language ) ) { ?>
			<div class="languages">
				<h4><?php esc_html_e( 'Languages ', 'pragueescort' ); ?></h4>
				<ul>
					<?php foreach ( $language as $key => $item ) { ?>
						<li class="<?php echo esc_attr( $item ); ?>"><?php echo esc_html( CarbonFields::LIST_LANGUAGE[ $item ] ); ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
	</div>
</div>
