<?php
/**
 * Blog template file.
 *
 * @package pragueescort/theme
 */

get_header();

$blog_categories = get_terms(
	[
		'taxonomy'   => 'category',
		'hide_empty' => true,
	]
);
?>
	<section>
		<div class="vc_row wpb_row vc_row-fluid vc_row-o-content-middle vc_row-flex">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_row wpb_row vc_inner vc_row-fluid">
							<div class="wpb_column vc_column_container vc_col-xs-12 vc_col-sm-5 vc_col-md-4 vc_col-lg-3">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="wpb_column vc_column_container">
											<a
													class="back icon-arrow-left"
													href="<?php echo esc_url( get_bloginfo( 'url' ) ); ?>">
												<?php esc_html_e( 'Home', 'pragueescort' ); ?>
											</a>
											<div class="category">
												<h4>
													<?php esc_html_e( 'Category', 'pragueescort' ); ?>
												</h4>
												<ul class="category-items">
													<?php
													if ( ! empty( $blog_categories ) ) {
														foreach ( $blog_categories as $blog_category ) {
															?>
															<li>
																<a href="<?php echo esc_url( get_term_link( $blog_category->term_id ) ); ?>">
																	<?php echo esc_html( $blog_category->name ); ?>
																</a>
															</li>
															<?php
														}
													} ?>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="wpb_column vc_column_container vc_col-xs-12 vc_col-sm-7 vc_col-md-8 vc_col-lg-9">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="wpb_column vc_column_container">
											<h1 class="title"><?php esc_html_e( 'BLOG', 'pragueescort' ); ?></h1>
											<?php if ( have_posts() ) { ?>
												<div class="blogs">
													<?php
													while ( have_posts() ) {
														the_post();

														get_template_part( 'template-part/blog/blog', 'item' );
													}
													?>
												</div>
											<?php } else { ?>
												<h4><?php esc_html_e( 'There are no posts on this blog yet', 'pragueescort' ); ?></h4>
											<?php } ?>
											<div class="page_navigation_wrapper">
												<?php
												if ( function_exists( 'wp_pagenavi' ) ) {
													wp_pagenavi();
												}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
