<?php
/**
 * Tab navigation.
 *
 * @package pragueescort/theme
 */

?>
<ul class="tabs-nav">
	<li class="active">
		<a href="#bio"><?php esc_html_e( 'BIO', 'pragueescort' ); ?></a>
	</li>
	<li>
		<a href="#services"><?php esc_html_e( 'SERVICES', 'pragueescort' ); ?></a>
	</li>
	<li>
		<a href="#price"><?php esc_html_e( 'PRICE', 'pragueescort' ); ?></a>
	</li>
	<li>
		<a href="#comments">
			<?php
			esc_html_e( 'COMMENTS', 'pragueescort' );

			$comment_count = (int) get_comments_number();
			if ( ! empty( $comment_count ) ) {
				printf( '<span>%d</span>', esc_attr( $comment_count ) );
			}
			
			?>

		</a>
	</li>
</ul>
