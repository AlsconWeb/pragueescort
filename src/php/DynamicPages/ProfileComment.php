<?php
/**
 * Profile comment dynamic page.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\DynamicPages;

/**
 * ProfileComment file name.
 */
class ProfileComment extends DynamicPageBase {

	/**
	 * Page slug.
	 */
	public const PAGE_PATH = 'profile-comments';

	/**
	 * Active profile id.
	 *
	 * @var int
	 */
	public int $active_profile_id;

	/**
	 * User id.
	 *
	 * @var int
	 */
	public int $user_id;

	/**
	 * Active page.
	 *
	 * @var string
	 */
	public string $active_page;

	/**
	 * AccountPage construct.
	 */
	public function __construct() {
		parent::__construct();

		$this->user_id = get_current_user_id();
	}

	/**
	 * Output content.
	 *
	 * @return void
	 */
	protected function content(): void {
		$this->active_page = self::PAGE_PATH;
		$this->show_content();
	}

	/**
	 * Show content.
	 *
	 * @return void
	 */
	private function show_content(): void {
		?>
		<div class="vc_row wpb_row vc_row-fluid vc_row-o-content-middle vc_row-flex">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_row wpb_row vc_inner vc_row-fluid">
							<div class="wpb_column vc_column_container vc_col-sm-12">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="wpb_column vc_column_container">
											<a
													class="back icon-arrow-left"
													href="<?php echo esc_url( get_bloginfo( 'url' ) ); ?>">
												<?php esc_attr_e( 'Home', 'pragueescort' ); ?>
											</a>
										</div>
									</div>
								</div>
							</div>
							<?php
							get_template_part( 'template-part/profile/sidebar', null, [ 'profile' => $this ] );
							get_template_part( 'template-part/profile/comments', null, [ 'profile' => $this ] );
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		get_template_part( 'template-part/modal/modal', 'comment_refute', [ 'profile' => $this ] );
	}
}
