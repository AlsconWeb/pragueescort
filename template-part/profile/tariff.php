<?php
/**
 * Tariff content.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\Helpers;

$tariff_plans = get_terms(
	[
		'taxonomy'   => 'escort-type',
		'hide_empty' => false,
		'order'      => 'DESC',
		'orderby'    => 'meta_value_num',
		'meta_key'   => '_pra_tariff_order',
	]
);

$profile = $args['profile'];

?>
<div class="wpb_column vc_column_container vc_col-xs-12 vc_col-sm-7 vc_col-md-8 vc_col-lg-9">
	<div class="vc_column-inner">
		<div class="wpb_wrapper">
			<div class="wpb_column vc_column_container">
				<div class="tariff-block">
					<h1><?php esc_html_e( 'Tarification', 'pragueescort' ); ?></h1>
					<p><?php esc_html_e( 'The higher the status of your account, the greater number of visitors will see your profile and you will receive more benefits from the site Prague Escort.', 'pragueescort' ); ?></p>
					<div class="items">
						<?php
						if ( ! empty( $tariff_plans ) ) {
							foreach ( $tariff_plans as $plan ) {
								?>
								<div class="item <?php echo esc_attr( $plan->slug ); ?>">
									<h3 class="status <?php echo esc_attr( $plan->slug ); ?>">
										<?php echo esc_html( $plan->name ); ?>
									</h3>
									<?php
									$tariff_point = carbon_get_term_meta( $plan->term_id, 'pra_type_tariff' );

									if ( ! empty( $tariff_point ) ) {
										?>
										<ul>
											<?php foreach ( $tariff_point as $item ) { ?>
												<li><?php echo esc_html( $item['pra_tarif_point'] ); ?></li>
											<?php } ?>
										</ul>
										<?php
									}
									?>
									<p class="price"><?php echo esc_html( Helpers::get_tariff_plan( $plan->term_id ) ); ?></p>
									<a
											class="button gradient" href="#"
											data-tariff_id="<?php echo esc_html( $plan->term_id ); ?>"
											data-current_user="<?php echo esc_attr( get_current_user_id() ); ?>"
											data-profile_id="<?php echo esc_html( $profile->active_profile_id ); ?>"
											data-price="<?php echo esc_html( Helpers::get_tariff_plan( $plan->term_id ) ); ?>">
										<?php esc_html_e( 'Join', 'pragueescort' ); ?>
									</a>
								</div>
								<?php
							}
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>