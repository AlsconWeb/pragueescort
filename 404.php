<?php
/**
 * 404 Error template.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\Helpers;

get_header();

$execute = new Helpers();
?>
	<section>
		<div class="vc_row wpb_row vc_row-fluid vc_row-o-content-middle vc_row-flex">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_row wpb_row vc_inner vc_row-fluid">
							<div class="wpb_column vc_column_container vc_col-sm-12">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="wpb_column vc_column_container">
											<a class="back icon-arrow-left" href="<?php esc_url( home_url() ); ?>">
												<?php esc_html_e( 'Back', 'pragueescort' ); ?>
											</a>
											<div class="page404">
												<h1>404</h1>
												<h2><?php echo esc_html( carbon_get_theme_option( 'pra_404_title' ) ); ?></h2>
												<p><?php echo esc_html( carbon_get_theme_option( 'pra_404_text' ) ); ?></p>
												<a class="button gradient" href="<?php echo esc_url( home_url() ); ?>">
													<?php esc_html_e( 'Homepage', 'pragueescort' ); ?>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="wpb_column vc_column_container vc_col-sm-12">
								<?php get_template_part( 'template-part/single-escort/slider', 'diamond', [ 'execute' => $execute ] ); ?>
							</div>
							<div class="wpb_column vc_column_container vc_col-sm-12">
								<?php get_template_part( 'template-part/single-escort/slider', 'vip', [ 'execute' => $execute ] ); ?>
							</div>
							<div class="wpb_column vc_column_container vc_col-sm-12 seo-block">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="wpb_column vc_column_container">
											<h3><?php echo esc_html( carbon_get_theme_option( 'pra_404_seo_title' ) ); ?></h3>
											<?php echo wp_kses_post( wpautop( carbon_get_theme_option( 'pra_404_seo_text' ) ) ); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
