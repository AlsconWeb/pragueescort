<?php
/**
 * Gallery photo template part.
 *
 * @package pragueescort/theme
 */

$escort_id = $args['escort_id'];

$gallery = carbon_get_post_meta( $escort_id, 'pra_media_gallery' );

if ( ! empty( $gallery ) ) {
	?>
	<div class="gallery-photo">
		<?php
		foreach ( $gallery as $image ) {
			$image_url = wp_get_attachment_image_url( $image, 'full' );
			?>
			<div class="item">
				<a class="icon-zoom" href="<?php echo esc_url( $image_url ); ?>" data-fancybox="gallery">
					<img
							src="<?php echo esc_url( $image_url ); ?>"
							alt="<?php echo esc_html( get_the_title( $image_url ) ); ?>">
				</a>
			</div>
		<?php } ?>
	</div>
	<?php
}
