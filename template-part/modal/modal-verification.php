<?php
/**
 * Modal Thx.
 *
 * @package pragueescort/theme
 */

?>
<div class="modal fade send-modal">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<i class="icon-close" data-dismiss="modal"></i>
			<i class="icon-send"></i>
			<h3 id="modal-title"></h3>
			<p id="modal-text"></p>
			<a class="button" href="#" data-dismiss="modal">
				<?php esc_html_e( 'Close', 'pragueescort' ); ?>
			</a>
		</div>
	</div>
</div>
