<?php
/**
 * Escort Listing Ajax.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\WpBakery\Components;

use PRAGUE\Theme\Main;
use WP_Query;

/**
 * EscortListingAjax class file.
 */
class EscortListingAjax {

	/**
	 * Action name and nonce code.
	 */
	public const PRA_LISTING_ACTION_NAME = 'pra_listing_action_name';

	/**
	 * Escort type to select.
	 *
	 * @var array
	 */
	public array $escort_type;

	/**
	 * EscortListing construct
	 */
	public function __construct() {
		add_shortcode( 'pra_escort_listing_ajax', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'pra_escort_listing_ajax', [ $this, 'map' ] );
		}

		$this->set_term_escort_typ();

		// add profile ajax.
		add_action( 'wp_ajax_' . self::PRA_LISTING_ACTION_NAME, [ $this, 'load_more' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_LISTING_ACTION_NAME, [ $this, 'load_more' ] );
	}

	/**
	 * Set term escort type to select.
	 *
	 * @return void
	 */
	private function set_term_escort_typ(): void {
		$term_escort_type = get_terms(
			[
				'taxonomy'   => 'escort-type',
				'hide_empty' => false,
			]
		);

		if ( ! empty( $term_escort_type ) && ! is_wp_error( $term_escort_type ) ) {
			foreach ( $term_escort_type as $term ) {
				$this->escort_type[ $term->name ] = $term->slug;
			}
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Escort Listing Ajax', 'pragueescort' ),
			'description'             => esc_html__( 'Escort Listing Ajax', 'pragueescort' ),
			'base'                    => 'pra_escort_listing_ajax',
			'category'                => __( 'PRA', 'pragueescort' ),
			'show_settings_on_create' => false,
			'icon'                    => PRA_URL_PATH . '/assets/icons/list-check-solid.svg',
			'params'                  => [
				[
					'type'       => 'checkbox',
					'value'      => $this->escort_type,
					'param_name' => 'escort_type',
					'heading'    => __( 'Choose escort type', 'pragueescort' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'param_name' => 'pra_per_page',
					'heading'    => __( 'Number item per page', 'pragueescort' ),
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Custom css', 'pragueescort' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design options', 'pragueescort' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::PRA_DIR_PATH . '/WpBakery/Templates/EscortListingAjax/template.php';

		return ob_get_clean();
	}

	/**
	 * Ajax handler load more.
	 *
	 * @return void
	 */
	public function load_more(): void {
		$nonce = ! empty( $_POST['loadMoreNonce'] ) ? filter_var( wp_unslash( $_POST['loadMoreNonce'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_LISTING_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Bad nonce code', 'pragueescort' ) ] );
		}

		$exclude_post   = ! empty( $_POST['excludePost'] ) ? filter_var( wp_unslash( $_POST['excludePost'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;
		$count_per_page = ! empty( $_POST['countPerPage'] ) ? filter_var( wp_unslash( $_POST['countPerPage'] ), FILTER_SANITIZE_NUMBER_INT ) : 10;
		$escort_type    = ! empty( $_POST['escortType'] ) ? filter_var( wp_unslash( $_POST['escortType'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		$exclude_post = explode( ',', $exclude_post ) ?? [];

		$args      = [
			'post_type'      => 'escort',
			'posts_per_page' => $count_per_page,
			'order'          => 'DESC',
			'orderby'        => 'rand',
			'post_status'    => 'publish',
			'post__not_in'   => $exclude_post,
			'tax_query'      => [
				[
					'taxonomy' => 'escort-type',
					'field'    => 'slug',
					'terms'    => $escort_type,
				],
			],
		];
		$query_obj = new WP_Query( $args );
		$exclude   = [];

		if ( $query_obj->have_posts() ) {
			ob_start();
			while ( $query_obj->have_posts() ) {
				$query_obj->the_post();

				$escort_id = get_the_ID();

				get_template_part(
					'template-part/single-escort/slider',
					'item',
					[
						'escort_id'  => $escort_id,
						'item_class' => $escort_type,
					]
				);
				$exclude[] = (string) $escort_id;
			}
			wp_reset_postdata();

			$all_exclude = array_unique( array_intersect( array_merge( $exclude, $exclude_post ) ) );

			wp_send_json_success(
				[
					'html'     => ob_get_clean(),
					'exclude'  => $all_exclude,
					'count_ex' => count( $all_exclude ),
				]
			);
		}

		wp_send_json_error( [ 'post_count' => 0 ] );
	}
}
