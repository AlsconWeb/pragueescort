<?php
/**
 * Template Name: Page in back btn.
 *
 * @package pragueescort/theme
 */

get_header();
?>
	<section>
		<div class="vc_row wpb_row vc_row-fluid vc_row-o-content-middle vc_row-flex">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_row wpb_row vc_inner vc_row-fluid">
							<div class="wpb_column vc_column_container vc_col-sm-12">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="wpb_column vc_column_container">
											<a
												class="back icon-arrow-left"
												href="<?php echo esc_url( get_bloginfo( 'url' ) ); ?>">
												<?php esc_attr_e( 'Back', 'pragueescort' ); ?>
											</a>
										</div>
									</div>
								</div>
							</div>
							<?php
							if ( have_posts() ) {
								while ( have_posts() ) {
									the_post();

									the_content();
								}
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
