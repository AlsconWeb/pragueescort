<?php
/**
 * Single escort template
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\Helpers;

$execute = new Helpers();

get_header();
?>
	<section>
		<div class="vc_row wpb_row vc_row-fluid vc_row-o-content-middle vc_row-flex">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="vc_row wpb_row vc_inner vc_row-fluid">
							<div class="wpb_column vc_column_container vc_col-sm-12">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="wpb_column vc_column_container">
											<a
													class="back icon-arrow-left"
													href="<?php echo esc_url( get_post_type_archive_link( 'escort' ) ); ?>">
												<?php esc_html_e( 'All girls', 'pragueescort' ); ?>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="wpb_column vc_column_container vc_col-sm-3">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="wpb_column vc_column_container">
											<?php
											get_template_part( 'template-part/single-escort/sidebar', 'hot', [ 'execute' => $execute ] );
											get_template_part( 'template-part/single-escort/advertising', 'sitebar' );
											?>
										</div>
									</div>
								</div>
							</div>
							<div class="wpb_column vc_column_container vc_col-sm-9">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="wpb_column vc_column_container">
											<?php
											if ( have_posts() ) {
												while ( have_posts() ) {
													the_post();
													$escort_id = get_the_ID();
													?>
													<div class="description">
														<?php

														if ( has_post_thumbnail( $escort_id ) ) {
															the_post_thumbnail( 'escort_thumbnail' );
														} else {
															echo '<img src="https://via.placeholder.com/180x238" alt="">';
														}
														?>
														<div class="desc">
															<h2><?php the_title(); ?></h2>
															<?php the_content(); ?>
															<div class="contacts">
																<h5><?php esc_html_e( 'Contacts', 'pragueescort' ); ?></h5>
																<div class="buttons">
																	<a
																			class="button gradient" href="#"
																			data-toggle="modal"
																			data-target=".contacts-modal">
																		<?php esc_html_e( 'Contacts ', 'pragueescort' ); ?>
																	</a>
																	<a
																			class="button transparent"
																			data-toggle="modal"
																			data-target=".comment-modal"
																			href="#">
																		<?php esc_html_e( 'Leave a comment', 'pragueescort' ); ?>
																	</a>
																</div>
															</div>
														</div>
													</div>
													<?php
													get_template_part( 'template-part/single-escort/gallery', 'photo', [ 'escort_id' => $escort_id ] );
													get_template_part( 'template-part/single-escort/description', 'tabs', [ 'escort_id' => $escort_id ] );
													get_template_part( 'template-part/single-escort/slider', 'diamond', [ 'execute' => $execute ] );
													get_template_part( 'template-part/single-escort/slider', 'vip', [ 'execute' => $execute ] );
												}
											}
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
