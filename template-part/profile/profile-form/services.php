<?php
/**
 * Profile from services.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\CarbonFields;

$question = [
	'yes' => __( 'Yes', 'pragueescort' ),
	'no'  => __( 'No', 'pragueescort' ),
];

$i = 0;

$profile = $args['profile'];

$profile_field = new CarbonFields();
?>
<div class="services">
	<h2><?php esc_html_e( 'SERVICES', 'pragueescort' ); ?></h2>
	<h5><?php esc_html_e( 'Standard service', 'pragueescort' ); ?></h5>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'Duo with Girlfriend', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$duo_gir_friend = carbon_get_post_meta( $profile->active_profile_id, 'pra_duo_gir_friend' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="girlfriend-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_duo_gir_friend]"
						<?php checked( $duo_gir_friend, $key ); ?>>
					<label for="girlfriend-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'Classic sex with condom', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$classic_sex = carbon_get_post_meta( $profile->active_profile_id, 'pra_classic_sex' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="classic-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_classic_sex]"
						<?php checked( $classic_sex, $key ); ?>>
					<label for="classic-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( '2men', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$two_men = carbon_get_post_meta( $profile->active_profile_id, 'pra_two_men' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="two-men-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_two_men]"
						<?php checked( $two_men, $key ); ?>>
					<label for="two-men-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'Relax massage', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$relax_massage = carbon_get_post_meta( $profile->active_profile_id, 'pra_relax_massage' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="relax-massage-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_relax_massage]"
						<?php checked( $relax_massage, $key ); ?>>
					<label for="relax-massage-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'French kissing', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$kissing = carbon_get_post_meta( $profile->active_profile_id, 'pra_kissing' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="kissing-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_kissing]"
						<?php checked( $kissing, $key ); ?>>
					<label for="kissing-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'Erotic and relaxing massage', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$erotic_massage = carbon_get_post_meta( $profile->active_profile_id, 'pra_erotic_massage' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="erotic-massage-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_erotic_massage]"
						<?php checked( $erotic_massage, $key ); ?>>
					<label for="erotic-massage-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'Sex toys', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$sex_toys = carbon_get_post_meta( $profile->active_profile_id, 'pra_sex_toys' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="sex-toys-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_sex_toys]"
						<?php checked( $sex_toys, $key ); ?>>
					<label for="sex-toys-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'Shower/bathing together', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$bathing_together = carbon_get_post_meta( $profile->active_profile_id, 'pra_bathing_together' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="shower-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_bathing_together]"
						<?php checked( $bathing_together, $key ); ?>>
					<label for="shower-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'Fingering', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$fingering = carbon_get_post_meta( $profile->active_profile_id, 'pra_fingering' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="fingering-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_fingering]"
						<?php checked( $fingering, $key ); ?>>
					<label for="fingering-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( '69 position', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$sixty_nine = carbon_get_post_meta( $profile->active_profile_id, 'pra_sixty_nine' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="position-69-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_sixty_nine]"
						<?php checked( $sixty_nine, $key ); ?>>
					<label for="position-69-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'Oral sex without condom', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$oral = carbon_get_post_meta( $profile->active_profile_id, 'pra_oral' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="oral-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_oral]"
						<?php checked( $oral, $key ); ?>>
					<label for="oral-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'Couple', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$couple = carbon_get_post_meta( $profile->active_profile_id, 'pra_couple' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="couple-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_couple]"
						<?php checked( $couple, $key ); ?>>
					<label for="couple-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'Extraball', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$extra_ball = carbon_get_post_meta( $profile->active_profile_id, 'pra_extra_ball' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="extraball-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_extra_ball]"
						<?php checked( $extra_ball, $key ); ?>>
					<label for="extraball-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'Licking', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$licking = carbon_get_post_meta( $profile->active_profile_id, 'pra_licking' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="licking-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_licking]"
						<?php checked( $licking, $key ); ?>>
					<label for="licking-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'Cum on body', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$cum_body = carbon_get_post_meta( $profile->active_profile_id, 'pra_cum_body' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="cum-body-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_cum_body]"
						<?php checked( $cum_body, $key ); ?>>
					<label for="cum-body-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'Handjob', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$handjob = carbon_get_post_meta( $profile->active_profile_id, 'pra_handjob' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="handjob-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_handjob]"
						<?php checked( $handjob, $key ); ?>>
					<label for="handjob-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="radio-buttons">
		<p><?php esc_html_e( 'Masturbation', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$handjob = carbon_get_post_meta( $profile->active_profile_id, 'pra_masturbation' );
			foreach ( $question as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="masturbation-<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_masturbation]"
						<?php checked( $handjob, $key ); ?>>
					<label for="masturbation-<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
	<hr class="sline">
	<div class="radio-buttons w_two">
		<p><?php esc_html_e( 'Currency', 'pragueescort' ); ?></p>
		<div class="wrap">
			<?php
			$currency = carbon_get_post_meta( $profile->active_profile_id, 'pra_currency' );
			foreach ( $profile_field->currency as $key => $item ) {
				?>
				<div class="radio-button">
					<input
							id="<?php echo esc_attr( $key ); ?>"
							type="radio"
							value="<?php echo esc_attr( $key ); ?>"
							name="edit_profile[pra_currency]"
						<?php checked( $currency, $key ); ?>>
					<label for="<?php echo esc_attr( $key ); ?>">
						<?php echo esc_html( $item ); ?>
					</label>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
