<?php
/**
 * Modal add profile.
 *
 * @package pragueescort/theme
 */

?>
<div class="modal fade add-profile" style="display: none;">
	<div class="modal-dialog modal-xs">
		<div class="modal-content">
			<i class="icon-close" data-dismiss="modal"></i>
			<i class="icon-user"></i>
			<?php
			printf(
				'<h3>%s <br>%s</h3>',
				esc_html( __( 'Enter the name', 'pragueescort' ) ),
				esc_html( __( 'of your future profile:', 'pragueescort' ) )
			);
			?>

			<div class="input">
				<label for="add_name" class="hidden"></label>
				<input type="text" id="add_name" placeholder="<?php esc_html_e( 'Name', 'pragueescort' ); ?>">
			</div>
			<div class="submit">
				<input
					class="button gradient"
					id="add_profile"
					type="submit"
					value="<?php esc_html_e( 'Add profile', 'pragueescort' ); ?>">
			</div>

		</div>
	</div>
</div>
