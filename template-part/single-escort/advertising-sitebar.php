<?php
/**
 * Ads in site bare.
 *
 * @package pragueescort/theme
 */

$ads = carbon_get_theme_option( 'pr_ads_site_bar_page' );
if ( ! empty( $ads ) ) {
	$key = wp_rand( 0, count( $ads ) - 1 );
	shuffle( $ads );
	$image_desktop = wp_get_attachment_image( $ads[ $key ]['ads_desktop'], 'full' );
	$image_mobile  = wp_get_attachment_image( $ads[ $key ]['ads_mobile'], 'full' );
	?>
	<div class="advertising-soderbar">
		<?php
		echo sprintf(
			'<a href="%s"><span class="desktop">%s</span> <span class="mobile">%s</span></a>',
			esc_url( $ads[ $key ]['link'] ),
			wp_kses_post( $image_desktop ),
			wp_kses_post( $image_mobile ),
		);
		?>
	</div>
	<?php
}