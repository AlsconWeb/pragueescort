<?php
/**
 * Theme init file.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\Main;

require_once __DIR__ . '/vendor/autoload.php';

define( 'PRA_URL_PATH', get_stylesheet_directory_uri() );
define( 'PRA_ROOT_PATH', get_stylesheet_directory() );

new Main();


/**
 * Determines whether the query is for an existing single page or dynamic page.
 *
 * @param int|string|int[]|string[] $page Optional. Page ID, title, slug, or array of such
 *                                        to check against. Default empty.
 *
 * @return bool Whether the query is for an existing single page.
 */
function fq_is_page( $page ) {
	if ( is_page( $page ) ) {
		return true;
	}

	return apply_filters( 'sws_dynamic_page', false );
}
