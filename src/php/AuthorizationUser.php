<?php
/**
 * Authorization User
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme;

use PRAGUE\Theme\DynamicPages\ProfilePage;
use PRAGUE\Theme\Email\AdminEmail;

/**
 * AuthorizationUser class file.
 */
class AuthorizationUser {

	/**
	 * Authorization action name and nonce.
	 */
	public const PRA_AUTH_ACTION_NAME = 'pra_auth_action';

	/**
	 * Registration action name and nonce.
	 */
	public const PRA_REG_ACTION_NAME = 'pra_reg_action';

	/**
	 * Nonce code name to email validate.
	 */
	public const  PRA_MAIL_AUTH_NONCE = 'pra_reg_nonce';

	/**
	 * Recover password action name and nonce.
	 */
	public const PRA_RECOVER_PSWD_ACTION_NAME = 'pra_recover_password';

	/**
	 * AuthorizationUser construct.
	 */
	public function __construct() {

		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {

		// log-in.
		add_action( 'wp_ajax_' . self::PRA_AUTH_ACTION_NAME, [ $this, 'login_handler' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_AUTH_ACTION_NAME, [ $this, 'login_handler' ] );

		// register.
		add_action( 'wp_ajax_' . self::PRA_REG_ACTION_NAME, [ $this, 'register_handler' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_REG_ACTION_NAME, [ $this, 'register_handler' ] );

		// recover password.
		add_action( 'wp_ajax_' . self::PRA_RECOVER_PSWD_ACTION_NAME, [ $this, 'recover_password_handler' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_RECOVER_PSWD_ACTION_NAME, [ $this, 'recover_password_handler' ] );

		// add user role.
		add_action( 'after_setup_theme', [ $this, 'add_role_escort' ] );
	}

	/**
	 * Login ajax handler.
	 *
	 * @return void
	 */
	public function login_handler(): void {
		$nonce = ! empty( $_POST['nonce_login'] ) ? filter_var( wp_unslash( $_POST['nonce_login'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_AUTH_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Bad nonce code', 'pragueescort' ) ] );
		}

		$email       = ! empty( $_POST['user_email'] ) ? filter_var( wp_unslash( $_POST['user_email'] ), FILTER_VALIDATE_EMAIL ) : null;
		$user_pswd   = ! empty( $_POST['user_pswd'] ) ? filter_var( wp_unslash( $_POST['user_pswd'] ), FILTER_SANITIZE_STRING ) : null;
		$remember_me = ! empty( $_POST['remember_me'] ) ? filter_var( wp_unslash( $_POST['remember_me'] ), FILTER_SANITIZE_STRING ) : null;

		if ( ! email_exists( $email ) ) {
			wp_send_json_error(
				[
					'field'   => 'user_email',
					'message' => __( 'Email not exist or no correct', 'pragueescort' ),
				]
			);
		}

		if ( empty( $user_pswd ) ) {
			wp_send_json_error(
				[
					'field'   => 'user_pswd',
					'message' => __( 'Empty password field', 'pragueescort' ),
				]
			);
		}

		$user = wp_signon(
			[
				'user_login'    => $email,
				'user_password' => $user_pswd,
				'remember'      => $remember_me,
			]
		);

		if ( is_wp_error( $user ) ) {
			wp_send_json_error(
				[
					'field'   => 'login_submit',
					'message' => $user->get_error_message(),
				]
			);
		}

		wp_send_json_success(
			[
				'urlRedirect' => get_bloginfo( 'url' ) . '/' . ProfilePage::PAGE_PATH,
			]
		);

	}

	/**
	 * Register ajax handler.
	 *
	 * @return void
	 */
	public function register_handler(): void {
		$nonce = ! empty( $_POST['nonce_register'] ) ? filter_var( wp_unslash( $_POST['nonce_register'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_REG_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Bad nonce code', 'pragueescort' ) ] );
		}

		$email                 = ! empty( $_POST['user_email'] ) ? filter_var( wp_unslash( $_POST['user_email'] ), FILTER_VALIDATE_EMAIL ) : null;
		$user_name             = ! empty( $_POST['user_name'] ) ? filter_var( wp_unslash( $_POST['user_name'] ), FILTER_SANITIZE_STRING ) : null;
		$reg_user_phone        = ! empty( $_POST['reg_user_phone'] ) ? filter_var( wp_unslash( $_POST['reg_user_phone'] ), FILTER_SANITIZE_NUMBER_INT ) : null;
		$reg_user_pswd         = ! empty( $_POST['reg_user_pswd'] ) ? filter_var( wp_unslash( $_POST['reg_user_pswd'] ), FILTER_SANITIZE_STRING ) : null;
		$reg_user_pswd_confirm = ! empty( $_POST['reg_user_pswd_confirm'] ) ? filter_var( wp_unslash( $_POST['reg_user_pswd_confirm'] ), FILTER_SANITIZE_STRING ) : null;

		if ( empty( $email ) ) {
			wp_send_json_error(
				[
					'field'   => 'reg_user_email',
					'message' => __( 'Email not correct', 'pragueescort' ),
				]
			);
		}

		if ( email_exists( $email ) ) {
			wp_send_json_error(
				[
					'field'   => 'reg_user_email',
					'message' => __( 'This email is linked to another account', 'pragueescort' ),
				]
			);
		}

		if ( empty( $user_name ) || 3 >= strlen( trim( $user_name ) ) ) {
			wp_send_json_error(
				[
					'field'   => 'reg_user_name',
					'message' => __( 'User name empty or less than three characters', 'pragueescort' ),
				]
			);
		}

		if ( empty( $reg_user_phone ) || ! Helpers::validate_phone_number( $reg_user_phone ) ) {
			wp_send_json_error(
				[
					'field'   => 'reg_user_phone',
					'message' => __( 'User name empty or less than three characters', 'pragueescort' ),
				]
			);
		}

		if ( empty( $reg_user_pswd ) ) {
			wp_send_json_error(
				[
					'field'   => 'reg_user_pswd',
					'message' => __( 'Empty password field', 'pragueescort' ),
				]
			);
		}

		if ( $reg_user_pswd !== $reg_user_pswd_confirm ) {
			wp_send_json_error(
				[
					'field'   => 'reg_user_pswd',
					'message' => __( 'Password field and password confirmation do not match', 'pragueescort' ),
				]
			);
		}

		if ( strlen( $reg_user_pswd ) < 4 || strlen( $reg_user_pswd ) > 15 ) {
			wp_send_json_error(
				[
					'field'   => 'reg_user_pswd',
					'message' => __( 'Incorrect password length', 'pragueescort' ),
				]
			);
		}

		//phpcs:disable
		$query = _http_build_query(
			[
				'user_email'   => $email,
				'display_name' => $user_name,
				'user_pass'    => base64_encode( $reg_user_pswd ),
				'phone'        => $reg_user_phone,
				'nonce'        => wp_create_nonce( self::PRA_MAIL_AUTH_NONCE ),
			]
		);
		//phpcs:enable

		$url = get_bloginfo( 'url' ) . '/verification-email/?' . $query;

		AdminEmail::send_validation_email_register(
			[
				'url'   => $url,
				'email' => $email,
			]
		);

		wp_send_json_success(
			[
				'title'   => __( 'Your account has been sent for verification email', 'pragueescort' ),
				'message' => __( 'A verification email has been sent to you, click on the link to activate your account.', 'pragueescort' ),
			]
		);
	}

	/**
	 * Add user role escort.
	 *
	 * @return void
	 */
	public function add_role_escort(): void {
		add_role( 'escort', __( 'Escort', 'pragueescort' ), get_role( 'author' )->capabilities );
	}

	/**
	 * Recover password.
	 *
	 * @return void
	 */
	public function recover_password_handler(): void {
		$nonce = ! empty( $_POST['nonce_recover'] ) ? filter_var( wp_unslash( $_POST['nonce_recover'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_RECOVER_PSWD_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Bad nonce code', 'pragueescort' ) ] );
		}

		$email = ! empty( $_POST['email'] ) ? filter_var( wp_unslash( $_POST['email'] ), FILTER_VALIDATE_EMAIL ) : null;

		if ( empty( $email ) ) {
			wp_send_json_error(
				[
					'field'   => 'recover_email',
					'message' => __( 'Email not correct', 'pragueescort' ),
				]
			);
		}

		if ( ! email_exists( $email ) ) {
			wp_send_json_error(
				[
					'field'   => 'recover_email',
					'message' => __( 'This email does not exist', 'pragueescort' ),
				]
			);
		}

		$password = wp_generate_password();

		//phpcs:disable
		$query = _http_build_query(
			[
				'user_email'    => $email,
				'new_user_pass' => base64_encode( $password ),
				'nonce_recover' => wp_create_nonce( self::PRA_MAIL_AUTH_NONCE ),
			]
		);
		//phpcs:enable

		$url = get_bloginfo( 'url' ) . '/recover-password-email/?' . $query;

		AdminEmail::send_recover_password(
			[
				'url'      => $url,
				'email'    => $email,
				'password' => $password,
			]
		);
	}

}
