<?php
/**
 * EscortListingAjax template.
 *
 * @package pragueescort/theme
 */

$escort_type = $atts['escort_type'] ?? null;
$css_class   = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$count       = 10;

// phpcs:disable
$gender = ! empty( $_GET['gender'] ) && 'all' !== $_GET['gender'] ? filter_var( wp_unslash( $_GET['gender'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

$escort_type_array = explode( ',', $escort_type );

$escort_type_array = [ $escort_type_array[0], $escort_type_array[2], $escort_type_array[1] ];

foreach ( $escort_type_array as $item ) {
	$slider_arg = [
		'post_type'      => 'escort',
		'posts_per_page' => 'standard' === $item ? $count : - 1,
		'order'          => 'DESC',
		'orderby'        => 'rand',
		'post_status'    => 'publish',
		'tax_query'      => [
			[
				'taxonomy' => 'escort-type',
				'field'    => 'slug',
				'terms'    => $item,
			],
		],
	];

	if ( $gender ) {
		$slider_arg = [
			'post_type'      => 'escort',
			'posts_per_page' => $count,
			'post_status'    => 'publish',
			'order'          => 'DESC',
			'orderby'        => 'rand',
			'tax_query'      => [
				[
					'taxonomy' => 'escort-type',
					'field'    => 'slug',
					'terms'    => $escort_type,
				],
			],
			'meta_query'     => [
				[
					'key'   => '_pra_gender',
					'value' => $gender,
				],
			],
		];
	}
// phpcs:enable

	$query = new WP_Query( $slider_arg );

	if ( $query->have_posts() ) {
		?>
		<div class="<?php echo esc_attr( $item ); ?>-plan-block  <?php echo esc_attr( $css_class ); ?>">
			<h2 class="status <?php echo esc_attr( $item ); ?>">
				<?php echo esc_html( strtoupper( $item ) ); ?>
			</h2>
			<div class="<?php echo esc_html( $item ); ?>-plan ajax-block">
				<?php
				$exclude = [];
				while ( $query->have_posts() ) {
					$query->the_post();
					$escort_id = get_the_ID();

					get_template_part(
						'template-part/single-escort/slider',
						'item',
						[
							'escort_id'  => $escort_id,
							'item_class' => $item,
						]
					);
					$exclude[] = (string) $escort_id;
				}
				wp_reset_postdata();
				?>
			</div>
			<?php if ( $query->max_num_pages > 1 ) { ?>
				<a
						href="#"
						data-exclude="<?php echo esc_attr( implode( ',', $exclude ) ); ?>"
						data-count="<?php echo esc_attr( $atts['pra_per_page'] ?? 15 ); ?>"
						data-type="<?php echo esc_attr( $item ); ?>"
						data-max_post="<?php echo esc_attr( $query->found_posts ); ?>"
						class="button gradient load-more-button">
					<?php esc_html_e( 'Look more' ); ?>
				</a>
				<span class="icon-more" style="display:none;">
				<?php esc_html_e( 'Loading more', 'pragueescort' ); ?>
			</span>
			<?php } ?>
		</div>
		<?php
	}
}
