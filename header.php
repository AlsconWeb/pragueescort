<?php
/**
 * Header theme file.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\DynamicPages\ProfilePage;

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?></title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header>
	<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex top-head">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<div class="vc_column-inner">
				<div class="wpb_wrapper">
					<div class="wpb_column vc_column_container">
						<?php
						if ( has_nav_menu( 'top_bar_menu' ) ) {
							wp_nav_menu(
								[
									'theme_location' => 'top_bar_menu',
									'menu_class'     => '',
									'menu_id'        => '',
									'echo'           => true,
									'fallback_cb'    => 'wp_page_menu',
									'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								]
							);
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex bottom-head">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<div class="vc_column-inner">
				<div class="wpb_wrapper">
					<div class="wpb_column vc_column_container">
						<div class="dfr">
							<div class="burger-menu">
								<span></span><span></span><span></span>
							</div>
							<?php
							the_custom_logo();

							if ( ! is_user_logged_in() ) {
								echo '<a class="icon-user" href="#" data-toggle="modal" data-target=".login">' . esc_html( __( 'Log in', 'pragueescort' ) ) . '</a>';
							} elseif ( ! current_user_can( 'administrator' ) ) {
								$logout_url          = wp_logout_url( get_bloginfo( 'url' ) );
								$current_user_escort = wp_get_current_user();

								$user_name = $current_user_escort->display_name;

								echo '<a class="icon-user" href="' . esc_url( home_url() . '/' . ProfilePage::PAGE_PATH ) . '">' . esc_html( $user_name ) . '</a><a class="icon-logout" href="' . esc_url( $logout_url ) . '"></a>';
							}

							if ( has_nav_menu( 'main_menu' ) ) {
								wp_nav_menu(
									[
										'theme_location' => 'main_menu',
										'menu_class'     => 'menu',
										'menu_id'        => '',
										'echo'           => true,
										'fallback_cb'    => 'wp_page_menu',
										'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									]
								);
							}

							if ( is_active_sidebar( 'languages_sidebar' ) ) {
								dynamic_sidebar( 'languages_sidebar' );
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
