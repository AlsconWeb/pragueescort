<?php
/**
 * Modal delete profile.
 *
 * @package pragueescort/theme
 */

?>
<div class="modal fade delete-modal">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<i class="icon-close" data-dismiss="modal"></i>
			<i class="icon-delete"></i>
			<h3><?php esc_html_e( 'Are you sure you want to delete your profile?', 'pragueescort' ); ?></h3>
			<a class="button delete" href="#"><?php esc_html_e( 'Delete my profile', 'pragueescort' ); ?></a>
			<a class="button close" href="#" data-dismiss="modal">
				<?php esc_html_e( 'Cancel', 'pragueescort' ); ?>
			</a>
		</div>
	</div>
</div>
