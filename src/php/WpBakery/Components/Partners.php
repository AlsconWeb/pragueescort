<?php
/**
 * Partners wp bakery component.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\WpBakery\Components;

use PRAGUE\Theme\Main;

/**
 * Partners class file.
 */
class Partners {
	/**
	 * Partners construct.
	 */
	public function __construct() {
		add_shortcode( 'pra_partners', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'pra_partners', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Partners', 'pragueescort' ),
			'description'             => esc_html__( 'Partners', 'pragueescort' ),
			'base'                    => 'pra_partners',
			'category'                => __( 'PRA', 'pragueescort' ),
			'show_settings_on_create' => false,
			'icon'                    => PRA_URL_PATH . '/assets/icons/handshake-solid.svg',
			'params'                  => [
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Title', 'pragueescort' ),
					'param_name' => 'title',
				],
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Partners', 'pragueescort' ),
					'param_name' => 'partners',
					'params'     => [
						[
							'type'       => 'attach_image',
							'value'      => '',
							'heading'    => __( 'Logo', 'pragueescort' ),
							'param_name' => 'logo',
						],
						[
							'type'       => 'vc_link',
							'value'      => '',
							'heading'    => __( 'Link', 'pragueescort' ),
							'param_name' => 'link',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Custom css', 'pragueescort' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design options', 'pragueescort' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::PRA_DIR_PATH . '/WpBakery/Templates/Partners/template.php';

		return ob_get_clean();
	}
}
