<?php
/**
 * Escort listing class.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\WpBakery\Components;

use PRAGUE\Theme\Main;

/**
 * EscortListing class file.
 */
class EscortListing {

	/**
	 * Escort type to select.
	 *
	 * @var array
	 */
	public array $escort_type;

	/**
	 * EscortListing construct
	 */
	public function __construct() {
		add_shortcode( 'pra_escort_listing', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'pra_escort_listing', [ $this, 'map' ] );
		}

		$this->set_term_escort_typ();
	}

	/**
	 * Set term escort type to select.
	 *
	 * @return void
	 */
	private function set_term_escort_typ(): void {
		$term_escort_type = get_terms(
			[
				'taxonomy'   => 'escort-type',
				'hide_empty' => false,
			]
		);

		$this->escort_type[ __( 'Choose escort type', 'pragueescort' ) ] = 'null';

		if ( ! empty( $term_escort_type ) && ! is_wp_error( $term_escort_type ) ) {
			foreach ( $term_escort_type as $term ) {
				$this->escort_type[ $term->name ] = $term->slug;
			}
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Escort Listing', 'pragueescort' ),
			'description'             => esc_html__( 'Escort Listing', 'pragueescort' ),
			'base'                    => 'pra_filter_diamond',
			'category'                => __( 'PRA', 'pragueescort' ),
			'show_settings_on_create' => false,
			'icon'                    => PRA_URL_PATH . '/assets/icons/table-list-solid.svg',
			'params'                  => [
				[
					'type'       => 'dropdown',
					'value'      => $this->escort_type,
					'param_name' => 'escort_type',
					'heading'    => __( 'Choose escort type', 'pragueescort' ),
				],
				[
					'type'       => 'textfield',
					'value'      => '',
					'param_name' => 'listing_in_row',
					'heading'    => __( 'Number of outputs per line', 'pragueescort' ),
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Custom css', 'pragueescort' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design options', 'pragueescort' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::PRA_DIR_PATH . '/WpBakery/Templates/EscortListing/template.php';

		return ob_get_clean();
	}
}
