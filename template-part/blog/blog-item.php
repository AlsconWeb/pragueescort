<?php
/**
 * Blog item template part.
 *
 * @package pragueescort/theme
 */

?>
<div class="blog-item">
	<?php
	if ( has_post_thumbnail( get_the_ID() ) ) {
		the_post_thumbnail( 'prague-blog-thumbnail' );
	} else {
		?>
		<img
				src="https://placehold.co/270x196"
				alt="No Image">
	<?php } ?>
	<a class="link" href="<?php the_permalink(); ?>"></a>
	<h4><?php the_title(); ?></h4>
	<p><?php the_excerpt(); ?></p>
</div>
