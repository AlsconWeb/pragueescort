<?php
/**
 * Profile form extra.
 *
 * @package pragueescort/theme
 */

$profile = $args['profile'];
?>
<div class="extra-block">
	<h5 class="extra"><?php esc_html_e( 'Extra', 'pragueescort' ); ?></h5>
	<div class="input">
		<p><?php esc_html_e( 'CIM (Come in mouth)', 'pragueescort' ); ?></p>
		<label for="cim" class="hide"></label>
		<input
				type="number"
				id="cim"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_cim' ) ?? '' ); ?>"
				min="0"
				name="edit_profile[pra_cim]"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'Golden shower', 'pragueescort' ); ?></p>
		<label for="golden" class="hide"></label>
		<input
				type="number"
				min="0"
				id="golden"
				name="edit_profile[pra_golden]"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_golden' ) ?? '' ); ?>"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'Rimming give', 'pragueescort' ); ?></p>
		<label for="anal-rimming-give" class="hide"></label>
		<input
				type="number"
				id="anal-rimming-give"
				name="edit_profile[pra_anal_rimming_give]"
				min="0"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_anal_rimming' ) ?? '' ); ?>"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'COF (Come on face)', 'pragueescort' ); ?></p>
		<label for="cof" class="hide"></label>
		<input
				type="text"
				name="edit_profile[pra_cof]"
				id="cof"
				min="0"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_cof' ) ?? '' ); ?>"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'Threesome', 'pragueescort' ); ?></p>
		<label for="threesome" class="hide"></label>
		<input
				type="number"
				id="threesome"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_threesome' ) ?? '' ); ?>"
				min="0"
				name="edit_profile[pra_threesome]"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'Rimming take', 'pragueescort' ); ?></p>
		<label for="rimming" class="hide"></label>
		<input
				type="number"
				id="rimming"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_anal_rimming' ) ?? '' ); ?>"
				min="0"
				name="edit_profile[pra_anal_rimming]"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'Swallow', 'pragueescort' ); ?></p>
		<label for="swallow" class="hide"></label>
		<input
				type="number"
				id="swallow"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_swallow' ) ?? '' ); ?>"
				min="0"
				name="edit_profile[pra_swallow]"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'Foot fetish', 'pragueescort' ); ?></p>
		<label for="foot-fetish" class="hide"></label>
		<input
				type="number"
				id="foot-fetish"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_foot_fetish' ) ?? '' ); ?>"
				min="0"
				name="edit_profile[pra_foot_fetish]"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'BDSM', 'pragueescort' ); ?></p>
		<label for="bdsm" class="hide"></label>
		<input
				type="number"
				id="bdsm"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_bdsm' ) ?? '' ); ?>"
				min="0"
				name="edit_profile[pra_bdsm]"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'DFK (Deep french kissing)', 'pragueescort' ); ?></p>
		<label for="dfk" class="hide"></label>
		<input
				type="number"
				id="dfk"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_dfk' ) ?? '' ); ?>"
				name="edit_profile[pra_dfk]"
				min="0"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'Domination', 'pragueescort' ); ?></p>
		<label for="domination" class="hide"></label>
		<input
				type="number"
				id="domination"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_domination' ) ?? '' ); ?>"
				name="edit_profile[pra_domination]"
				min="0"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'Submission', 'pragueescort' ); ?></p>
		<label for="submission" class="hide"></label>
		<input
				type="number"
				id="submission"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_submission' ) ?? '' ); ?>"
				name="edit_profile[pra_submission]"
				min="0"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'A-Level (Anal sex)', '' ); ?></p>
		<label for="anal" class="hide"></label>
		<input
				type="number"
				name="edit_profile[pra_anal]"
				id="anal"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_anal' ) ?? '' ); ?>"
				min="0"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'Strap on', '' ); ?></p>
		<label for="strap_on" class="hide"></label>
		<input
				type="number"
				name="edit_profile[pra_strap_on]"
				id="strap_on"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_strap_on' ) ?? '' ); ?>"
				min="0"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'Squirt', '' ); ?></p>
		<label for="pra_squirt" class="hide"></label>
		<input
				type="number"
				name="edit_profile[pra_squirt]"
				id="pra_squirt"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_squirt' ) ?? '' ); ?>"
				min="0"
				placeholder="0">
	</div>
	<div class="input">
		<p><?php esc_html_e( 'Couples', '' ); ?></p>
		<label for="pra_couples" class="hide"></label>
		<input
				type="number"
				name="edit_profile[pra_couples]"
				id="pra_couples"
				value="<?php echo esc_attr( carbon_get_post_meta( $profile->active_profile_id, 'pra_couples' ) ?? '' ); ?>"
				min="0"
				placeholder="0">
	</div>
	<hr class="sline">
</div>
