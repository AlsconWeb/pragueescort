<?php
/**
 * Profile action class.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\Profile;

use PRAGUE\Theme\Email\AdminEmail;

/**
 * ProfileActions class file.
 */
class ProfileActions {

	/**
	 * Add comment action name and nonce.
	 */
	public const PRA_ADD_COMMENT_ACTION_NAME = 'pra_add_comment_action_name';

	/**
	 * Refute comment action and nonce.
	 */
	public const PRA_COMMENT_REFUTE_ACTION_NAME = 'pra_comment_refute_action_name';

	/**
	 * ProfileActions construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hook and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'publish_escort', [ $this, 'notify_profile_moderation_status' ] );

		// add comment ajax.
		add_action( 'wp_ajax_' . self::PRA_ADD_COMMENT_ACTION_NAME, [ $this, 'add_comment' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_ADD_COMMENT_ACTION_NAME, [ $this, 'add_comment' ] );

		// add refute comment ajax.
		add_action( 'wp_ajax_' . self::PRA_COMMENT_REFUTE_ACTION_NAME, [ $this, 'add_refute' ] );
		add_action( 'wp_ajax_nopriv_' . self::PRA_COMMENT_REFUTE_ACTION_NAME, [ $this, 'add_refute' ] );
	}

	/**
	 * Profile moderation done and notifications.
	 *
	 * @param int $post_id Post ID.
	 *
	 * @return void
	 */
	public function notify_profile_moderation_status( int $post_id ): void {
		$post_author_id = get_post_field( 'post_author', $post_id );

		AdminEmail::notify_profile_moderation_publish(
			[
				'url'          => get_the_permalink( $post_id ),
				'profile_name' => get_the_title( $post_id ),
				'user_id'      => $post_author_id,
			]
		);
	}

	/**
	 * Add comment ajax handler.
	 *
	 * @return void
	 */
	public function add_comment(): void {
		$nonce = ! empty( $_POST['commentName'] ) ? filter_var( wp_unslash( $_POST['commentName'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_ADD_COMMENT_ACTION_NAME ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'comment-submit',
					'message'   => __( 'Bad nonce code', 'pragueescort' ),
				]
			);
		}

		$profile_id = ! empty( $_POST['profileID'] ) ? filter_var( wp_unslash( $_POST['profileID'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;

		if ( empty( $profile_id ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'comment-submit',
					'message'   => __( 'Profile id is empty', 'pragueescort' ),
				]
			);
		}

		$client_name = ! empty( $_POST['clientName'] ) ? filter_var( wp_unslash( $_POST['clientName'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( empty( trim( $client_name ) ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'client-name',
					'message'   => __( 'Client Name is empty', 'pragueescort' ),
				]
			);
		}

		$meeting_date = ! empty( $_POST['meetingDate'] ) ? filter_var( wp_unslash( $_POST['meetingDate'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( empty( $meeting_date ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'meeting-date',
					'message'   => __( 'Meeting date is empty', 'pragueescort' ),
				]
			);
		}

		$client_phone = ! empty( $_POST['clientPhone'] ) ? filter_var( wp_unslash( $_POST['clientPhone'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( empty( trim( $client_phone ) ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'client-phone',
					'message'   => __( 'Phone is empty', 'pragueescort' ),
				]
			);
		}

		$comment = ! empty( $_POST['comment'] ) ? filter_var( wp_unslash( $_POST['comment'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( empty( trim( $comment ) ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'comment',
					'message'   => __( 'Comment is empty', 'pragueescort' ),
				]
			);
		}

		$comment_data = [
			'comment_post_ID'  => $profile_id,
			'comment_author'   => $client_name,
			'comment_content'  => $comment,
			'comment_approved' => 0,
		];

		$comment_id = wp_insert_comment( $comment_data );

		if ( $comment_id ) {
			add_comment_meta( $comment_id, 'client_phone', $client_phone );
			add_comment_meta( $comment_id, 'meeting_date', $meeting_date );

			wp_send_json_success( [ 'message' => __( 'A comment was sent', 'pragueescort' ) ] );
		}
	}

	/**
	 * Add refute comment ajax handler.
	 *
	 * @return void
	 */
	public function add_refute(): void {
		$nonce = ! empty( $_POST['addRefuteNonce'] ) ? filter_var( wp_unslash( $_POST['addRefuteNonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, self::PRA_COMMENT_REFUTE_ACTION_NAME ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'refute-submit',
					'message'   => __( 'Bad nonce code', 'pragueescort' ),
				]
			);
		}

		$comment_id = ! empty( $_POST['commentID'] ) ? filter_var( wp_unslash( $_POST['commentID'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;

		if ( empty( $comment_id ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'refute-submit',
					'message'   => __( 'Comment id is empty', 'pragueescort' ),
				]
			);
		}

		$message = ! empty( $_POST['commentRefute'] ) ? filter_var( wp_unslash( $_POST['commentRefute'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( empty( trim( $message ) ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'refute-submit',
					'message'   => __( 'Comment is empty', 'pragueescort' ),
				]
			);
		}

		$profile_id = ! empty( $_POST['profileID'] ) ? filter_var( wp_unslash( $_POST['profileID'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;

		if ( empty( $profile_id ) ) {
			wp_send_json_error(
				[
					'fieldName' => 'refute-submit',
					'message'   => __( 'Profile id is empty', 'pragueescort' ),
				]
			);
		}

		$client_name = ! empty( $_POST['authorName'] ) ? filter_var( wp_unslash( $_POST['authorName'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		$comment_data = [
			'comment_post_ID'  => $profile_id,
			'comment_author'   => $client_name,
			'comment_content'  => $message,
			'comment_parent'   => $comment_id,
			'comment_approved' => 1,
		];

		$comment_id = wp_insert_comment( $comment_data );

		if ( $comment_id ) {
			wp_send_json_success( [ 'message' => __( 'A comment was sent', 'pragueescort' ) ] );
		}
	}
}
