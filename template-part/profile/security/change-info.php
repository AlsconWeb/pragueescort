<?php
/**
 * Change info profile from.
 *
 * @package pragueescort/theme
 */

?>
<form class="change-user-info" method="post">
	<div class="input">
		<label for="user-info-email">
			<?php esc_html_e( 'Email', 'pragueescort' ); ?>
		</label>
		<input type="email" name="user_info[email]" id="user-info-email">
	</div>
	<div class="input">
		<label for="user-info-phone">
			<?php esc_html_e( 'Phone', 'pragueescort' ); ?>
		</label>
		<input type="tel" name="user_info[phone]" id="user-info-phone">
	</div>
	<div class="submit">
		<input type="hidden" name="user_id" id="user-id" value="<?php echo esc_attr( get_current_user_id() ); ?>">
		<input
			class="button dark"
			id="submit-field"
			type="submit"
			value="<?php esc_html_e( 'Update', 'pragueescort' ); ?>">
	</div>
</form>
