<?php
/**
 * Helpers class.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme;

use mysql_xdevapi\Exception;

/**
 * Helpers class file.
 */
class Helpers {

	/**
	 * Exclude id.
	 *
	 * @var array
	 */
	public $exclude_id = [];

	/**
	 * Escort ID.
	 *
	 * @var int
	 */
	private int $escort_id = 0;

	/**
	 * Currency.
	 *
	 * @var string
	 */
	private string $currency;

	/**
	 * Validate phone number.
	 *
	 * @param string $phone Phone number.
	 *
	 * @return bool
	 */
	public static function validate_phone_number( string $phone ): bool {

		if ( preg_match( '/^\+?[1-9][0-9]{7,14}$/', $phone ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Get age escort.
	 *
	 * @param string $date Date birthday.
	 *
	 * @return string
	 */
	public static function get_age( string $date ): string {
		$date_b       = gmdate( 'Y', strtotime( $date ) );
		$current_year = gmdate( 'Y' );

		return $current_year - $date_b;
	}

	/**
	 * Set error cookie.
	 *
	 * @param array $errors Errors array.
	 *
	 * @return void
	 */
	public static function set_cookie_errors( array $errors ): void {
		try {
			$error_obj = wp_json_encode( $errors, JSON_THROW_ON_ERROR );
			setcookie( 'errors', $error_obj, time() + 600, COOKIEPATH, COOKIE_DOMAIN );
		} catch ( Exception $e ) {
			throw new Exception( $e->getMessage() );
		}
	}

	public static function print_comment( $comment, $refute = true ): void {
		?>
		<li>
			<div class="comment">
				<div class="user">
					<h3><?php echo esc_html( $comment->comment_author ); ?></h3>
				</div>
				<p><?php echo esc_html( $comment->comment_content ); ?></p>
			</div>
			<?php
			$children = get_comments(
				[
					'post_id' => get_the_ID(),
					'status'  => 'approve',
					'parent'  => $comment->comment_ID,
					'order'   => 'DESC',
					'orderby' => 'comment_date',
				]
			);

			if ( ! empty( $children ) ) {
				$refute = false;
				?>
				<ul class="reply icon-arrow">
					<?php
					foreach ( $children as $child ) {
						self::print_comment( $child, false );
					}
					?>
				</ul>
				<?php
			}

			if ( $refute ) {
				?>
				<a
					class="button refute-button"
					data-toggle="modal"
					data-target=".comment-refute-modal"
					data-comment_id="<?php echo esc_attr( $comment->comment_ID ); ?>"
					href="#"><?php esc_html_e( 'Refute', 'pragueescort' ); ?></a>
				<?php
			}
			?>
		</li>
		<?php
	}

	/**
	 * Get extra price.
	 *
	 * @param string $fields_name Carbon filed name.
	 * @param int    $escort_id   Post Id.
	 *
	 * @return string
	 */
	public function get_extra_price( string $fields_name, int $escort_id ): string {

		if ( $this->get_escort_id() !== $escort_id ) {
			$currency = carbon_get_post_meta( $escort_id, 'pra_currency' );
			$this->set_escort_id( $escort_id );
			$this->set_currency( $currency );
		} else {
			$currency = $this->get_currency();
		}

		switch ( $currency ) {
			case 'dollar':
				$currency_symbol = '$';
				break;
			case 'euro':
				$currency_symbol = '€';
				break;
			case 'crone':
				$currency_symbol = 'Kč';
				break;
		}

		$extra_prise = carbon_get_post_meta( $escort_id, $fields_name );

		if ( ! empty( $extra_prise ) ) {
			$price = '+' . $extra_prise . $currency_symbol;
		} else {
			$price = '-';
		}

		return $price;
	}

	/**
	 * Get tariff price.
	 *
	 * @param int $term_id Term ID Escort type.
	 *
	 * @return string
	 */
	public static function get_tariff_plan( int $term_id ) {
		$currency = carbon_get_term_meta( $term_id, 'pra_tariff_currency' );
		$prise    = carbon_get_term_meta( $term_id, 'pra_tariff_price' );
		switch ( $currency ) {
			case 'dollar':
				$currency_symbol = '$';
				break;
			case 'euro':
				$currency_symbol = '€';
				break;
			case 'crone':
				$currency_symbol = 'Kč';
				break;
		}

		if ( ! empty( $prise ) ) {
			$price = $prise . $currency_symbol;

			return $price;
		}

		return '';
	}

	/**
	 * Get escort id.
	 *
	 * @return int
	 */
	private function get_escort_id(): int {
		return $this->escort_id;
	}

	/**
	 * Set escort id.
	 *
	 * @return void
	 */
	private function set_escort_id( int $escort_id ): void {
		$this->escort_id = $escort_id;
	}

	/**
	 * Set Currency.
	 *
	 * @param string $currency Currency.
	 *
	 * @return void
	 */
	private function set_currency( string $currency ): void {
		$this->currency = $currency;
	}

	/**
	 * Get currency.
	 *
	 * @return string
	 */
	private function get_currency(): string {
		return $this->currency;
	}

	/**
	 * Exclude id.
	 *
	 * @param int $id Exclude id.
	 *
	 * @return void
	 */
	public function set_exclude_id( int $id ): void {
		$this->exclude_id[] = $id;
	}
}
