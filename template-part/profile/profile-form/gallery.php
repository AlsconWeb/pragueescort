<?php
/**
 * Profile form gallery.
 *
 * @package pragueescort/theme
 */

$profile     = $args['profile'];
$gallery     = carbon_get_post_meta( $profile->active_profile_id, 'pra_media_gallery' );
$tariff_plan = wp_get_post_terms( $profile->active_profile_id, 'escort-type', 'names' )[0];
$max_file    = 5;
$accept_type = '';

switch ( $tariff_plan ) {
	case 'standard':
		$max_file    = 5;
		$accept_type = 'image/*';
		break;
	case 'vip':
		$max_file    = 11;
		$accept_type = 'image/*, video/*';
		break;
	case 'diamond':
		$max_file    = 18;
		$accept_type = 'image/*, video/*';
		break;
}
?>
<div class="add-gallery">
	<h3><?php esc_html_e( 'Photo', 'pragueescort' ); ?></h3>
	<div class="gallery" id="gallery-images">
		<div class="images">
			<?php
			if ( ! empty( $gallery ) ) {
				foreach ( $gallery as $image_id ) {
					$image_url = wp_get_attachment_image_url( $image_id, 'escort_thumbnail' );
					?>
					<div class="img">
						<img
								class="imageThumb"
								src="<?php echo esc_url( $image_url ); ?>"
								title="<?php get_the_title( $image_id ); ?>">
						<br>
						<i
								class="icon-close delete-image"
								data-image_id="<?php echo esc_attr( $image_id ); ?>"
								data-profile_id="<?php echo esc_attr( $profile->active_profile_id ); ?>"></i>
					</div>
					<?php
				}
			}
			?>
			<input
					class="add-images"
					id="gallery-img"
					name="media_gallery[]"
					type="file"
					accept="<?php echo esc_html( $accept_type ); ?>"
					max="<?php echo esc_html( $max_file ); ?>"
					multiple>
			<label for="gallery-img">
				<?php
				printf(
					'<i class="icon-plus">%s<br>%s</i>',
					esc_html( __( 'Add photo', 'pragueescort' ) ),
					esc_html( __( 'or video', 'pragueescort' ) )
				);
				?>
			</label>
		</div>
	</div>
</div>
