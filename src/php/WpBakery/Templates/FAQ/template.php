<?php
/**
 * FAQ template.
 *
 * @package pragueescort/theme
 */

$css_class     = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$faq_questions = vc_param_group_parse_atts( $atts['questions'] );
?>
<div class="faq <?php echo esc_attr( $css_class ); ?>">
	<h2><?php echo esc_html( $atts['title'] ); ?></h2>
	<?php
	if ( ! empty( $faq_questions ) ) {
		foreach ( $faq_questions as $question ) {
			?>
			<div class="item">
				<h3><?php echo esc_html( $question['question'] ); ?></h3>
				<div class="text">
					<?php echo wp_kses_post( wpautop( $question['answer'] ) ); ?>
				</div>
			</div>
			<?php
		}
	}
	?>
</div>
