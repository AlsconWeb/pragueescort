<?php
/**
 * Authorization modal.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\AuthorizationUser;

?>
<div class="modal fade login">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<h3><?php esc_html_e( 'Registration on the site', 'pragueescort' ); ?></h3>
			<i class="icon-close" data-dismiss="modal"></i>
			<ul class="tabs-nav">
				<li class="active">
					<a href="#sign-in"><?php esc_html_e( 'Sign in', 'pragueescort' ); ?></a>
				</li>
				<li>
					<a href="#sign-up"><?php esc_html_e( 'Sign up', 'pragueescort' ); ?></a>
				</li>
				<li>
					<a href="#recover-password"><?php esc_html_e( 'Recover password', 'pragueescort' ); ?></a>
				</li>
			</ul>
			<div class="tabs">
				<div class="tab-content active" id="sign-in">
					<form class="login-form" method="post">
						<div class="input">
							<label for="user_email"><?php esc_html_e( 'Email', 'pragueescort' ); ?></label>
							<input
								id="user_email"
								type="email"
								name="user_email"
								placeholder="<?php esc_html_e( 'Enter your email', 'pragueescort' ); ?>" required>
						</div>
						<div class="input">
							<label for="user_pswd"><?php esc_html_e( 'Password', 'pragueescort' ); ?></label>
							<input
								type="password"
								name="user_pswd"
								id="user_pswd"
								placeholder="<?php esc_html_e( 'Enter your password', 'pragueescort' ); ?>"
								required>
						</div>
						<div class="checkbox">
							<input name="rememberme" type="checkbox" id="rememberme" value="forever">
							<label class="icon-check" for="rememberme">
								<?php esc_html_e( 'Remember Me', 'pragueescort' ); ?>
							</label>
						</div>
						<div class="submit">
							<?php wp_nonce_field( AuthorizationUser::PRA_AUTH_ACTION_NAME, AuthorizationUser::PRA_AUTH_ACTION_NAME ); ?>
							<input
								class="button"
								id="login_submit"
								type="submit"
								value="<?php esc_html_e( 'Login', 'pragueescort' ); ?>">
						</div>
					</form>
				</div>
				<div class="tab-content" id="sign-up">
					<form class="register_user" method="post">
						<div class="input">
							<label for="reg_user_email"><?php esc_html_e( 'Email', 'pragueescort' ); ?></label>
							<input
								id="reg_user_email"
								type="email"
								name="user_email"
								placeholder="<?php esc_html_e( 'Enter your email', 'pragueescort' ); ?>"
								required>
						</div>
						<div class="input">
							<label for="reg_user_name"><?php esc_html_e( 'Name', 'pragueescort' ); ?></label>
							<input
								id="reg_user_name"
								name="reg_user_name"
								type="text"
								placeholder="<?php esc_html_e( 'Enter your name', 'pragueescort' ); ?>"
								required>
						</div>
						<div class="input">
							<label for="reg_user_phone"><?php esc_html_e( 'Phone', 'pragueescort' ); ?></label>
							<input
								id="reg_user_phone"
								name="reg_user_phone"
								type="tel" required>
						</div>
						<div class="input">
							<label class="reg_user_pswd"><?php esc_html_e( 'Password', 'pragueescort' ); ?></label>
							<input
								id="reg_user_pswd"
								name="reg_user_pswd"
								type="password"
								placeholder="<?php esc_html_e( 'Create password', 'pragueescort' ); ?>"
								required>
							<p><?php esc_html_e( 'Password length from 5 to 15 characters', 'pragueescort' ); ?></p>
						</div>
						<div class="input">
							<label
								for="reg_user_pswd_confirm"><?php esc_html_e( 'Confirm password', 'pragueescort' ); ?></label>
							<input
								id="reg_user_pswd_confirm"
								name="reg_user_pswd_confirm"
								type="password"
								placeholder="<?php esc_html_e( 'Create password', 'pragueescort' ); ?>"
								required>
						</div>
						<div class="checkbox">
							<input name="privacy_policy" type="checkbox" id="privacy-policy" value="yes">
							<label class="icon-check" for="privacy-policy">
								<?php
								printf(
									'%s <a href="%s">%s</a>',
									esc_html( __( 'By registering on this site, I accept the', 'pragueescort' ) ),
									esc_url( get_privacy_policy_url() ),
									esc_html( __( 'privacy policy', 'pragueescort' ) )
								);
								?>
							</label>
						</div>
						<div class="submit">
							<?php wp_nonce_field( AuthorizationUser::PRA_REG_ACTION_NAME, AuthorizationUser::PRA_REG_ACTION_NAME ); ?>
							<input
								class="button"
								id="reg_submit"
								type="submit"
								value="<?php esc_html_e( 'Registration', 'pragueescort' ); ?>">
						</div>
					</form>
				</div>
				<div class="tab-content" id="recover-password">
					<form class="recover_password" method="post">
						<div class="input">
							<label for="reg_user_email"><?php esc_html_e( 'Email', 'pragueescort' ); ?></label>
							<input
								name="recover_email"
								id="recover_email"
								type="email"
								placeholder="<?php esc_html_e( 'Enter your email', 'pragueescort' ); ?>"
								required>
						</div>
						<div class="submit">
							<?php wp_nonce_field( AuthorizationUser::PRA_RECOVER_PSWD_ACTION_NAME, AuthorizationUser::PRA_RECOVER_PSWD_ACTION_NAME ); ?>
							<input
								class="button"
								type="submit"
								value="<?php esc_html_e( 'Recover password', 'pragueescort' ); ?>">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
