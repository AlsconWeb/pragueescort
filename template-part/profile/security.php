<?php
/**
 * Profile security template.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\Notification\NotificationUser;

$freeze = get_user_meta( get_current_user_id(), 'pra_account_freeze', true );
?>
<div class="wpb_column vc_column_container vc_col-xs-12 vc_col-sm-7 vc_col-md-8 vc_col-lg-9">
	<div class="vc_column-inner">
		<div class="wpb_wrapper">
			<div class="wpb_column vc_column_container">
				<div class="security">
					<?php
					if ( ! empty( $freeze ) ) {
						NotificationUser::account_is_freeze();
					}
					?>
					<h1><?php esc_html_e( 'Security', 'pragueescort' ); ?></h1>
					<p><?php esc_html_e( 'Lorem ipsum dolor sit amet, ius et congue feugait interpretaris, in nobis qualisque abhorreant duo, eu simul assueverit consectetuer mei.', 'pragueescort' ); ?></p>
					<?php
					get_template_part( 'template-part/profile/security/change', 'info' );
					get_template_part( 'template-part/profile/security/change', 'password' );
					?>
					<div class="buttons">
						<a
								class="button gradient icon-delete"
								id="delete-profile"
								data-user_id="<?php echo esc_attr( get_current_user_id() ); ?>" href="#">
							<?php esc_html_e( 'Delete account', 'pragueescort' ); ?>
						</a>
						<?php if ( ! empty( $freeze ) ) { ?>
							<a
									class="button icon-freeze"
									id="unfreeze-account"
									data-user_id="<?php echo esc_attr( get_current_user_id() ); ?>" href="#">
								<?php esc_html_e( 'Unfreeze account', 'pragueescort' ); ?>
							</a>
						<?php } else { ?>
							<a
									class="button icon-freeze"
									id="freeze-account"
									data-user_id="<?php echo esc_attr( get_current_user_id() ); ?>" href="#">
								<?php esc_html_e( 'Freeze account', 'pragueescort' ); ?>
							</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
