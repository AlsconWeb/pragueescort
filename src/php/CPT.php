<?php
/**
 * Init custom post types.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme;

/**
 * CPT class file.
 */
class CPT {

	/**
	 * CPT construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'init', [ $this, 'register_escort_post_type' ] );
		add_action( 'init', [ $this, 'register_order_post_type' ] );
		add_action( 'init', [ $this, 'register_taxonomy_service' ] );
		add_action( 'init', [ $this, 'register_taxonomy_type_escort' ] );
	}

	/**
	 * Register post type escort.
	 *
	 * @return void
	 */
	public function register_escort_post_type(): void {
		register_post_type(
			'escort',
			[
				'label'         => null,
				'labels'        => [
					'name'               => __( 'Escort', 'pragueescort' ),
					'singular_name'      => __( 'Escort', 'pragueescort' ),
					'add_new'            => __( 'Add new', 'pragueescort' ),
					'add_new_item'       => __( 'Add new', 'pragueescort' ),
					'edit_item'          => __( 'Edit people', 'pragueescort' ),
					'new_item'           => __( 'New people', 'pragueescort' ),
					'view_item'          => __( 'View people', 'pragueescort' ),
					'search_items'       => __( 'Search, people', 'pragueescort' ),
					'not_found'          => __( 'Not found', 'pragueescort' ),
					'not_found_in_trash' => __( 'No books found in Trash', 'pragueescort' ),
					'menu_name'          => __( 'Escort', 'pragueescort' ),
				],
				'description'   => '',
				'public'        => true,
				'show_in_menu'  => null,
				'show_in_rest'  => null,
				'rest_base'     => null,
				'menu_position' => 10,
				'menu_icon'     => 'dashicons-groups',
				'hierarchical'  => true,
				'supports'      => [
					'title',
					'editor',
					'author',
					'thumbnail',
					'excerpt',
					'custom-fields',
					'comments',
					'revisions',
				],
				'taxonomies'    => [],
				'has_archive'   => true,
				'rewrite'       => true,
				'query_var'     => true,
			]
		);
	}

	/**
	 * Register post type order.
	 *
	 * @return void
	 */
	public function register_order_post_type(): void {
		register_post_type(
			'orders',
			[
				'label'         => null,
				'labels'        => [
					'name'               => __( 'Order', 'pragueescort' ),
					'singular_name'      => __( 'Order', 'pragueescort' ),
					'add_new'            => __( 'Add new', 'pragueescort' ),
					'add_new_item'       => __( 'Add new', 'pragueescort' ),
					'edit_item'          => __( 'Edit Order', 'pragueescort' ),
					'new_item'           => __( 'New Order', 'pragueescort' ),
					'view_item'          => __( 'View Order', 'pragueescort' ),
					'search_items'       => __( 'Search, Order', 'pragueescort' ),
					'not_found'          => __( 'Not found', 'pragueescort' ),
					'not_found_in_trash' => __( 'No books found in Trash', 'pragueescort' ),
					'menu_name'          => __( 'Orders', 'pragueescort' ),
				],
				'description'   => '',
				'public'        => true,
				'show_in_menu'  => null,
				'show_in_rest'  => null,
				'rest_base'     => null,
				'menu_position' => 10,
				'menu_icon'     => 'dashicons-printer',
				'hierarchical'  => true,
				'supports'      => [
					'title',
					'editor',
					'author',
					'custom-fields',
					'revisions',
				],
				'taxonomies'    => [],
				'has_archive'   => true,
				'rewrite'       => true,
				'query_var'     => true,
			]
		);
	}

	/**
	 * Register taxonomy service.
	 *
	 * @return void
	 */
	public function register_taxonomy_service(): void {
		register_taxonomy(
			'service',
			[ 'escort' ],
			[
				'label'             => '',
				'labels'            => [
					'name'              => __( 'Service type', 'pragueescort' ),
					'singular_name'     => __( 'Service', 'pragueescort' ),
					'search_items'      => __( 'Search service', 'pragueescort' ),
					'all_items'         => __( 'All service', 'pragueescort' ),
					'view_item '        => __( 'View service', 'pragueescort' ),
					'parent_item'       => __( 'Parent service', 'pragueescort' ),
					'parent_item_colon' => __( 'Parent service:', 'pragueescort' ),
					'edit_item'         => __( 'Edit service', 'pragueescort' ),
					'update_item'       => __( 'Update service', 'pragueescort' ),
					'add_new_item'      => __( 'Add New service', 'pragueescort' ),
					'new_item_name'     => __( 'New service', 'pragueescort' ),
					'menu_name'         => __( 'Service', 'pragueescort' ),
					'back_to_items'     => __( '← Back to service', 'pragueescort' ),
				],
				'description'       => '',
				'public'            => true,
				'hierarchical'      => true,
				'rewrite'           => true,
				'capabilities'      => [],
				'meta_box_cb'       => null,
				'show_admin_column' => false,
				'show_in_rest'      => null,
				'rest_base'         => null,
			]
		);
	}

	/**
	 * Register taxonomy type escort.
	 *
	 * @return void
	 */
	public function register_taxonomy_type_escort(): void {
		register_taxonomy(
			'escort-type',
			[ 'escort' ],
			[
				'label'             => '',
				'labels'            => [
					'name'              => __( 'Escort type', 'pragueescort' ),
					'singular_name'     => __( 'Escort type', 'pragueescort' ),
					'search_items'      => __( 'Search type', 'pragueescort' ),
					'all_items'         => __( 'All Escort types', 'pragueescort' ),
					'view_item '        => __( 'View type', 'pragueescort' ),
					'parent_item'       => __( 'Parent type', 'pragueescort' ),
					'parent_item_colon' => __( 'Parent type:', 'pragueescort' ),
					'edit_item'         => __( 'Edit type', 'pragueescort' ),
					'update_item'       => __( 'Update type', 'pragueescort' ),
					'add_new_item'      => __( 'Add New type', 'pragueescort' ),
					'new_item_name'     => __( 'New type', 'pragueescort' ),
					'menu_name'         => __( 'Escort type', 'pragueescort' ),
					'back_to_items'     => __( '← Back to types', 'pragueescort' ),
				],
				'description'       => '',
				'public'            => true,
				'hierarchical'      => true,
				'rewrite'           => true,
				'capabilities'      => [],
				'meta_box_cb'       => null,
				'show_admin_column' => false,
				'show_in_rest'      => null,
				'rest_base'         => null,
			]
		);
	}
}
