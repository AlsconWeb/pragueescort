<?php
/**
 * Contacts modal.
 *
 * @package pragueescort/theme
 */

$escort_id      = get_the_ID();
$contacts       = carbon_get_post_meta( $escort_id, 'pra_contacts' );
$contacts_email = carbon_get_post_meta( $escort_id, 'pra_contact_email' );
$author_id      = get_the_author_meta( 'ID' );
$author_data    = get_userdata( $author_id );
?>
<div class="modal contacts-modal">
	<div class="modal-dialog modal-sm">
		<div class="modal-content"><i class="icon-close" data-dismiss="modal"></i>
			<h3><?php esc_html_e( 'Contact model', 'pragueescort' ); ?></h3>
			<div class="user">
				<?php
				if ( has_post_thumbnail( $escort_id ) ) {
					the_post_thumbnail( 'thumbnail' );
				} else {
					echo '<img src="https://via.placeholder.com/70x70" alt="No image">';
				}
				?>
				<h4><?php echo esc_html( get_the_title( $escort_id ) ); ?></h4>
			</div>

			<ul>
				<?php if ( ! empty( $contacts_email ) ) { ?>
					<li class="icon-mail">
						<a href="mailto:<?php echo esc_attr( $contacts_email ?? '' ); ?>">
							<?php echo esc_attr( $contacts_email ); ?>
						</a>
					</li>
					<?php
				}
				if ( ! empty( $contacts ) ) {
					foreach ( $contacts as $contact ) {
						?>
						<li class="icon-phone">
							<a href="tel:<?php echo filter_var( $contact['pra_contact_value'], FILTER_SANITIZE_NUMBER_INT ); ?>">
								<?php echo esc_html( $contact['pra_contact_value'] ); ?>
							</a>
						</li>
						<?php
					}
					?>
					<li class="icon-whatsapp">
						<a href="https://wa.me/<?php echo filter_var( str_replace( '+', '', $contacts[0]['pra_contact_value'] ), FILTER_SANITIZE_NUMBER_INT ); ?>/?text=Hi <?php echo esc_html( get_the_title( $escort_id ) ); ?>! I found you in Praguescort.cz">
							<?php esc_html_e( 'Send my massage', 'pragueescort' ); ?>
						</a>
					</li>
					<?php
				}
				?>
			</ul>
			<a class="button close" href="#" data-dismiss="modal">
				<?php esc_html_e( 'Close', 'pragueescort' ); ?>
			</a>
		</div>
	</div>
</div>
