<?php
/**
 * Projects catalog dynamic page.
 *
 * @package pragueescort/theme
 */

namespace PRAGUE\Theme\DynamicPages;

use PRAGUE\Theme\AuthorizationUser;

/**
 * AuthVerificationEmail class file.
 */
class AuthVerificationEmail extends DynamicPageBase {

	/**
	 * Page slug.
	 */
	public const PAGE_PATH = 'verification-email';

	/**
	 * Exclude footer and header.
	 *
	 * @var string
	 */
	public string $execute_slug = 'verification-email';

	/**
	 * Output content.
	 *
	 * @return void
	 */
	protected function content(): void {
		$this->show_content();
	}

	/**
	 * Show content.
	 *
	 * @return void
	 */
	private function show_content(): void {

		$nonce = ! empty( $_GET['nonce'] ) ? filter_var( wp_unslash( $_GET['nonce'] ), FILTER_SANITIZE_STRING ) : false;

		if ( ! wp_verify_nonce( $nonce, AuthorizationUser::PRA_MAIL_AUTH_NONCE ) ) {
			echo '<h1>' . esc_html( __( 'Bad nonce code', 'pragueescort' ) ) . '</h1>';
			exit;
		}

		$email        = ! empty( $_GET['user_email'] ) ? filter_var( wp_unslash( $_GET['user_email'] ), FILTER_SANITIZE_EMAIL ) : null;
		$display_name = ! empty( $_GET['display_name'] ) ? filter_var( wp_unslash( $_GET['display_name'] ), FILTER_SANITIZE_STRING ) : null;
		$phone        = ! empty( $_GET['phone'] ) ? filter_var( wp_unslash( $_GET['phone'] ), FILTER_SANITIZE_STRING ) : null;
		$user_pass    = ! empty( $_GET['user_pass'] ) ? filter_var( wp_unslash( $_GET['user_pass'] ), FILTER_SANITIZE_STRING ) : null;

		if ( empty( $email ) ) {
			echo '<h1>' . esc_html( __( 'No correct email', 'pragueescort' ) ) . '</h1>';
			exit;
		}

		if ( empty( $display_name ) ) {
			echo '<h1>' . esc_html( __( 'Empty name', 'pragueescort' ) ) . '</h1>';
			exit;
		}

		if ( empty( $user_pass ) ) {
			echo '<h1>' . esc_html( __( 'Empty name', 'pragueescort' ) ) . '</h1>';
			exit;
		}

		if ( email_exists( $email ) ) {
			echo '<h1>' . esc_html( __( 'This email is linked to another account', 'pragueescort' ) ) . '</h1>';
			exit;
		}

		// phpcs:disable
		$user_id = wp_insert_user(
			[
				'user_email'   => $email,
				'user_login'   => $email,
				'display_name' => $display_name,
				'user_pass'    => base64_decode( $user_pass ),
			]
		);
		// phpcs:enable

		if ( is_wp_error( $user_id ) ) {
			echo '<h1>' . esc_html( $user_id->get_error_message() ) . '</h1>';
			exit;
		}

		$profile_id = $this->create_profile_page( $user_id, $display_name );

		add_user_meta( $user_id, 'no_verification_account', 'true', true );
		add_user_meta( $user_id, 'phone', $phone, true );

		wp_signon(
			[
				'user_login'    => $email,
				'user_password' => base64_decode( $user_pass ),
			]
		);

		$_COOKIE['current_profile_id'] = $profile_id;
		setcookie( 'current_profile_id', $profile_id, time() + WEEK_IN_SECONDS, COOKIEPATH, SITECOOKIEPATH );

		wp_redirect( get_bloginfo( 'url' ) . '/' . TariffPage::PAGE_PATH, 301 );
		exit;
	}

	/**
	 * Create user profile page.
	 *
	 * @param int    $user_id User id.
	 * @param string $name    User name.
	 *
	 * @return int
	 */
	private function create_profile_page( int $user_id, string $name ): int {
		$new_post = [
			'post_title'  => $name,
			'post_status' => 'draft',
			'post_author' => $user_id,
			'post_type'   => 'escort',
		];

		$profile_id = wp_insert_post( $new_post );

		if ( is_wp_error( $profile_id ) ) {
			return 0;
		}

		wp_set_object_terms( $profile_id, [ 'standard' ], 'escort-type' );

		return $profile_id;
	}
}
