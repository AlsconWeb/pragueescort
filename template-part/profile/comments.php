<?php
/**
 * Template part comments in profile page.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\Helpers;
use PRAGUE\Theme\Notification\NotificationUser;

$profile        = $args['profile'];
$active_profile = $profile->active_profile_id;

$profile_comments = get_comments(
	[
		'post_id'      => $profile->active_profile_id,
		'status'       => 'approve',
		'hierarchical' => true,
		'order'        => 'DESC',
		'orderby'      => 'comment_date',
	]
);


?>
<div class="wpb_column vc_column_container vc_col-xs-12 vc_col-sm-7 vc_col-md-8 vc_col-lg-9">
	<div class="vc_column-inner">
		<div class="wpb_wrapper">
			<div class="wpb_column vc_column_container">
				<div class="comments-block">
					<?php
					if ( 'draft' === get_post_status( $active_profile ) ) {
						NotificationUser::profile_no_verified();
					}

					if ( get_user_meta( get_current_user_id(), 'pra_account_freeze', false ) ) {
						NotificationUser::account_is_freeze();
					}
					?>
					<h1><?php esc_html_e( 'Comments', 'pragueescort' ); ?></h1>
					<p><?php echo wp_kses_post( carbon_get_theme_option( 'pra_profile_comment_page' ) ); ?></p>
					<?php
					if ( ! empty( $profile_comments ) ) {
						?>
						<ul class="comments">
							<?php
							foreach ( $profile_comments as $profile_comment ) {
								if ( 0 === (int) $profile_comment->comment_parent ) {
									Helpers::print_comment( $profile_comment );
								}
							}
							?>
						</ul>
						<?php
					} else {
						?>
						<ul class="comments">
							<li>
								<h4 class="no-comments">
									<?php esc_html_e( 'No comments', 'pragueescort' ); ?>
								</h4>
							</li>
						</ul>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
