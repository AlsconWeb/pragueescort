/* global praObjectSettings */

/**
 * @param praObjectSettings.ajaxUrl
 * @param praObjectSettings.loginAction
 * @param praObjectSettings.loginNonceName
 * @param praObjectSettings.registerAction
 * @param praObjectSettings.registerNonceName
 * @param praObjectSettings.recoverPswdActionName
 * @param praObjectSettings.recoverPswdNonceName
 * @param praObjectSettings.addProfileAction
 * @param praObjectSettings.addProfileNonce
 * @param praObjectSettings.deleteProfileAction
 * @param praObjectSettings.deleteProfileNonce
 * @param praObjectSettings.changeProfileAction
 * @param praObjectSettings.changeProfileNonce
 * @param praObjectSettings.deleteImageInGalleryAction
 * @param praObjectSettings.deleteImageInGalleryNonce
 * @param praObjectSettings.loadMoreEscortListingAction
 * @param praObjectSettings.loadMoreEscortListingNonce
 * @param praObjectSettings.addCommentActionName
 * @param praObjectSettings.addCommentNonce
 * @param praObjectSettings.addRefuteCommentActionName
 * @param praObjectSettings.addRefuteCommentNonce
 * @param praObjectSettings.updateUserInfoActionName
 * @param praObjectSettings.updateUserInfoNonce
 * @param praObjectSettings.changePasswordActionName
 * @param praObjectSettings.changePasswordNonce
 * @param praObjectSettings.deleteAllProfileActionName
 * @param praObjectSettings.deleteAllProfileNonce
 * @param praObjectSettings.freezeAccountActionName
 * @param praObjectSettings.freezeAccountNonce
 * @param praObjectSettings.unfreezeAccountActionName
 * @param praObjectSettings.unfreezeAccountNonce
 * @param praObjectSettings.loadMoreArchiveActionName
 * @param praObjectSettings.loadMoreArchiveNonce
 * @param praObjectSettings.generatePaymentOrderAction
 * @param praObjectSettings.generatePaymentOrderNonce
 */

jQuery( document ).ready( function( $ ) {

	// Login ajax.

	let logInForm = $( '.login-form' );

	if ( logInForm.length ) {
		logInForm.submit( function( e ) {
			e.preventDefault();

			let data = {
				action: praObjectSettings.loginAction,
				user_email: $( '#user_email' ).val(),
				user_pswd: $( '#user_pswd' ).val(),
				remember_me: $( '#rememberme:checked' ).val() ?? null,
				nonce_login: $( '#' + praObjectSettings.loginNonceName ).val()
			};

			$.ajax( {
				type: 'POST',
				url: praObjectSettings.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( ! res.success ) {
						$( 'p.error' ).remove();
						let messageHtml = `<p class="error">${res.data.message}</p>`;
						$( '#' + res.data.field ).parent().append( messageHtml );
					} else {
						$( 'p.error' ).remove();
						location.href = res.data.urlRedirect;
					}

				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
				}
			} );
		} );
	}

	// Registration form.
	let registrationForm = $( '.register_user' );

	if ( registrationForm.length ) {

		registrationForm.submit( function( e ) {
			e.preventDefault();

			let data = {
				action: praObjectSettings.registerAction,
				user_email: $( '#reg_user_email' ).val(),
				user_name: $( '#reg_user_name' ).val(),
				reg_user_phone: $( '#reg_user_phone' ).val(),
				reg_user_pswd: $( '#reg_user_pswd' ).val(),
				reg_user_pswd_confirm: $( '#reg_user_pswd_confirm' ).val(),
				nonce_register: $( '#' + praObjectSettings.registerNonceName ).val(),
			};

			$.ajax( {
				type: 'POST',
				url: praObjectSettings.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( ! res.success ) {

						let error = '<p>' + res.data.message + '</p>';

						$( '.error' ).remove();
						$( '#' + res.data.field ).parent().append( error );
					} else {

						let thxModal = $( '.send-modal' );

						$( '.login.in' ).modal( 'hide' );
						$( '.error' ).remove();
						thxModal.find( '#modal-title' ).text( res.data.title );
						thxModal.find( '#modal-text' ).text( res.data.text );
						thxModal.modal( 'show' );

					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );
		} );
	}

	// Recover password.
	let recover = $( '.recover_password' );

	if ( recover.length ) {

		recover.submit( function( e ) {
			e.preventDefault();

			let data = {
				action: praObjectSettings.recoverPswdActionName,
				nonce_recover: $( '#' + praObjectSettings.recoverPswdNonceName ).val(),
				email: $( '#recover_email' ).val()
			};

			$.ajax( {
				type: 'POST',
				url: praObjectSettings.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( ! res.success ) {
						let error = '<p>' + res.data.message + '</p>';

						$( '.error' ).remove();
						$( '#' + res.data.field ).parent().append( error );
					} else {
						console.log( res.data.message );
					}

				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );
		} );
	}

	// Filter in main page.

	let genderFilterEl = $( '[data-gender]' );

	genderFilterEl.click( function( e ) {
		e.preventDefault();

		let value = $( this ).data( 'gender' );
		let formFilter = $( '.filters' );

		$( '.filters > #gender' ).val( value );

		formFilter.submit();
	} );

	// add profile.

	let addProfileEl = $( '#add-profile' );

	if ( addProfileEl.length ) {
		addProfileEl.click( function( e ) {
			e.preventDefault();

			let modalAddProfile = $( '.add-profile' );
			let userData = $( this ).data( 'user_id' );

			modalAddProfile.modal( 'show' );

			$( '#add_profile' ).click( function( e ) {
				e.preventDefault();

				let data = {
					action: praObjectSettings.addProfileAction,
					addUserNonce: praObjectSettings.addProfileNonce,
					userID: userData,
					profileName: $( '#add_name' ).val()
				};

				$.ajax( {
					type: 'POST',
					url: praObjectSettings.ajaxUrl,
					data: data,
					success: function( res ) {
						if ( res.success ) {
							setCookie( 'current_profile_id', res.data.newProfileID, '30' );
							location.reload();
						} else {
							console.log( res.data.message );
						}
					},
					error: function( xhr, ajaxOptions, thrownError ) {
						console.log( 'error...', xhr );
						//error logging
					}
				} );
			} );
		} );
	}

	// delete profile.

	let deleteProfileEL = $( '.delete-profile' );

	if ( deleteProfileEL.length ) {
		deleteProfileEL.click( function( e ) {
			e.preventDefault();

			let modalConfirm = $( '.delete-modal' );
			let deleteEL = $( this );

			modalConfirm.modal( 'show' );

			$( '.delete-modal .delete' ).click( function( e ) {
				e.preventDefault();

				let data = {
					action: praObjectSettings.deleteProfileAction,
					deleteProfileNonce: praObjectSettings.deleteProfileNonce,
					profileID: deleteEL.data( 'profile_id' )
				};

				let countEL = $( '.delete-profile' );

				if ( countEL.length > 1 ) {
					$.ajax( {
						type: 'POST',
						url: praObjectSettings.ajaxUrl,
						data: data,
						success: function( res ) {
							if ( res.success ) {
								let cookieProfileID = getCookie( 'current_profile_id' );

								if ( cookieProfileID === res.data.profileID ) {
									deleteCookie( 'current_profile_id' );
								}

								$( '[data-profile_id = ' + res.data.profileID + ' ]' ).remove();

								$( deleteProfileEL[ 0 ] ).parent().addClass( 'active' );

								setCookie( 'current_profile_id', $( deleteProfileEL[ 0 ] ).data( 'profile_id' ), 30 );

								modalConfirm.modal( 'hide' );

								location.reload();
							} else {
								console.log( res.data.message );
							}

						},
						error: function( xhr, ajaxOptions, thrownError ) {
							console.log( 'error...', xhr );
							//error logging
						}
					} );
				} else {
					modalConfirm.modal( 'hide' );
				}

			} );

		} );


		// set active profile.

		$( '#active-profile' ).val( $( '.profiles-menu .active' ).data( 'profile_id' ) );
	}

	// change profile.

	let changeProfile = $( '.change-profile' );

	if ( changeProfile.length ) {

		changeProfile.click( function( e ) {
			e.preventDefault();

			let data = {
				action: praObjectSettings.changeProfileAction,
				nonceChange: praObjectSettings.changeProfileNonce,
				profileID: $( this ).parent().data( 'profile_id' )
			};

			$.ajax( {
				type: 'POST',
				url: praObjectSettings.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( res.success ) {
						setCookie( 'current_profile_id', res.data.profileID, '1' );
						location.reload();
					} else {
						console.log( res.data.message );
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );
		} );
	}

	// delete image in gallery profile.

	let deleteImageEl = $( '.delete-image' );

	if ( deleteImageEl.length ) {

		deleteImageEl.click( function( e ) {
			e.preventDefault();

			let data = {
				action: praObjectSettings.deleteImageInGalleryAction,
				deleteImageNonce: praObjectSettings.deleteImageInGalleryNonce,
				imageID: $( this ).data( 'image_id' ),
				profileID: $( this ).data( 'profile_id' )
			};

			$.ajax( {
				type: 'POST',
				url: praObjectSettings.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( res.success ) {
						$( '[data-image_id=' + res.data.imageID + ']' ).parent().remove();
					} else {
						console.log( res.data.message );
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );

		} );
	}


	/**
	 * Set Cookie.
	 *
	 * @param name Cookie name.
	 * @param value Cookie value.
	 * @param days Cookie date.
	 */
	function setCookie( name, value, days ) {
		let expires = '';
		if ( days ) {
			var date = new Date();
			date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ) );
			expires = '; expires=' + date.toUTCString();
		}
		document.cookie = name + '=' + ( value || '' ) + expires + '; path=/';
	}

	/**
	 * Get Cookie.
	 *
	 * @param name Cookie name.
	 * @returns {null|string}
	 */
	function getCookie( name ) {
		let nameEQ = name + '=';
		let ca = document.cookie.split( ';' );

		for ( let i = 0; i < ca.length; i++ ) {
			let c = ca[ i ];
			while ( c.charAt( 0 ) === ' ' ) c = c.substring( 1, c.length );
			if ( c.indexOf( nameEQ ) === 0 ) return c.substring( nameEQ.length, c.length );
		}

		return null;
	}

	/**
	 * Delete cookie.
	 *
	 * @param name
	 */
	function deleteCookie( name ) {
		document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}

	// date-pecker.
	let age = $( '.age' );

	if ( age.length ) {
		age.datepicker( {
			container: '.age-block',
			autoclose: true,
		} );
	}

	// ajax in escort listing.

	let escortListingAjaxBtEl = $( '.load-more-button' );

	if ( escortListingAjaxBtEl.length ) {
		escortListingAjaxBtEl.click( function( e ) {
			e.preventDefault();

			let data = {
				action: praObjectSettings.loadMoreEscortListingAction,
				loadMoreNonce: praObjectSettings.loadMoreEscortListingNonce,
				excludePost: $( this ).data( 'exclude' ),
				countPerPage: $( this ).data( 'count' ),
				escortType: $( this ).data( 'type' ),
			};

			let preloadEL = $( '.icon-more' );
			let maxPost = $( this ).data( 'max_post' );
			let postCount = $( '.ajax-block .item.standard' ).length;
			let excludePostArray = $( this ).data( 'exclude' ).split( ',' ).map( Number );
			console.log( 'postCount', postCount );
			if ( postCount < maxPost ) {
				$.ajax( {
					type: 'POST',
					url: praObjectSettings.ajaxUrl,
					data: data,
					beforeSend: function() {
						preloadEL.show();
					},
					success: function( res ) {
						if ( res.success ) {

							excludePostArray = res.data.exclude.concat( excludePostArray );
							let newExcludePost = excludePostArray.join( ',' );
							escortListingAjaxBtEl.data( 'exclude', newExcludePost );

							$( '.standard-plan.ajax-block' ).append( res.data.html );

							let postCount = $( '.standard-plan.ajax-block .item' ).length;
							if ( postCount === maxPost ) {
								escortListingAjaxBtEl.hide();
							}

							preloadEL.hide();

						} else {
							preloadEL.hide();
							console.log( res.data.log() );
						}

					},
					error: function( xhr, ajaxOptions, thrownError ) {
						console.log( 'error...', xhr );
						//error logging
					}
				} );
			}
		} );
	}

	// ajax add new comment

	let commentFormEl = $( '.add-comment-form-client' );

	if ( commentFormEl.length ) {
		commentFormEl.submit( ( e ) => {
			e.preventDefault();

			let data = {
				action: praObjectSettings.addCommentActionName,
				commentName: praObjectSettings.addCommentNonce,
				profileID: $( '#comments' ).data( 'profile_id' ),
				clientName: $( '#client-name' ).val(),
				meetingDate: $( '#meeting-date' ).val(),
				clientPhone: $( '#client-phone' ).val(),
				comment: $( '#comment' ).val(),
			};

			$.ajax( {
				type: 'POST',
				url: praObjectSettings.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( res.success ) {
						$( '.error' ).remove();
						$( '.comment-modal' ).modal( 'hide' );
						commentFormEl.resetForm();
					} else {
						let htmlError = '<p class="error">' + res.data.message + '</p>';
						$( '#' + res.data.fieldName ).append( htmlError );
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );

		} );
	}

	// add refute comment.

	let refuteBtn = $( '.refute-button' );

	if ( refuteBtn.length ) {
		refuteBtn.click( function() {
			let commentID = $( this ).data( 'comment_id' );

			$( '#comment-id' ).val( commentID );
		} );
	}

	let refuteForm = $( '.comment-refute-modal' );

	if ( refuteForm.length ) {
		refuteForm.submit( ( e ) => {
			e.preventDefault();

			let data = {
				action: praObjectSettings.addRefuteCommentActionName,
				addRefuteNonce: praObjectSettings.addRefuteCommentNonce,
				commentID: $( '#comment-id' ).val(),
				commentRefute: $( '#comment-refute' ).val(),
				profileID: $( '.profiles-menu .active' ).data( 'profile_id' ),
				authorName: $( '.profiles-menu .active .change-profile' ).text()
			};

			$.ajax( {
				type: 'POST',
				url: praObjectSettings.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( res.success ) {
						$( '.error' ).remove();
						$( '.comment-refute-modal' ).modal( 'hide' );
						location.reload();
					} else {
						let htmlError = '<p class="error">' + res.data.message + '</p>';
						$( '#' + res.data.fieldName ).append( htmlError );
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );
		} );
	}

	// change user info email and phone.

	let userInfoForm = $( '.change-user-info' );

	if ( userInfoForm.length ) {
		userInfoForm.submit( ( e ) => {
			e.preventDefault();

			let data = {
				action: praObjectSettings.updateUserInfoActionName,
				nonceUserInfo: praObjectSettings.updateUserInfoNonce,
				newEmail: $( '#user-info-email' ).val(),
				newPhone: $( '#user-info-phone' ).val(),
				userID: $( '#user-id' ).val()
			};

			$.ajax( {
				type: 'POST',
				url: praObjectSettings.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( res.success ) {
						console.log( res.data.message );
						$( '.error' ).remove();
					} else {
						let htmlError = '<p class="error">' + res.data.message + '</p>';
						$( '#' + res.data.fieldName ).append( htmlError );
					}

				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );
		} );
	}

	// change password form.

	let changePasswordFrom = $( '.change-user-password' );

	if ( changePasswordFrom.length ) {
		changePasswordFrom.submit( ( e ) => {
			e.preventDefault();

			let data = {
				action: praObjectSettings.changePasswordActionName,
				changePswdNonce: praObjectSettings.changePasswordNonce,
				userID: $( '#user-id' ).val(),
				oldPassword: $( '#old-password' ).val(),
				newPassword: $( '#new-password' ).val(),
				confirmPassword: $( '#confirm-password' ).val(),
			};

			$.ajax( {
				type: 'POST',
				url: praObjectSettings.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( res.success ) {
						location.reload();
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );
		} );
	}

	// delete profile.

	let deleteProfileBtn = $( '#delete-profile' );
	let userID = 0;

	if ( deleteProfileBtn.length ) {
		deleteProfileBtn.click( function( e ) {
			e.preventDefault();

			let deleteModal = $( '.delete-modal' );
			userID = $( this ).data( 'user_id' );
			deleteModal.addClass( 'delete-all' );
			deleteModal.modal( 'show' );


			$( '.delete-all .delete' ).click( function( e ) {
				e.preventDefault();

				let data = {
					action: praObjectSettings.deleteAllProfileActionName,
					deleteAllNonce: praObjectSettings.deleteAllProfileNonce,
					userID: userID,
				};

				$.ajax( {
					type: 'POST',
					url: praObjectSettings.ajaxUrl,
					data: data,
					success: function( res ) {
						if ( res.success ) {
							location.reload();
						} else {
							console.log( res.data.message );
						}
					},
					error: function( xhr, ajaxOptions, thrownError ) {
						console.log( 'error...', xhr );
						//error logging
					}
				} );

			} );

			$( '.delete-all .close' ).click( function( e ) {
				$( '.delete-modal' ).removeClass( 'delete-all' );
			} );

		} );
	}

	// freeze account.

	let freezeAccountBtn = $( '#freeze-account' );

	if ( freezeAccountBtn.length ) {
		freezeAccountBtn.click( function( e ) {
			e.preventDefault();

			let data = {
				action: praObjectSettings.freezeAccountActionName,
				freezeNonce: praObjectSettings.freezeAccountNonce,
				userID: $( this ).data( 'user_id' )
			};

			$.ajax( {
				type: 'POST',
				url: praObjectSettings.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( res.success ) {
						location.reload();
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );
		} );
	}

	// unfreeze account.
	let unfreezeAccountBtn = $( '#unfreeze-account' );

	if ( unfreezeAccountBtn.length ) {
		unfreezeAccountBtn.click( function( e ) {
			e.preventDefault();

			let data = {
				action: praObjectSettings.unfreezeAccountActionName,
				unfreezeNonce: praObjectSettings.unfreezeAccountNonce,
				userID: $( this ).data( 'user_id' )
			};

			$.ajax( {
				type: 'POST',
				url: praObjectSettings.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( res.success ) {
						location.reload();
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );
		} );
	}

	// Archive page load more.

	let archiveLoadMore = $( '.load-more-archive' );

	if ( archiveLoadMore.length ) {
		archiveLoadMore.click( function( e ) {
			e.preventDefault();

			let count = $( this ).data( 'count' );
			let paged = $( this ).data( 'paged' );
			let maxPaged = $( this ).data( 'max_pages' );

			let data = {
				action: praObjectSettings.loadMoreArchiveActionName,
				archiveNonce: praObjectSettings.loadMoreArchiveNonce,
				count: count,
				paged: paged,
				maxPaged: maxPaged,
				taxName: $( this ).data( 'tax' ),
				termSlug: $( this ).data( 'term' )
			};

			$.ajax( {
				type: 'POST',
				url: praObjectSettings.ajaxUrl,
				data: data,
				beforeSend: function() {
					$( '.icon-more' ).show();
				},
				success: function( res ) {
					if ( res.success ) {
						$( '.diamond-plan, .standard-plan' ).append( res.data.html );
						if ( res.data.paged === maxPaged ) {
							$( '.load-more-archive' ).hide();
						}
						$( '.icon-more' ).hide();
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );

		} );
	}

	let tariffBtn = $( '.tariff-block a.button.tariff' );

	if ( tariffBtn.length ) {
		tariffBtn.click( function( e ) {
			e.preventDefault();

			let data = {
				action: praObjectSettings.generatePaymentOrderAction,
				nonceOrder: praObjectSettings.generatePaymentOrderNonce,
				userID: $( this ).data( 'current_user' ),
				price: $( this ).data( 'price' ),
				tariffID: $( this ).data( 'tariff_id' ),
				profileID: $( this ).data( 'profile_id' )
			};

			$.ajax( {
				type: 'POST',
				url: praObjectSettings.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( res.success ) {
						window.open( res.data.invoice, '_blank' );
					}

					if ( ! res.success ) {
						console.log( res.data.message );
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );

		} );
	}

	const yearModal = $( '.years-modal' );
	if ( yearModal.length ) {
		let showCookie = getCookie( 'yearsModal' );
		if ( null === showCookie ) {
			yearModal.modal( 'show' );
		}

		console.log( showCookie );
		if ( 'false' === showCookie ) {
			yearModal.modal( 'show' );
			$( 'body' ).css( { pointerEvents: 'none' } );
		}

		yearModal.find( '.yes-btn' ).click( function( e ) {
			e.preventDefault();

			setCookie( 'yearsModal', 'true', '3' );
			yearModal.modal( 'hide' );
		} );

		yearModal.find( '.no-btn' ).click( function( e ) {
			e.preventDefault();

			setCookie( 'yearsModal', 'false', '3' );
			window.location.reload();
		} );
	}
} );
