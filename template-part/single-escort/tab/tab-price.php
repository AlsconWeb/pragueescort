<?php
/**
 * Tab price.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\Helpers;

$escort_id = get_the_ID();
$helpers   = new Helpers();
?>
<div class="tab-content" id="price">
	<ul class="desc">
		<li>
			<?php esc_html_e( '30 min', 'pragueescort' ); ?>
			<span><?php echo esc_html( $helpers->get_extra_price( 'pra_half_hour', $escort_id ) ); ?></span>
		</li>
		<li>
			<?php esc_html_e( '1 hour', 'pragueescort' ); ?>
			<span><?php echo esc_html( $helpers->get_extra_price( 'pra_one_hour', $escort_id ) ); ?></span>
		</li>
		<li>
			<?php esc_html_e( '2 hour', 'pragueescort' ); ?>
			<span><?php echo esc_html( $helpers->get_extra_price( 'pra_two_hour', $escort_id ) ); ?></span>
		</li>
		<li>
			<?php esc_html_e( '3 hour', 'pragueescort' ); ?>
			<span><?php echo esc_html( $helpers->get_extra_price( 'pra_three_hour', $escort_id ) ); ?></span>
		</li>
		<li>
			<?php esc_html_e( 'Night', 'pragueescort' ); ?>
			<span><?php echo esc_html( $helpers->get_extra_price( 'pra_night', $escort_id ) ); ?></span>
		</li>
		<li>
			<?php esc_html_e( 'Twenty-four hours', 'pragueescort' ); ?>
			<span><?php echo esc_html( $helpers->get_extra_price( 'pra_day', $escort_id ) ); ?></span>
		</li>
	</ul>
</div>
