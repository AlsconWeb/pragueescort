<?php
/**
 * Profile form description.
 *
 * @package pragueescort/theme
 */

use PRAGUE\Theme\CarbonFields;

$profile       = $args['profile'];
$profile_field = new CarbonFields();
?>
<div class="description">
	<div class="photo">
		<div class="name">
			<h2 title="<?php echo esc_html( get_the_title( $profile->active_profile_id ) ); ?>">
				<?php echo esc_html( get_the_title( $profile->active_profile_id ) ); ?>
			</h2>
			<i class="icon-edit"></i>
			<div class="input">
				<input
						class="name-input"
						type="text"
						name="edit_profile[profile_title]">
				<i class="icon-check"> </i>
			</div>
		</div>
		<input id="photo" name="main_photo" type="file" accept="image/*">
		<label for="photo" class="<?php echo has_post_thumbnail( $profile->active_profile_id ) ? 'edite' : ''; ?>">
			<i class="icon-photo" for="photo">
				<?php esc_html_e( 'Change', 'pragueescort' ); ?>
			</i>
			<?php
			if ( has_post_thumbnail( $profile->active_profile_id ) ) {
				echo wp_kses_post( get_the_post_thumbnail( $profile->active_profile_id, 'escort_thumbnail', [ 'class' => '' ] ) );
			}
			?>
		</label>
	</div>
	<?php
	$content = get_the_content( $profile->active_profile_id );
	?>
	<div class="textarea">
		<label for="description"><?php esc_html_e( 'Info', 'pragueescort' ); ?></label>
		<textarea
				name="edit_profile[description]"
				id="description"
				placeholder="<?php esc_html_e( 'Write a few words about yourself.', 'pragueescort' ); ?>"><?php echo ! empty( $content ) ? wp_kses_post( $content ) : ''; ?></textarea>
	</div>
	<div class="contacts">
		<h5><?php esc_html_e( 'Contacts', 'pragueescort' ); ?></h5>
		<?php
		$contacts = carbon_get_post_meta( $profile->active_profile_id, 'pra_contacts' );
		if ( ! empty( $contacts ) ) {
			foreach ( $contacts as $key => $contact ) {
				echo sprintf(
					'<div class="contact-item"><p class="icon-phone">%s</p><input type="tel" name="edit_profile[pra_contact_value][%d]" id="number-%d" value="%s"><i class="icon-checked"></i><i class="icon-close"></i></div>',
					esc_html( __( 'Add phone', 'pragueescort' ) ),
					esc_attr( $key ),
					esc_attr( $key ),
					esc_attr( $contact['pra_contact_value'] )
				);
			}
		}
		?>
		<a class="add-contact" href="#">
			<i><?php esc_html_e( '+ Add contact', 'pragueescort' ); ?></i>
		</a>
	</div>
</div>
