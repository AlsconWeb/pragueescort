<?php
/**
 *  Template part modal refute.
 *
 * @package pragueescort/theme
 */

$profile = $args['profile'];
?>

<div class="modal comment-refute-modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content"><i class="icon-close" data-dismiss="modal"> </i>
			<div class="user">
				<?php
				if ( has_post_thumbnail( $profile->active_profile_id ) ) {
					echo wp_kses_post( get_the_post_thumbnail( $profile->active_profile_id, 'escort_thumbnail', [ 'class' => '' ] ) );
				}
				?>
				<h3><?php echo esc_html( get_the_title( $profile->active_profile_id ) ); ?></h3>
			</div>
			<form class="add-comment-refute" method="post">
				<div class="textarea">
					<label class="hide" for="comment-refute"></label>
					<textarea
							id="comment-refute"
							placeholder="<?php esc_html_e( 'Refute comment', 'pragueescort' ); ?>"></textarea>
				</div>
				<div class="submit">
					<input id="comment-id" type="hidden" name="comment_id">
					<input
							class="button gradient" id="refute-submit" type="submit"
							value="<?php esc_html_e( 'Send', 'pragueescort' ); ?>">
				</div>
			</form>
		</div>
	</div>
</div>
